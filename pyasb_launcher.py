#!/usr/bin/env python2

'''
PyASB launcher module

Concatenate processes
____________________________

This module is part of the PyASB project, 
created and maintained by Miguel Nievas [UCM].
____________________________
'''

__author__ = "Miguel Nievas"
__copyright__ = "Copyright 2012, PyASB project"
__credits__ = ["Miguel Nievas"]
__license__ = "GNU GPL v3"
__shortname__ = "PyASB"
__longname__ = "Python All-Sky Brightness pipeline"
__version__ = "1.99.0"
__maintainer__ = "Miguel Nievas"
__email__ = "miguelnr89[at]gmail[dot]com"
__status__ = "Prototype" # "Prototype", "Development", or "Production"

try:
	#import gc
	import sys,os,inspect
	import signal
	import time
	
	from input_options import *
	from image_info import *
	from program_help import *
	from astrometry import *
	from star_calibration import *
	from load_fitsimage import *
	from bouguer_fit import *
	from sky_brightness import *
	from skymap_plot import *
	from cloud_coverage import *
	from write_summary import *
except:
	print("Launcher, " + str(inspect.stack()[0][2:4][::-1])+': One or more modules missing')
	raise SystemExit

config_file_default  = 'pyasb_config.cfg'

#@profile
class ImageAnalysis():
	def __init__(self,Image):
		''' Analize image and perform star astrometry & photometry. 
			Returns ImageInfo and StarCatalog'''
		Image.ImageInfo.catalog_filename = 'catalog.csv'
		# Get coordinates of each pixel
		Image.FitsImage.Coordinates = ImageCoordinates(Image.ImageInfo)
		RawCatalog = Catalog(Image.ImageInfo)
		self.StarCatalog = SolvedImage(Image.FitsImage,Image.ImageInfo,RawCatalog)
		TheSkyMap = SkyMap(self.StarCatalog,Image.ImageInfo,Image.FitsImage)

#@profile
def perform_complete_analysis(InputOptions,ImageInfoCommon,ConfigOptions,input_file):
	# Load Image into memory & reduce it.
		# Clean (no leaks)
		TheImage = LoadImage(InputOptions,ImageInfoCommon,ConfigOptions,input_file)
		
		# Look for stars that appears in the catalog, measure their fluxes. Generate starmap.
		# Clean (no leaks)
		AnalyzedImage = ImageAnalysis(TheImage)
		
		print('Image date: '+str(TheImage.ImageInfo.date_string)+\
		 ', Image filter: '+str(TheImage.ImageInfo.used_filter))
		
		'Create the needed classes for the summary write'
		class CalibratedInstrument:
			class BouguerFit:
				class Regression:
					mean_zeropoint = -1
					error_zeropoint = -1
					extinction = -1
					error_extinction = -1
					Nstars_rel = -1
					Nstars_initial = -1
		
		try:
			# Calibrate instrument with image. Generate fit plot.
			# Clean (no leaks)
			CalibratedInstrument = InstrumentCalibration(\
				TheImage.ImageInfo,
				AnalyzedImage.StarCatalog)
		except:
			class ImageSkyBrightness:
				SBzenith = '-1'
				SBzenith_err = '-1'
			
		else:
			# Measure sky brightness / background. Generate map.
			ImageSkyBrightness = MeasureSkyBrightness(\
				TheImage.FitsImage,
				TheImage.ImageInfo,
				CalibratedInstrument.BouguerFit)
		
		'''
		Even if calibration fails, 
		we will try to determine cloud coverage
		and write the summary
		'''
		
		# Detect clouds on image
		ImageCloudCoverage = CloudCoverage(\
			TheImage,
			AnalyzedImage,
			CalibratedInstrument.BouguerFit)
		
		Summary_ = Summary(TheImage, InputOptions, AnalyzedImage, \
			CalibratedInstrument, ImageSkyBrightness, ImageCloudCoverage)
		
		#gc.collect()
		#print(gc.garbage)


def get_config_filename(InputOptions):
	config_file = config_file_default
	try:
		assert(InputOptions.configfile!=False)
	except:
		print(str(inspect.stack()[0][2:4][::-1])+\
		 ': config file not specified, use the default one:')
	else:
		config_file = InputOptions.configfile
	
	return(config_file)


if __name__ == '__main__':
	#gc.set_debug(gc.DEBUG_STATS)
	PlatformHelp_ = PlatformHelp()
	InputOptions = ReadOptions(sys.argv)
	
	config_file = get_config_filename(InputOptions)
	ConfigOptions_ = ConfigOptions(config_file)
	ImageInfoCommon = ImageInfo()
	ImageInfoCommon.config_processing_common(ConfigOptions_,InputOptions)
	try:
		assert(InputOptions.show_help == False)
	except:
		print(inspect.stack()[0][2:4][::-1])
		# Show help and halt
		PlatformHelp_.show_help()
		raise SystemExit
	
	for input_file in InputOptions.fits_filename_list:
		perform_complete_analysis(InputOptions,ImageInfoCommon,ConfigOptions_,input_file)
	
