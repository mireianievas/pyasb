#!/usr/bin/env python2

'''
Data mining module

Analize raw data from PyASB images.
____________________________

This module is part of the PyASB project, 
created and maintained by Miguel Nievas [UCM].
____________________________
'''

__author__ = "Miguel Nievas"
__copyright__ = "Copyright 2012, PyASB project"
__credits__ = ["Miguel Nievas"]
__license__ = "GNU GPL v3"
__shortname__ = "PyASB_analysis"
__longname__ = "Python All-Sky Brightness pipeline analysis"
__version__ = "1.99.0"
__maintainer__ = "Miguel Nievas"
__email__ = "miguelnr89[at]gmail[dot]com"
__status__ = "Prototype" # "Prototype", "Development", or "Production"



try:
	# System
	import sys,os,inspect
	import signal
	
	# Multiproccessing
	import multiprocessing
	import subprocess
	
	# Data and plot manipulation
	import matplotlib as mpl
	import matplotlib.pyplot as plt
	import matplotlib.colors as mpc
	import matplotlib.patches as mpp
	import matplotlib.dates as mpd
	import matplotlib.cm as cm
	import scipy
	import scipy.stats
	import scipy.interpolate
	import numpy as np
	import datetime
	import ephem
	
	# Data fitting
	from kapteyn import kmpfit
	
	# URL processing
	import urllib
	
	# Aux functions from PyASB
	from pyasb_launcher import get_config_filename
	from read_config import *
	from image_info import *
	from astrometry import *
except:
	print("Analysis, "+str(inspect.stack()[0][2:4][::-1])+\
	 ': One or more modules missing')
	raise


''' Max number of points to analyze for each filter'''
max_number_points = 10000


''' Hard coded base path '''
base_path = './Data/'

'''
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~ Halt handler ~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''

def handler(signum, frame):
	print 'Signal handler called with signal', signum
	print "CTRL-C pressed"
	sys.exit(0)

signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)


'''
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~ Exec Function in verbose mode ~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''

def verbose(function, *args):
	'''
	Run a function in verbose mode
	'''
	try:
		out = function(*args)
	except:
		# Something happened while runing function
		raise
		if DEBUG==True:
			print(str(inspect.stack()[0][2:4][::-1])+' Error')
			raise
	else:
		return(out)


'''
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~ Config file loading ~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''

class PlatformHelp():
	def __init__(self):
		self.make_title()
		self.make_welcome()
		self.make_requisites()
		self.make_options()
	
	def make_title(self):
		nameversion = 10*'#'+3*' '+__shortname__+' v'+__version__+3*' '+10*'#'
		self.separator = len(nameversion)*'#'
		self.title = nameversion
	
	def make_welcome(self):
		self.welcome = 'Welcome to '+__shortname__+' ('+__longname__+')\n'
	
	def make_requisites(self):
		self.requisites = \
			'Requisites: Python 2.7; Scipy; Numpy;\n'+\
			'  Matplotlib; Pyfits; Pyephem\n\n'
	
	def make_options(self):
		self.options = \
			'-h: print this help message\n\n'+\
			'-b base_dir: \n'+\
			'  Use alternative base dir for the input/output\n'+\
			'-c config_file: \n'+\
			'  Use alternative config file\n\n'
	
	def show_help(self):
		print(\
		 self.separator+'\n'+self.title+'\n'+self.separator+'\n'+self.welcome+\
		 self.requisites+self.options+self.separator)
		
	def incorrect_parameter(self,parameter):
		print('ERROR. Incorrect parameter: '+str(parameter))
		self.show_help()
	
	def date_or_file_input_error(self):
		print('ERROR. Date or file input')
		self.show_help()
	
	def no_parameters_error(self):
		print('ERROR. No input parameters especified')
		self.show_help()


class ReadOptions():
	def __init__(self,input_options):
		# Lambda: http://docs.python.org/release/2.5.2/tut/node6.html [4.7.5]
		self.options = {\
		 '-h': lambda: 'show_help',\
		 '-c': lambda: 'use_configfile',\
		 '-b': lambda: 'use_basedir'}
		
		# By default, we wont show on screen nor save on disk.
		self.configfile = False;
		self.show_help = False;
		
		print('Input Options: '+str(input_options))
		
		self.input_options = input_options
		try: self.input_options[1]
		except Exception as e: 
			print(str(inspect.stack()[0][2:4][::-1])+'ERR. No imput options')
			self.no_parameters()
		else:
			while len(self.input_options)>1:
				input_option = self.options.get(self.input_options[1], lambda : None)()
				if input_option == 'show_help':
					self.show_help = True
					# Stop reading options. Program will halt
					self.input_options = []
				elif input_option == 'use_configfile':
					self.configfile = self.reference_file()
				elif input_option == 'use_basedir':
					self.base_path = self.reference_file()
				else:
					self.input_options.pop(1)
				
	def no_parameters(self):
		print('\nERR: Need more than one parameter')
		self.input_options = []
		self.show_help = True
	
	def reference_file(self):
		print('Path specified with '+self.input_options[1]+'. Extracting path')
		file_reference = None
		try: self.input_options[2]
		except:
			self.input_options.remove(self.input_options[1])
		else:
			if self.options.get(self.input_options[2], lambda : None)():
				self.input_options.remove(self.input_options[1])
			else:
				file_reference=self.input_options[2]
				self.input_options.remove(self.input_options[2]) 
				self.input_options.remove(self.input_options[1])
				return(file_reference)


def load_config_file(config_file):
	''' 
	Open the config file
	This will set-up the observatory properties 
	such as Latitude and Longitude of the Observatory
	'''
	ConfigOptions_ = ConfigOptions(config_file)
	class FakeInputOptions:
		void = False
	ImageInfoCommon = ImageInfo()
	ImageInfoCommon.config_processing_common(ConfigOptions_,FakeInputOptions)

	# Some parameters that should have a default value if not in the config file
	ImageInfoCommon.limits_sb = [None,None]
	ImageInfoCommon.limits_extinction = [None,None]
	ImageInfoCommon.limits_colorcolor = [None,None]
	ImageInfoCommon.limits_extext = [None,None]
	ImageInfoCommon.limits_dates = [None,None]
	ImageInfoCommon.limits_goodnightsclouds=[0,1./3]
	ImageInfoCommon.valueerror_cornerbias=[0,0]		
	
	for attribute in ConfigOptions_.FileOptions:
		if 'pyanalysis_limits' in attribute[0] \
		or 'pyanalysis_valueerror' in attribute[0]:
			attribute[0] = attribute[0].replace('pyanalysis_','')
			attribute[1] = attribute[1].replace('[','').replace(']','')
			attribute[1] = attribute[1].split(',')
			exec('val1='+attribute[1][0]) in locals()
			exec('val2='+attribute[1][1]) in locals()
			attribute[1]=[val1,val2]
			vars(ImageInfoCommon)[attribute[0]]=[val1,val2]
	
	return(ImageInfoCommon)


''' 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~ Auxiliary functions  ~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''

'''
----------------------------------------------------
The following functions will check for common errors
that may be present in the data
----------------------------------------------------
'''

def validate_datalength(data):
	''' Does the X data length math Y data length? '''
	try:
		assert(len(data.X) == len(data.Y))
		if data.errors_x == True:
			assert(len(data.Xerr) == len(data.X))
		if data.errors_y == True:
			assert(len(data.Yerr) == len(data.Y))
	except Exception as EXC:
		raise
		print(str(inspect.stack()[0][2:4][::-1])+\
		 str(type(EXC)))

def validate_floatdataY(data):
	''' Is the Y data of float/int type '''
	for each_value in data.Y:
		assert(type(each_value)==float or type(each_value)==int)
	
	if data.errors_y == True:
		for each_value in data.Yerr:
			assert(type(each_value)==float or type(each_value)==int)

def validate_floatdataX(data):
	''' Is the X data of float/int type '''
	for each_value in data.X:
		assert(type(each_value)==float or type(each_value)==int)
	
	if data.errors_x == True:
		for each_value in data.Xerr:
			assert(type(each_value)==float or type(each_value)==int)

def split_value_error(value_error):
	''' 
	If input is given as value +/- error, split them 
	'''
	value_error_sep = "+/-"
	return([float(value) for value in value_error.split("+/-")])


def extract_column(matrix,column):
	'''
	Extract the selected column in the matrix and give it as an array
	'''
	return([matrix[k][column] for k in xrange(len(matrix))])


def match_dates_array(DateTime1,DateTime2,max_absoffset=1200):
	'''
	Match two arrays with dates with a Max absolute offset of max_absoffset. 
	Returns the bi-directional match between date and the mean datetime.
	'''
	
	match_dates = {}
	mean_dates = {}
	time_deltas = {}
	
	for index1,date1 in enumerate(DateTime1):
		time_deltas[index1] = datetime.timedelta(days=1e5)
		for index2,date2 in enumerate(DateTime2):
			timedelta = date1-date2
			if abs(timedelta).days*86400+abs(timedelta).seconds<max_absoffset:
				try:
					abs(timedelta)<abs(time_deltas[index1])
				except:
					pass
				else:
					match_dates[index1] = index2
					mean_dates[index1]  = date1+timedelta/2
					time_deltas[index1] = timedelta
	
	return(match_dates,mean_dates)


def DensityWeighted(x,bins=10):
	'''
	Weight the values with their reversed density.
	gives more weight to lonely values.
	'''
	hist,bin_edges = np.histogram(x,bins)
	bin_edges[-1] += 1e-10
	xweight = np.array(\
	 [hist[np.array(xi>=bin_edges[:-1])*np.array(xi<bin_edges[1:])]\
	 for xi in x])
	return(xweight)


'''
--------------------------
Least-Squares Data FITTING
--------------------------
'''

class DataFit():
	def remove_outliers(self,data,sigma=1,iterat=1):
		'''
		Least SQuare fit are sensitive to outliers.
		We need to remove them assuming that at short x-scale
		the data doesnt change dramatically.
		'''
		
		x, y = data[:2]
		
		bins = int(10**(max(1,(np.log10(len(x))-1.5))))
		hist,bin_edges = np.histogram(x,bins)
		bin_edges[-1] += 1e-10
		
		filteroutlier = np.ones(len(x),dtype=bool)
		orders = np.arange(0,len(x),1)
		print(np.sum(filteroutlier)),
		
		for k in xrange(len(hist)):
			# Elements in the selected x range
			elements = np.array(x>=bin_edges[:-1][k])*np.array(x<bin_edges[1:][k])
			
			# Outlier detection
			median  = np.median(y[elements])
			desvest = np.std(y[elements])
			thefilter = np.abs(y[elements]-median)>sigma*desvest
			# The elements of the original array that are detected as outliers
			selectedorders = orders[elements]
			filteredorders = selectedorders[thefilter]
			filteroutlier[filteredorders] = False
		
		print(np.sum(filteroutlier)),
		print(np.size(hist))
		
		for k in xrange(len(data)):
			data[k] = data[k][filteroutlier]
		
		if iterat>1:
			data = self.remove_outliers(data,sigma,iterat-1)
		
		return(data)
		
		
	
	def mean_fit(self,p,data):
		''' Perform a mean fit '''
		x, y = data[:2]
		self.model = 'y = p[0]'
		return(y-p[0])

	def linear_residuals(self,p,data):
		''' Perform a linear LSQ fit '''
		x, y = data[:2] # Data arrays is a tuple given by programmer
		self.model = 'y =a+b*x' 
		return (y-(p[0]+p[1]*x))
	
	def cuadratic_residuals(self,p,data):
		''' Perform a cuadratic LSQ fit '''
		x, y = data[:2]
		self.model = 'y = p[0]+p[1]*x+p[2]*x**2' 
		return (y-(p[0]+p[1]*x-p[2]*x**2))
	
	def polynomial_xoffset(self,p,data):
		''' Perform a polynomial fit centered on xc '''
		x, y = data[:2]
		self.model = 'y = '
		for number in xrange(1,len(p)):
			self.model = self.model+'p['+str(number)+']*(x-p[0])**'+str(number-1)
			if number<len(p)-1:
				self.model = self.model+' + '
		
		xoff = np.array(x-p[0])
		poly = np.array([p[number]*xoff**(number-1) for number in xrange(1,len(p))])
		return(y-sum(poly))
	
	def polynomial(self,p,data):
		''' Perform a polynomial fit centered on xc '''
		x, y = data[:2]
		self.model = 'y = '
		for number in xrange(len(p)):
			self.model = self.model+'p['+str(number)+']*(x)**'+str(number)
			if number<len(p)-1:
				self.model = self.model+' + '
		
		self.derivmodel = 'deriv = np.array(['
		for number in xrange(len(p)):
			self.derivmodel = self.derivmodel+'(x)**'+str(number)
			if number<len(p)-1:
				self.derivmodel = self.derivmodel+','
		self.derivmodel += '])'
		
		poly = np.array([p[number]*x**(number) for number in xrange(len(p))])
		return(y-sum(poly))
	
	def gaussian_residuals(self,p,data):
		''' Perform a gaussian fit of the data '''
		x,y = data[:2]
		self.model = 'y = p[0]+p[1]*np.exp(-((x-p[2])**2)/(2*p[3]**2))'
		self.derivmodel = \
		 'deriv = np.array(['+\
		 'x*0+1,np.exp(-(x-p[2])**2 /(2*p[3]**2)),'+\
		 'p[1]*np.exp(-(x-p[2])**2 /(2*p[3]**2))*((x-p[2])/(p[3]**2)),'+\
		 'p[1]*np.exp(-(x-p[2])**2 /(2*p[3]**2))*((x-p[2])/(p[3]**3))])'
		exec(self.model.replace('y = ','ymodel = ')) in locals(),globals()
		return(y-ymodel)
		
	def sinusoidal_fit(self,p,data):
		''' Perform a sinusoidal LSQ fit of the data '''
		
		x, y = data[:2]
		a,b,c,d = p
		self.model = 'y = p[0]+p[1]*np.sin(p[2]*x+p[3])'
		self.derivmodel = \
		 'deriv = np.array(['+\
		 'x*0+1,np.sin(p[2]*x+p[3]),'+\
		 'x*np.cos(p[2]*x+p[3]),'+\
		 'np.cos(p[2]*x+p[3])])'
		exec(self.model.replace('y = ','ymodel = ')) in locals(),globals()
		return(y-ymodel)
	
	def data_fit(self,data,residuals,params0):
		'''
		Perform the fit with kapteyn module.
		Returns fit parameters, chi2 ... .
		'''
		
		# If no weights given, assign ones to them.
		try: data[2]
		except: data = [data[0],data[1],np.ones(np.size(data[0]))]
		
		# Remove outliers
		data = self.remove_outliers(data,2,iterat=2)
		
		self.fitsobj = kmpfit.Fitter(residuals=residuals, params0=params0, data=data)
		self.fitsobj.fit()
		
		self.comment = ''
		self.xdata = data[0]
		self.ydata = data[1]
		self.yweight = data[2]
		self.params = self.fitsobj.params
		self.xerror = self.fitsobj.xerror
		self.stderr = self.fitsobj.stderr
		self.chi2_min = self.fitsobj.chi2_min
		self.rchi2_min = self.fitsobj.rchi2_min
		self.niter = self.fitsobj.niter
		self.nfree = self.fitsobj.nfree
		self.dof = self.fitsobj.dof
	
	def data_confidence_band(self,xmodel=None,confprob=0.95,abswei=False):
		'''
		Return the confidence bands.
		NOTE: The model must have the derivatives !!!
		'''
		
		if xmodel==None:
			xmodel = np.linspace(np.min(self.xdata),np.max(self.xdata),1000)
		
		# Create a synthetic model to accomodate kapteyn format.
		def synthetic_model(p,x):
			#p = self.params
			exec(self.model) in locals(),globals() # y=f(x,p)
			return(y)
		
		p = self.params
		x = xmodel
		exec(self.derivmodel) in locals(),globals() # get deriv = 
		
		self.xmodel = xmodel
		self.ymodel,self.upperband,self.lowerband = \
		 self.fitsobj.confidence_band(xmodel,deriv,confprob,synthetic_model)
		
	def add_comment(self,comment=''):
		'''Add a comment '''
		self.comment = self.comment+str(comment)+'; '
	
	def print_results(self,callback=sys.stdout):
		''' Print results to callback (screen/file) '''
		callback.write(\
		"\nSummary from Least-Squares fit:"+'\n'+\
		 "================================="+'\n'+\
		 "Model:					  "+str(self.model)+'\n'+\
		 "Comment:					"+str(self.comment)+'\n'+\
		 "Best-fit parameters:		"+str(self.params)+'\n'+\
		 "Asymptotic error:		   "+str(self.xerror)+'\n'+\
		 "Error assuming red.chi^2=1: "+str(self.stderr)+'\n'+\
		 "Chi^2 min:				  "+str(self.chi2_min)+'\n'+\
		 "Reduced Chi^2:			  "+str(self.rchi2_min)+'\n'+\
		 "Iterations:				 "+str(self.niter)+'\n'+\
		 "Number of free pars.:	   "+str(self.nfree)+'\n'+\
		 "Degrees of freedom:		 "+str(self.dof)+'\n'\
		)
	

'''
----------------------------------------------------
Process the Date
----------------------------------------------------
'''

def datetime_from_number(date_number):
	''' 
	get Datetime from number
	date_number must be in the format [YYYYMMDD,hhmmss]
	ex. [20120101,023521]
	'''
	date_number[0] = int(date_number[0])
	date_number[1] = int(date_number[1])
	
	Year  = int(date_number[0]/10000)
	Month = int((date_number[0] - Year*10000)/100)
	Day   = int(date_number[0] - Year*10000 - Month*100)
	
	Hour   = int(date_number[1]/10000)
	Minute = int((date_number[1] - Hour*10000)/100)
	Second = int(date_number[1] - Hour*10000 - Minute*100)
	
	TheDate = datetime.datetime(Year,Month,Day,Hour,Minute,Second)
	return(TheDate)

def juliandate_from_number(date_number):
	''' 
	get Julian date from number
	date_number must be in the format [YYYYMMDD,hhmmss]
	ex. [20120101,023521]
	'''
	TheDateTime = datetime_from_number(date_number)
	JulianDate = 2415018.5 + ephem(TheDateTime)
	return(JulianDate)

def get_season(thedate):
	# seasons dict
	seasonsnumber = {'Spring':0,'Summer':1,'Autumn':2,'Winter':3}
	
	# Needs datetime as input.
	# Get the current year and convert to string so it will play nicely with PyEphem
	currentYear = str(thedate.year)
	
	# Get next year in order to cover the gap between the first day of the winter
	# solstice and the end of the calendar year
	nextYear = str(thedate.year + 1)
	
	# Get the current LOCAL date and time from PyEphem (because I'm too lazy to parse 
	# stuff) so we can determine which season we're in. This is very important because
	# PyEphem reports everything in UTC.
	
	#currentlocaltime = ephem.datetime(thedate)
	#currentlocaltime = datetime.datetime(thedate)
	currentlocaltime = thedate#.datetime()
	
	# Get the previous winter solstice expressed as the local date
	lastWinter = ephem.date(ephem.previous_winter_solstice(currentYear)).datetime()
	
	# Get the current year's equinoxes and solstices expressed as the local date/time
	beginSpring = ephem.localtime(ephem.next_vernal_equinox(currentYear))
	beginSummer = ephem.localtime(ephem.next_summer_solstice(currentYear))
	beginAutumn = ephem.localtime(ephem.next_autumnal_equinox(currentYear))
	beginWinter = ephem.localtime(ephem.next_winter_solstice(currentYear))
	
	# Get the vernal equinox for the following year so we can cover through the end of
	# the current year
	futureSpring = ephem.localtime(ephem.next_vernal_equinox(nextYear))
	
	# Determine the season
	if lastWinter < currentlocaltime < beginSpring:
		season = 'Winter'
	if beginSpring < currentlocaltime < beginSummer:
		season = 'Spring'
	if beginSummer < currentlocaltime < beginAutumn:
		season = 'Summer'
	if beginAutumn < currentlocaltime < beginWinter:
		season = 'Autumn'
	if beginWinter < currentlocaltime < futureSpring:
		season = 'Winter'
	
	# Return the season
	return(seasonsnumber[season])
	#return(season)

'''
--------------------------------
Sky conditions on the given date
--------------------------------
'''

def galactic_latlon_point(az,alt,thedatetime,ImageInfo):
	'''
	Calculate the Galactic latitude and longitude
	of any position in the sky for the given 
	observatory position in the Earth and a given datetime
	'''
	
	ImageInfo.date_string = str(thedatetime).replace("-","/")
	ObsPyephem = pyephem_setup_real(ImageInfo)
	
	ra,dec = ObsPyephem.radec_of(az,alt)
	point_ephem = ephem.readdb("'Point,f|S|A0,"+str(ra)+"|0.0,"+str(dec)+"|0.0,0,2000.0'")
	point_ephem.compute(ObsPyephem)
	point_galactic_coordinates = ephem.Galactic(point_ephem)
	gal_lat = float(point_galactic_coordinates.lat)*180/np.pi
	gal_lon = float(point_galactic_coordinates.lon)*180/np.pi
	return(gal_lat,gal_lon)

def galactic_latlon_zenith(thedatetime,ImageInfo):
	'''
	Calculate the Galactic latitude and longitude
	of the zenith at a given datetime
	'''
	gal_lat,gal_lon = galactic_latlon_point('0','90',thedatetime,ImageInfo)
	return(gal_lat,gal_lon)


def ecliptic_latlon_point(az,alt,thedatetime,ImageInfo):
	'''
	Calculate the Ecliptic latitude and longitude
	of any position in the sky for the given 
	observatory position in the Earth and a given datetime
	'''
	
	ImageInfo.date_string = str(thedatetime).replace("-","/")
	ObsPyephem = pyephem_setup_real(ImageInfo)
	
	ra,dec = ObsPyephem.radec_of(az,alt)
	point_ephem = ephem.readdb("'Point,f|S|A0,"+str(ra)+"|0.0,"+str(dec)+"|0.0,0,2000.0'")
	point_ephem.compute(ObsPyephem)
	point_ecliptic_coordinates = ephem.Ecliptic(point_ephem)
	ecl_lat = float(point_ecliptic_coordinates.lat)*180/np.pi
	ecl_lon = float(point_ecliptic_coordinates.lon)*180/np.pi
	return(ecl_lat,ecl_lon)

def ecliptic_latlon_zenith(thedatetime,ImageInfo):
	'''
	Calculate the Ecliptic latitude and longitude
	of the zenith at a given datetime
	'''
	ecl_lat,ecl_lon = ecliptic_latlon_point(0,90,thedatetime,ImageInfo)
	return(ecl_lon,ecl_lat)


def moon_position_calculator(thedatetime,ImageInfo):
	''' 
	Return the Moon position (azimuth and altitude) 
	for a given date and Observatory
	'''
	ImageInfo.date_string = str(thedatetime).replace("-","/")
	ObsPyephem = pyephem_setup_real(ImageInfo)
	themoon = ephem.Moon()
	themoon.compute(ObsPyephem)
	return(themoon.az*180./np.pi,themoon.alt*90./np.pi)

def moon_phase_calculator(thedatetime,ImageInfo):
	'''
	Calculate the moon phase (in days after last new moon)
	for a given datetime.
	'''
	prev_new_moon = ephem.previous_new_moon(thedatetime)
	next_new_moon = ephem.next_new_moon(thedatetime)
	
	phase = thedatetime-prev_new_moon.datetime()
	
	return(phase.days+phase.seconds/86400.)

def moon_illumination_calculator(thedatetime,ImageInfo):
	'''
	Estimate the moon illumination %
	'''
	prev_new_moon = ephem.previous_new_moon(thedatetime)
	next_new_moon = ephem.next_new_moon(thedatetime)
	
	phase = thedatetime-prev_new_moon.datetime()
	phaselength = next_new_moon.datetime()-prev_new_moon.datetime()
	
	phase_float = phase.days+phase.seconds/86400.
	phaselength_float = phaselength.days+phaselength.seconds/86400.
	
	illumination = phase_float*1./(phaselength_float/2.)
	if illumination>1:
		illumination = 2.-illumination
	
	return(illumination)

def moon_zdist_calculator(thedatetime,ImageInfo):
	'''
	Return the Moon zenithal distance for a given date
	and Observatory
	'''
	moon_alt = moon_position_calculator(thedatetime,ImageInfo)[1]
	return(90.-moon_alt)

def solar_activity_radiation(column):
	'''
	Estimate mean Solar Activity in the XUV range (1-70A)
	'''
	
	sdo_filename = base_path+'/sdo_daily_data.txt'
	sdo_datafile = open(sdo_filename)
	sdo_data = sdo_datafile.read()
	
	sdo_data = sdo_data.split('\n')
	sdo_data = [line for line in sdo_data if line!='']
	sdo_data = [line for line in sdo_data if line[0]!='#']
	
	def adapt_csv_string(string):
		# Remove extra spaces, endline symbols and so
		string = string.replace('\r\n','')
		string = string.replace('\n','')
		string = string.replace('\t',' ')
		while 2*' ' in string:
			string = string.replace('  ',' ')
		return(string)
	
	def convert_csv_to_floatarray(string):
		array = string.split(',')
		
		YYYYMMDD = array[0].split("-")
		year = int(YYYYMMDD[0])
		month = int(YYYYMMDD[1])
		day = int(YYYYMMDD[2])
		
		array[0] = datetime.date(year,month,day)
		
		for item in xrange(1,len(array)):
			'''
			try:
				value,error = array[item].split('+/-')
			except:
				try:
					value = float(array[item])
					error = None
				except:
					value = None
					error = None
			else:
				try:
					value = float(value)
					error = float(error)
				except:
					value = None
					error = None
			array[item]=[value,error]
			'''
			try:
				array[item] = [float(value) for value in split_value_error(array[item])]
			except:
				array[item] = [None,None]
		
		return(array)
	
	vector_convert_csv_to_floatarray = np.vectorize(convert_csv_to_floatarray)
	
	sdo_data = vector_convert_csv_to_floatarray(sdo_data)
	sdo_xuvflux_data = np.array(extract_column(sdo_data,column))
	sdo_dates = np.array(extract_column(sdo_data,0))
	date_xuv = dict(zip(sdo_dates,sdo_xuvflux_data))
	
	return(date_xuv)


'''
----------------------------------------------------
Process the Data
----------------------------------------------------
'''

def sigma_clipping_median_limits(data_array_raw,sigmasdown=3,sigmasup=3):
	'''
	Returns a sigma clipping limits of the given array data
	for a given asymmetrical 
	'''
	
	data_array = np.ma.masked_invalid(data_array_raw)   # Remove invalid data
	
	values_median = np.median(data_array)
	values_std = np.std(data_array)+1e-10
	
	if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+': '+str(values_median))
	if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+': '+str(values_std))
	
	values_min = 1.0*int((values_median-sigmasdown*values_std)*10-0.5)/10.0
	values_max = 1.0*int((values_median+sigmasup*values_std)*10+0.5)/10.0
	
	# Check if limits are outside of the data range and clip.
	#values_min = max(values_min,min(data_array_raw))
	#values_max = min(values_max,max(data_array_raw))

	return(values_min,values_max)


def sigma_clipping(fdata,sigmaX=0.,sigmaY=0.,max_sigmaX=3.,max_sigmaY=3.):
	'''
	sigma = 0 means that no filter will be applied to the data
	'''
	
	if (max_sigmaX==0.0 and max_sigmaY==0.0):
		print(str(inspect.stack()[0][2:4][::-1])+\
		 ': Dont perform sigma filtering')
		return(fdata)
	else:
		''' Create the filter matrix of Trues and Falses '''
		matrix_filter = np.array([np.True_]*\
		 np.size(fdata.X)).reshape(np.array(fdata.X).shape)
		
		if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+\
		 ' Before sigma clipping, '+str(len(fdata.X))+' values can be used.')
		
		if ((type(fdata.Y[0])==float or type(fdata.Y[0])==np.float64)\
		 and max_sigmaX!=0. and sigmaX!=0.):
			matrix_filter = matrix_filter*\
			 np.array(abs(np.array(fdata.X)-np.array(fdata.Xpred))<=abs(max_sigmaX*sigmaX))
			if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+str(matrix_filter))
		
		if ((type(fdata.Y[0])==float or type(fdata.Y[0])==np.float64)\
		 and max_sigmaY!=0. and sigmaY!=0.):
			matrix_filter = matrix_filter*\
			 np.array(abs(np.array(fdata.Y)-np.array(fdata.Ypred))<=abs(max_sigmaY*sigmaY))
			if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+str(matrix_filter))
		
		
		if fdata.errors_x == True:
			matrix_filter = matrix_filter*\
			 np.array(np.array(fdata.Xerr)<=abs(sigmaX))
			if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+str(matrix_filter))
		
		if fdata.errors_y == True:
			matrix_filter = matrix_filter*\
			 np.array(np.array(fdata.Yerr)<=abs(sigmaY))
			if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+str(matrix_filter))
		
		''' Filter the data '''
		
		fdata.Y = np.array(fdata.Y)[matrix_filter]
		fdata.X = np.array(fdata.X)[matrix_filter]
		if fdata.errors_x == True:
			fdata.Xerr = np.array(fdata.Xerr)[matrix_filter]
		if fdata.errors_y == True:
			fdata.Yerr = np.array(fdata.Yerr)[matrix_filter]
			
		if DEBUG==True: print('After sigma clipping, '+str(len(fdata.X))+' values can be used.')
		
		''' Return filtered data '''
		return(fdata)


'''
----------------------------------------------------
Analysis classes
----------------------------------------------------
'''


class Data():
	def check_uncertainties(self,data):
		# Check if data contains uncertainties
		
		try: assert(data.Xerr!=None)
		except: 
			data.errors_x = False
			data.Xerr = None
		else: data.errors_x = True
		
		try: assert(data.Yerr!=None)
		except: 
			data.errors_y = False
			data.Yerr = None
		else: data.errors_y = True
		
		try:
			validate_datalength(data)
			validate_floatdataY(data)
		except Exception as EXC:
			if DEBUG==True:
				print(str(inspect.stack()[0][2:4][::-1])+\
				 str(type(EXC)))
		
	def check_validdata(self,data):
		pass
	
		'''
		def create_mask_invalid(value_list):
			#Create a mask of invalid data
			mask_isnan = np.isnan(value_list)
			mask_isinf = np.isinf(value_list)
			mask_total = mask_isnan + mask_isinf
			return(mask_total)
		
		def create_mask_valid(value_list):
			return(~create_mask_invalid(value_list))
		
		def check_if_float(value_list):
			try: np.array(value_list,dtype=np.float)
			except: return(False)
			else: return(True)
		
		
		thefilter = np.array([True for item in data.X])
		
		try: assert(check_if_float(data.X)==True)
		except: pass
		else: thefilter = thefilter*create_mask_valid(data.X)
		
		try: assert(check_if_float(data.Xerr)==True and data.Xerr!=None)
		except: pass
		else: thefilter = thefilter*create_mask_valid(data.Xerr)
		
		try: assert(check_if_float(data.Y)==True)
		except: pass
		else: thefilter = thefilter*create_mask_valid(data.Y)
		
		try: assert(check_if_float(data.Yerr)==True and data.Yerr!=None)
		except: pass
		else: thefilter = thefilter*create_mask_valid(data.Yerr)
		
		try: assert(check_if_float(data.Z)==True)
		except: pass
		else: thefilter = thefilter*create_mask_valid(data.Z)
		
		try: assert(check_if_float(data.Zerr)==True and data.Yerr!=None)
		except: pass
		else: thefilter = thefilter*create_mask_valid(data.Zerr)
		
		# Apply the filter		
		try: assert(data.X != None)
		except: pass
		else: data.X = np.array(data.X)[thefilter]
		
		try: assert(data.Y != None)
		except: pass
		else: data.Y = np.array(data.Y)[thefilter]
		
		try: assert(data.Z != None)
		except: pass
		else: data.Z = np.array(data.Z)[thefilter]
		
		try: assert(data.Xerr != None)
		except: pass
		else: data.Xerr = np.array(data.Xerr)[thefilter]
		
		try: assert(data.Yerr != None)
		except: pass
		else: data.Yerr = np.array(data.Yerr)[thefilter]
		
		try: assert(data.Zerr != None)
		except: pass
		else: data.Zerr = np.array(data.Zerr)[thefilter]'''

class MedianFit():
	def __init__(self,data):
		''' 
		When data object with data.X, data.Y 
		and possibly data.Xerr, data.Yerr;
		compute the median and give it as output
		'''
		
		# 5% data discarded up and down the mean.
		self.confidence_level = 0.90
		
		# Perform the fit
		self.Fit(data)
	
	def Fit(self,data):
		''' Fit the data to median and compute
			confidence limits '''
		
		self.median_values  = np.median(data.Y)
		self.desvest_values = np.std(data.Y)
		self.downlimit_values,self.uplimit_values = \
		 scipy.stats.norm.interval(\
		  self.confidence_level,\
		  self.median_values,\
		  self.desvest_values)
		  
		self.median_error = 0.5*(self.uplimit_values-self.downlimit_values)
		
		self.downlimit_values = max(self.downlimit_values,min(data.Y))
		self.uplimit_values = min(self.uplimit_values,max(data.Y))
		

class Histogram():
	def __init__(self,data,bins=10,weights=None,normalize=False):
		''' Create the Matplotlib figure '''
		self.thefigure = plt.figure(figsize=(12,6))
		self.thefigure.subplots_adjust(top=0.8)
		self.thegraph = self.thefigure.add_subplot(111)
		''' Perform an histogram for the given data '''
		self.create_histogram(data,bins,weights,normalize)
	
	def create_histogram(self,data,bins=10,weights=None,normalize=False):
		'''
		Numpy histogram
		For the given data.Y, calculate the histogram
		self.data_histogram: 2D array of [bins],[counts]
		'''
		self.data_histogram  = np.histogram(data.Y,bins,weights)
		
		if normalize==True:
			histogram = np.array(self.data_histogram)
			histogram_min = np.min(histogram[1][:-1][self.data_histogram[0]!=0])-1e-10
			histogram_max = np.max(histogram[1][1:][self.data_histogram[0]!=0])
			print('Max/Min histogram values: %.3f,%.3f' %(histogram_max,histogram_min))
			histogram[1] = (histogram[1]-histogram_min)/(histogram_max-histogram_min)
			self.data_histogram = tuple(histogram)
		
		''' 
		np.histogram generates a tuple like (10),(11). 
		The following code gets the central X value
		'''
		
		cumulative_x = 0.5*(self.data_histogram[1][1:]+self.data_histogram[1][:-1])
		cumulative_y = np.cumsum(self.data_histogram[0])
		cumulative_y = cumulative_y*1./np.max(cumulative_y)
		
		histogram_x  = 0.5*(self.data_histogram[1][1:]+self.data_histogram[1][:-1])
		histogram_y  = self.data_histogram[0]
		self.bin_separation = histogram_x[1]-histogram_x[0]
		
		self.data_histogram  = [histogram_y,histogram_x]
		self.cumul_histogram = [cumulative_y,histogram_x]
		
		if DEBUG==True:
			print(str(inspect.stack()[0][2:4][::-1])+\
			 ' Histogram:\n'+str(self.data_histogram[0])+'\n'+\
			 str(self.data_histogram[1]))
	
	def title_and_labels(self,fdata):
		'''
		Set the title and axis labels given as input
		'''
		self.thegraph.set_title(fdata.title+'\n',size='x-large')
		self.thegraph.set_xlabel(fdata.xlabel)
		self.thegraph.set_ylabel(fdata.ylabel)
	
	def plot_histogramdata(self,data):
		try:
			data.yerr
		except:
			data.yerr=None
		histogram = self.thegraph.bar(\
		 self.data_histogram[1],self.data_histogram[0],\
		 yerr=data.yerr,width=self.bin_separation,\
		 color=data.color, alpha=0.5,align='center',label='histogram')
		
		self.thegraph2 = self.thegraph.twinx()
		
		cumhistogram = self.thegraph2.plot(\
		 self.cumul_histogram[1], self.cumul_histogram[0],\
		 'k-', marker=None, label='cumulative')
		
		lines, labels = self.thegraph.get_legend_handles_labels()
		lines2, labels2 = self.thegraph2.get_legend_handles_labels()
		self.thegraph.legend(lines + lines2, labels + labels2, loc=0)
	
	def plot_histogram(self,data):
		self.title_and_labels(data)
		self.plot_histogramdata(data)
	
	def show_plot(self):
		plt.show()
	
	def save_fig(self,savepath):
		self.thefigure.savefig(savepath,bbox_inches='tight')


class Graph(Data):
	def __init__(self,width=12,height=6):
		''' Create the Matplotlib figure '''
		self.thefigure = plt.figure(figsize=(width,height))
		self.thefigure.subplots_adjust(top=0.8)
		#self.thegraph = plt.host_subplot(111,axes_class = AA.Axes)
		self.thegraph = self.thefigure.add_subplot(111)
	
	def scatter_plot(self,fdata,maxerrorbars=10):
		'''
		Data (fdata) must be previously formatted.
		'''
		
		self.thegraph.scatter(x=fdata.X,y=fdata.Y,marker='.',color=str(fdata.colorpoints))
		
		if maxerrorbars>0:
			''' Plot errorbars '''
			
			def selected_items(values,each=1):
				''' Choose only 1/each values from the data ''' 
				return([values[k] for k in xrange(len(values))\
				if k%int(len(values)/each +1)==0])
			
			X = selected_items(fdata.X,maxerrorbars)
			Y = selected_items(fdata.Y,maxerrorbars)
			Xerr = None
			Yerr = None
			
			if fdata.errors_x==True:
				Xerr = selected_items(fdata.Xerr,maxerrorbars)
			if fdata.errors_y==True:
				Yerr = selected_items(fdata.Yerr,maxerrorbars)
			
			try:
				self.thegraph.set_ylim(fdata.ymin,fdata.ymax)
			except:
				if DEBUG==True:
					print(str(inspect.stack()[0][2:4][::-1])+\
					": dont set ylims to the graph") 
			
			self.thegraph.errorbar(x=X,y=Y,xerr=Xerr,yerr=Yerr,fmt='+',\
			capsize=5,elinewidth=1.5,color=fdata.color)
	
	def clip_data(self,fdata):
		try:
			self.thegraph.set_xlim(fdata.xmin,fdata,xmax)
			#self.thegraph.set_xlim(np.percentile(fdata.X,[0,100]))
		except:
			if DEBUG==True: 
				print(str(inspect.stack()[0][2:4][::-1])+\
				 'Cannot clip X data')
		
		try:
			self.thegraph.set_xlim(fdata.ymin,fdata,ymax)
			#self.thegraph.set_ylim(np.percentile(fdata.Y,[0,100]))
		except:
			if DEBUG==True: 
				print(str(inspect.stack()[0][2:4][::-1])+\
				 'Cannot clip Y data')
	
	
	def set_xtickaxis(self,fdata,maxerrorbars=10):
		''' 
		Adjust the ticks to represent dates on X axis
		and Data on Y axis.
		'''
		try:
			fdata.x2ticks
			fdata.x2values
		except:
			print(str(inspect.stack()[0][2:4][::-1])+' dont set xticks')
		else:
			try:
				self.thegraph.set_xticks(fdata.x2ticks)
				self.thegraph.set_xticklabels(fdata.x2values)
			except:
				print(str(inspect.stack()[0][2:4][::-1])+\
				 ' Error found seting X ticks')
				raise
		
	
	def title_and_labels(self,fdata):
		'''
		Set the title and axis labels given as input
		'''
		self.thegraph.set_title(fdata.title+'\n',size='x-large')
		self.thegraph.set_xlabel(fdata.xlabel)
		self.thegraph.set_ylabel(fdata.ylabel)
	
	def xaxis_dates(self,fdata):
		'''
		If fdata.X is a valid datetime, extract the length (date max - date min) and
		decide if the mayor and minor ticks will be put on Years, Months or Days.
		'''
		
		try:
			assert(type(fdata.X) == datetime.datetime or type(fdata.X) == datetime.date)
		except:
			print(str(inspect.stack()[0][2:4][::-1])+' X is not a datetime type')
			#raise
		
		total_delta_date = max(datetime.datetime)-min(datetime.datetime)
		total_delta_days = total_delta_date.days
		
		'''
		years	= mpd.YearLocator()   # every year
		months   = mpd.MonthLocator()  # every month
		days	 = mpd.DayLocator()	# every day
		hours	= mpd.HourLocator()   # every hour
		
		if total_delta_days > 366:
			self.xaxis.set_major_locator(years)
			self.xaxis.set_major_formatter(mpd.DateFormatter('%Y'))
			self.xaxis.set_minor_locator(months)
		elif 31<total_delta_days<=366:
			self.xaxis.set_major_locator(months)
			self.xaxis.set_major_formatter(mpd.DateFormatter('%Y-%b'))
			self.xaxis.set_minor_locator(days)
		elif 1<total_delta_days<=31:
			self.xaxis.set_major_locator(days)
			self.xaxis.set_major_formatter(mpd.DateFormatter('%Y-%b-%d'))
			self.xaxis.set_minor_locator(hours)
		'''
		
		self.date_locator = dates.AutoDateLocator()
		self.date_formatter = dates.AutoDateFormatter(self.date_locator)
	
	def plot_scatter_data(self,fdata,maxerrorbars=10):
		''' Check if data contains uncertainties '''
		self.check_uncertainties(fdata)
		# Plot the data
		self.scatter_plot(fdata,maxerrorbars)
		# Make Titles and labels
		self.title_and_labels(fdata)
	
	def plot_scatter_fit(self,fdata):
		''' Plot the data fit '''
		datafit_exec_cmd = fdata.fitmodel.replace("^","**")
		p = fdata.fitparam   # Get parameters of the fit
		x_min = min(fdata.X) # Min value of the data
		x_max = max(fdata.X) # Max value of the data
		x_num = 1000		 # Number of points for model fit plot
		x = np.linspace(x_min,x_max,x_num)  # create the X value set to calculate the model 
		exec(datafit_exec_cmd) # estimated values from model --> y = f(x)
		
		fdata.Xmodel = x
		fdata.Ymodel = y
		
		self.thegraph.plot(x,y,'k-')
	
	def plot_scatter_2dhistogram(\
	 self,fdata,bins=20,drawpoints=True,\
	 contours=False,contourf=False,\
	 alpha_contours=1,alpha_contourf=1):
		''' Check if data contains uncertainties '''
		self.check_uncertainties(fdata)
		# Plot the data
		# Histogram
		
		Xi = fdata.X
		Yi = fdata.Y
		
		Xistime = False
		Yistime = False
		
		if type(Xi[0])==datetime.time:
			Xi = np.array([Xi[k].hour*3600+Xi[k].minute*60+Xi[k].second for k in xrange(len(Xi))])
			try: assert(fdata.Xtimeticks=='hours')
			except: pass
			else: Xi = Xi/3600.
			try: assert(fdata.Xtimeticks=='minutes')
			except: pass
			else: Xi = Xi/60.
			Xistime = True
		
		if type(Yi[0])==datetime.time:
			Yi = np.array([Yi[k].hour*3600+Yi[k].minute*60+Yi[k].second for k in xrange(len(Yi))])
			try: assert(fdata.Ytimeticks=='hours')
			except: pass
			else: Yi = Yi/3600.
			try: assert(fdata.Ytimeticks=='minutes')
			except: pass
			else: Yi = Yi/60.
			Yistime = True
		
		valid_types = [float,int,np.float16,np.int16,np.float32,np.int32,np.float64,np.int64]
		
		try:
			#assert(type(Xi[0]) in valid_types)
			#assert(type(Yi[0]) in valid_types)
			Xi = np.array(Xi,dtype=np.float64)
			Yi = np.array(Yi,dtype=np.float64)
			mask_nan = np.array(Xi==np.nan)+np.array(Yi==np.nan)
			mask_inf = np.array(abs(Xi)==np.inf)+np.array(abs(Yi)==np.inf)
			mask = mask_nan+mask_inf
			Xi = np.array(Xi[~mask])
			Yi = np.array(Yi[~mask])
		except:
			print(str(inspect.stack()[0][2:4][::-1])+\
			 ' X or Y not a float/int type, but '+str(type(Xi[0]))+' and '+str(type(Yi[0])))
			raise
		
		thebins = [np.linspace(min(Xi),max(Xi),bins),np.linspace(min(Yi),max(Yi),bins)]
		try:
			thehistogram,xedges,yedges = np.histogram2d(Xi, Yi, bins=thebins)
		except:
			thehistogram,xedges,yedges = np.histogram2d(Xi, Yi)
		
		xedges = 0.5*(xedges[1:]+xedges[:-1])
		yedges = 0.5*(yedges[1:]+yedges[:-1])
		
		'''if Xistime==True:
			hours = np.array(xedges,dtype=int)/3600
			minutes = np.array(xedges,dtype=int)/60 - hours*60
			seconds = np.array(xedges,dtype=int) - hours*3600 - minutes*60
			xedges = np.array([datetime.time(hours[k],minutes[k],seconds[k]) for k in xrange(len(xedges))])
		
		if Yistime==True:
			hours = np.array(yedges,dtype=int)/3600
			minutes = np.array(yedges,dtype=int)/60 - hours*60
			seconds = np.array(yedges,dtype=int) - hours*3600 - minutes*60
			yedges = np.array([datetime.time(hours[k],minutes[k],seconds[k]) for k in xrange(len(yedges))])'''
		
		
		class gdata:
			X = Xi
			Y = Yi
			color = fdata.color
			colormap = fdata.colormap
			colorpoints= fdata.colorpoints
			title = fdata.title
			xlabel = fdata.xlabel
			ylabel = fdata.ylabel
		
		class hdata:
			X = xedges
			Y = yedges
			Z = thehistogram
			color = fdata.color
			colormap = fdata.colormap
			colorpoints= fdata.colorpoints
			zlabel = 'Density of points'
			title = fdata.title
			xlabel = fdata.xlabel
			ylabel = fdata.ylabel
		
		self.plot_contours(hdata,alpha_contours,alpha_contourf,contours,contourf)
		
		if drawpoints==True:
			self.scatter_plot(gdata,maxerrorbars=0)
	
	def plot_median_fit(self,fdata,maxerrorbars=10):
		''' Check if data contains uncertainties '''
		self.plot_scatter_data(fdata,maxerrorbars)
		
		'''
		Perform the complete median of of the data
		'''
		data_fit = MedianFit(fdata)
		#self.secondxaxis(fdata)
		
		# horizontal lines for median, upper limit and lower limit
		self.thegraph.axhline(data_fit.median_values,color='k',linestyle='-')
		self.thegraph.axhline(data_fit.uplimit_values,color='k',linestyle='--')
		self.thegraph.axhline(data_fit.downlimit_values,color='k',linestyle='--')
		
		# Median and limit text labels
		self.thegraph.text(min(fdata.X),data_fit.uplimit_values,\
		 str('%.1f' %float(data_fit.confidence_level*100))+"%",fontsize='small')
		self.thegraph.text(min(fdata.X),data_fit.downlimit_values,\
		 str('%.1f' %float(data_fit.confidence_level*100))+"%",fontsize='small')
		self.thegraph.text(max(fdata.X),data_fit.uplimit_values,\
		 str('+%.3f' %float(data_fit.uplimit_values-data_fit.median_values)),fontsize='small')
		self.thegraph.text(max(fdata.X),data_fit.downlimit_values,\
		 str('-%.3f' %float(data_fit.median_values-data_fit.downlimit_values)),fontsize='small')
		self.thegraph.text(max(fdata.X),data_fit.median_values,\
		 str('%.3f' %float(data_fit.median_values)),fontsize='small')
	
	def plot_contours(self,fdata,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True):
		''' Check if data contains uncertainties '''
		#self.check_uncertainties(fdata)
		# Plot the data
		#self.scatter_plot(fdata,maxerrorbars)
		
		if contours==True:
			contours_ = self.thegraph.contour(fdata.X,fdata.Y,fdata.Z.transpose(),colors='k',alpha=alpha_contours)
			contourlabels_ = self.thegraph.clabel(contours_,inline=True,fmt='%.1f',fontsize=9)
		
		if contourf==True:
			contoursf_ = self.thegraph.contourf(fdata.X,fdata.Y,fdata.Z.transpose(),cmap=fdata.colormap,alpha=alpha_contourf)
			# Color bar
			cbar_ = self.thefigure.colorbar(contoursf_,orientation='vertical',shrink=0.85)		
			cbar_.ax.set_ylabel(fdata.zlabel)
	
	def fix_aspect_ratio(self):
		self.thegraph.set_aspect('equal',adjustable='box-forced')
		
	def show_plot(self):
		plt.show()
	
	def save_fig(self,savepath):
		self.thefigure.savefig(savepath,bbox_inches='tight')







class SummaryData():
	def __init__(self,summary_text_data):
		''' 
		- Provides:
		self.DateTime (type list of datetime.datetime)
		self.Date (type list of datetime.date)
		self.Time (type list of datetime.time)
		self.FilterName (type list of strings, p.e. 'Johnson V')
		self.PhotStars (type list of ints, number of stars with photometric data
		self.PercStars (type list of floats, % of stars used in the bouguer fit)
		self.ZeroPoint (type list of [float,float], calculated ZP)
		self.Extinction (type list of [float,float], calculated extinction)
		self.CloudCoverage (type list of [float,float], Cloud coverage %)
		'''
		
		num_points = len(summary_text_data)
		each_ = int(num_points/max_number_points)+1
		items = xrange(0,num_points,each_)
		#items = xrange(len(summary_text_data))
		
		TextDate = [summary_text_data[k][0].split("_")[0] for k in items]
		TextTime = [summary_text_data[k][0].split("_")[1] for k in items]
		self.DateTime = np.array([datetime.datetime(\
		 int(TextDate[k][0:4]),\
		 int(TextDate[k][4:6]),\
		 int(TextDate[k][6:8]),\
		 int(TextTime[k][0:2]),\
		 int(TextTime[k][2:4]),\
		 int(TextTime[k][4:6])) for k in xrange(len(items))])
		self.Date = np.array([datetime.date(\
		 int(TextDate[k][0:4]),\
		 int(TextDate[k][4:6]),\
		 int(TextDate[k][6:8])) for k in xrange(len(items))])
		self.Time = np.array([datetime.time(\
		 int(TextTime[k][0:2]),\
		 int(TextTime[k][2:4]),\
		 int(TextTime[k][4:6])) for k in xrange(len(items))])
		self.FilterName = np.array([summary_text_data[k][1].replace(" ","") for k in items])
		self.PhotStars  = np.array([int(summary_text_data[k][2]) for k in items])
		self.PercStars  = np.array([float(summary_text_data[k][3]) for k in items])
		self.ZeroPoint  = np.array([split_value_error(summary_text_data[k][4]) for k in items])
		self.Extinction = np.array([split_value_error(summary_text_data[k][5]) for k in items])
		self.ZenithSB   = np.array([split_value_error(summary_text_data[k][6]) for k in items])
		self.CloudCoverage = np.array([split_value_error(summary_text_data[k][7]) for k in items])
		self.CloudCoverage[:,0][self.CloudCoverage[:,0]<0] = 1
		
		# Cloud coverage stretch range
		percentile05 = np.percentile(self.CloudCoverage[:,0],5)
		percentile95 = np.percentile(self.CloudCoverage[:,0],95)
		self.CloudCoverage[:,0] = np.clip(self.CloudCoverage[:,0],percentile05,percentile95)
		self.CloudCoverage[:,0] = self.CloudCoverage[:,0]-np.min(self.CloudCoverage[:,0])
		self.CloudCoverage[:,0] = self.CloudCoverage[:,0]*1./np.max(self.CloudCoverage[:,0])
		
		self.FilterLambdas = {\
		 'Johnson_U':366.3,\
		 'Johnson_B':436.1,\
		 'Johnson_V':544.8,\
		 'Johnson_R':641.1,\
		 'Johnson_I':798.0,
		}
		
		self.FilterDeltaLambdas = {\
		 'Johnson_U':650,\
		 'Johnson_B':890,\
		 'Johnson_V':840,\
		 'Johnson_R':1580,\
		 'Johnson_I':1540,
		}

class SummariesAnalysis(Data):
	def __init__(self,base_path):
		''' Load summaries from a given base_path '''
		self.search_summaries(base_path)
		self.load_summaries()
		self.extract_data_summaries()
		
		self.base_path = base_path
		self.results_file = self.base_path+'/analysis_results.txt'
		
	
	'''
	-------------------------------------------------------------
	The following methods will be executed as soon as
	the SummaryAnalysis object is created.
	
	They basically load every data found in PyASB summary files
	found in a given base path.
	-------------------------------------------------------------
	'''
	
	def load_bias_data(self,filenames):
		bias_content = []
		
		for eachfile in filenames:
			bias_file = open(eachfile,'r')
			bias_rawcontent = bias_file.readlines()
			bias_file.close()
			
			num_points = len(bias_rawcontent)
			each_ = int(num_points/max_number_points)+1
			
			for line in xrange(0,num_points,each_):
				bias_content.append(bias_rawcontent[line])
		
		bias_content = np.array(\
		 [line.replace('\n','').replace('\r','').split(',')\
		 for line in bias_content])
		bias_dates = [\
		 eachdate.replace('/','').replace(':','').split(' ')\
		 for eachdate in bias_content[:,0]]
		self.bias_dates = np.array([\
		 datetime_from_number(eachdate)\
		 for eachdate in bias_dates])
		self.bias_filters = np.array(bias_content[:,1])
		self.bias_value = np.array(bias_content[:,2],dtype='float')
		self.bias_error = np.array(bias_content[:,3],dtype='float')
	
	def search_files(self,base_path,pattern):
		''' Search files recursively that match a given pattern '''
		foundfiles = []
		for root, dirs, files in os.walk(base_path):
			for file_ in files:
				if pattern in file_:
					foundfiles.append(os.path.join(root, file_))
		
		if (DEBUG==True): print(str(len(foundfiles))+' files loaded')
		return(foundfiles)
	
	def search_summaries(self,base_path):
		''' Search iteratively for summaries files starting in base_path '''
		self.foundfiles = self.search_files(base_path,pattern='Summary')
	
	def search_biasfile(self,base_path):
		''' Search iteratively for bias files starting in base_path '''
		self.biasfiles = self.search_files(base_path,pattern='measured_image_bias')
	
	def load_summaries(self):
		self.summary_data_separator = ','
		''' Load each summary data into an array (unformatted text elements)'''
		self.founddata = []
		for each_file in self.foundfiles:
			filecontent = open(each_file).readlines()
			for line in filecontent:
				if line[0].replace(" ","")!='#':
					self.founddata.append(line.split(self.summary_data_separator))
		
		if (DEBUG==True): print(str(len(self.founddata))+' data lines loaded')
	
	def extract_data_summaries(self):
		'''
		Format the data (separate values, errors, convert to float/int the values)
		'''
		self.summarydata = SummaryData(self.founddata)
		
		# Simple filter to remove invalid data (values of NSB=-1)
		self.select_data_nsb(minnsb=0,maxnsb=None)
		
	
	'''
	----------------------------------------------------------
	The following objects are optional.
	The user must decide what type of analysis want to do
	with the already loaded data
	----------------------------------------------------------
	'''
	
	'''
	--------------
	Data Filtering
	--------------
	'''
	
	def apply_filter_pattern(self,filterpattern):
		''' 
		- Updates:
		self.DateTime (type list of datetime.datetime)
		self.Date (type list of datetime.date)
		self.Time (type list of datetime.time)
		self.FilterName (type list of strings, p.e. 'Johnson V')
		self.PhotStars (type list of ints, number of stars with photometric data
		self.PercStars (type list of floats, % of stars used in the bouguer fit)
		self.ZeroPoint (type list of [float,float], calculated ZP)
		self.Extinction (type list of [float,float], calculated extinction)
		self.CloudCoverage (type list of [float,float], Cloud coverage %)
		'''
		
		self.summarydata.DateTime = self.summarydata.DateTime[filterpattern]
		self.summarydata.Date = self.summarydata.Date[filterpattern]
		self.summarydata.Time = self.summarydata.Time[filterpattern]
		self.summarydata.PhotStars = self.summarydata.PhotStars[filterpattern]
		self.summarydata.PercStars = self.summarydata.PercStars[filterpattern]
		self.summarydata.ZeroPoint = self.summarydata.ZeroPoint[filterpattern]
		self.summarydata.Extinction = self.summarydata.Extinction[filterpattern]
		self.summarydata.ZenithSB = self.summarydata.ZenithSB[filterpattern]
		self.summarydata.CloudCoverage = self.summarydata.CloudCoverage[filterpattern]
		self.summarydata.FilterName = self.summarydata.FilterName[filterpattern]
		
	def select_data_filter(self,filtername):
		''' 
		Get the complete (unfiltered) data
		'''
		self.extract_data_summaries()
		
		# Filter the data
		if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+\
		 ' Selected filter: '+str(filtername))
		filterpattern = np.array(self.summarydata.FilterName==filtername)
		self.apply_filter_pattern(filterpattern)
	
	def apply_bias_filter(self,filterpattern):
		'''
		Apply filterpattern to bias data
		'''
		self.bias_dates = self.bias_dates[filterpattern]
		self.bias_value = self.bias_value[filterpattern]
		self.bias_error = self.bias_error[filterpattern]
	
	def select_bias_filter(self,filtername):
		'''
		Select bias data by filter
		'''
		self.load_bias_data(self.biasfiles)
		filterpattern = np.array(self.bias_filters==filtername)
		self.apply_bias_filter(filterpattern)
		
	def create_filter_bydate(self,dates,startdate,enddate):
		'''
		Create filterpattern by date
		'''
		filterpattern = np.array([True for item in dates])
		if startdate != None:
			filterpattern = filterpattern*np.array(np.array(dates)>startdate)
		if enddate != None:
			filterpattern = filterpattern*np.array(np.array(dates)<enddate)
		return(filterpattern)
	
	
	def select_data_bydate(self,startdate=None,enddate=None):
		'''
		Filter data by date
		'''
		filterpattern = self.create_filter_bydate(self.summarydata.DateTime,startdate,enddate)
		self.apply_filter_pattern(filterpattern)
	
	def select_bias_bydate(self,startdate=None,enddate=None):
		'''
		Filter bias data by date
		'''
		filterpattern = self.create_filter_bydate(self.bias_dates,startdate,enddate)
		self.apply_bias_filter(filterpattern)
	
	def select_data_prefilter_bydate(self,ImageInfoCommon):
		'''
		Set the date filters from the config file for the summary data
		'''
		try:
			date1 = ImageInfoCommon.limits_dates[0]
			date2 = ImageInfoCommon.limits_dates[1]
			if date1 != None: date1 = datetime_from_number([date1,120000])
			if date2 != None: date2 = datetime_from_number([date2,120000])
			self.select_data_bydate(date1,date2)
		except:
			raise
	
	def select_biasdata_prefilter_bydate(self,ImageInfoCommon):
		'''
		Set the date filters from the config file for the bias data
		'''
		try:
			date1 = ImageInfoCommon.limits_dates[0]
			date2 = ImageInfoCommon.limits_dates[1]
			if date1 != None: date1 = datetime_from_number([date1,120000])
			if date2 != None: date2 = datetime_from_number([date2,120000])
			self.select_bias_bydate(date1,date2)
		except:
			raise
		
	
	def extract_color_filter(self,filter1,filter2,ImageInfo,\
	 filterdata_date=True,filterdata_clouds=False,filterdata_moon=False):
		'''
		Extract colors and the mean datetime.
		'''
		
		self.select_data_filter(filter1)
		if filterdata_date==True:
			self.select_data_prefilter_bydate(ImageInfo)
		if filterdata_clouds==True:
			self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],\
			 ImageInfo.limits_goodnightsclouds[1])
		elif filterdata_clouds=='NoOvercast':
			self.select_data_clouds(0,0.75)
		if filterdata_moon==True:
			self.select_moon_filter(ImageInfo,0,0.25,90,180)
			
		
		DateTime1 = self.summarydata.DateTime
		Date1 = self.summarydata.Date
		Time1 = self.summarydata.Time
		PhotStar1 = self.summarydata.PhotStars
		PercStars1 = self.summarydata.PercStars
		ZeroPoint1 = self.summarydata.ZeroPoint
		Extinction1 = self.summarydata.Extinction
		ZenithSB1 = self.summarydata.ZenithSB
		CloudCoverage1 = self.summarydata.CloudCoverage
		FilterName1 = self.summarydata.FilterName
		
		self.select_data_filter(filter2)
		if filterdata_clouds==True:
			self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		if filterdata_moon==True:
			self.select_moon_filter(ImageInfo,0,0.25,90,180)
		
		DateTime2 = self.summarydata.DateTime
		Date2 = self.summarydata.Date
		Time2 = self.summarydata.Time
		PhotStar2 = self.summarydata.PhotStars
		PercStars2 = self.summarydata.PercStars
		ZeroPoint2 = self.summarydata.ZeroPoint
		Extinction2 = self.summarydata.Extinction
		ZenithSB2 = self.summarydata.ZenithSB
		CloudCoverage2 = self.summarydata.CloudCoverage
		FilterName2 = self.summarydata.FilterName
		
		length = min(len(DateTime1),len(DateTime2))
		filterpattern = [False for index in xrange(length)]
		
		self.summarydata.DateTime = [] # None
		self.summarydata.Date = [] # None
		self.summarydata.Time = []
		self.summarydata.Extinction = [] # None
		self.summarydata.ZenithSB = [] # None
		self.summarydata.CloudCoverage = [] # None
		
		match_dates,mean_dates = match_dates_array(DateTime1,DateTime2)
		
		for index1 in match_dates.keys():
			index2 = match_dates[index1]
			#print(DateTime1[index1])
			#print(DateTime2[index2])
			self.summarydata.DateTime.append(mean_dates[index1])
			self.summarydata.Date.append(mean_dates[index1].date())
			self.summarydata.Time.append(mean_dates[index1].time())
			self.summarydata.Extinction.append(\
			 [Extinction1[index1],Extinction2[index2]])
			self.summarydata.ZenithSB.append(\
			 [ZenithSB1[index1],ZenithSB2[index2]])
			self.summarydata.CloudCoverage.append(\
			 [CloudCoverage1[index1],CloudCoverage2[index2]])
			
		colors = \
		 np.array(self.summarydata.ZenithSB)[:,0,0]-\
		 np.array(self.summarydata.ZenithSB)[:,1,0]
		
		colorserr = np.sqrt(\
		 np.array(self.summarydata.ZenithSB)[:,0,1]**2+\
		 np.array(self.summarydata.ZenithSB)[:,1,1]**2)
		
		self.summarydata.Colors = [colors,colorserr]
		
		extinction1_value = np.array(self.summarydata.Extinction)[:,0,0]
		extinction1_error = np.array(self.summarydata.Extinction)[:,0,1]
		extinction2_value = np.array(self.summarydata.Extinction)[:,1,0]
		extinction2_error = np.array(self.summarydata.Extinction)[:,1,1]
		
		extinction_slope = (np.log10(extinction1_value)-np.log10(extinction2_value))/\
		 (np.log10(self.summarydata.FilterLambdas[filter1])-\
		  np.log10(self.summarydata.FilterLambdas[filter2]))
		
		extinction_slope_err = np.sqrt(\
		 (extinction1_error/extinction1_value)**2 +\
		 (extinction2_error/extinction2_value)**2)/\
		 (np.log(10)*(\
		  np.log10(self.summarydata.FilterLambdas[filter1])-\
		  np.log10(self.summarydata.FilterLambdas[filter2])))
		
		self.summarydata.ExtinctionSlope = [extinction_slope,extinction_slope_err]
	
	
	def select_data_clouds(self,mincover=0.0,maxcover=0.2):
		'''
		Filter the data to select clear nights.
		'''
		
		if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+\
		 ' Select only <0.4 cloud coverage nights')
		filterpattern = np.array(self.summarydata.CloudCoverage[:,0]>=mincover)
		filterpattern = filterpattern*np.array(self.summarydata.CloudCoverage[:,0]<=maxcover)
		self.apply_filter_pattern(filterpattern)
	
	def select_data_nsb(self,minnsb=None,maxnsb=None):
		'''
		Filter the data to select clear nights.
		'''
		if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+\
		 ' Select only data in a given NSB interval')
		
		filterpattern = np.ones(np.size(self.summarydata.ZenithSB[:,0]),dtype=bool)
		try: assert(int(minnsb)!=None)
		except: pass
		else: filterpattern = filterpattern*np.array(self.summarydata.ZenithSB[:,0]>=minnsb)
		
		try: assert(int(maxnsb)!=None)
		except: pass
		else: filterpattern = filterpattern*np.array(self.summarydata.ZenithSB[:,0]<=maxnsb)
		
		self.apply_filter_pattern(filterpattern)
	
	def select_moon_filter(self,ImageInfo,min_illumination,max_illumination,min_zd,max_zd):
		'''
		Filter the data to select nights without moon (dark nights).
		'''
		
		vect_moon_illumination_calculator = np.vectorize(moon_illumination_calculator)
		vect_moon_zdist_calculator = np.vectorize(moon_zdist_calculator)
		
		datetimes = self.summarydata.DateTime
		moon_illumination_values = vect_moon_illumination_calculator(datetimes,ImageInfo)
		moon_zenithaldist_values = vect_moon_zdist_calculator(datetimes,ImageInfo)
		
		filterpattern = np.array(moon_illumination_values>=min_illumination)
		filterpattern = filterpattern*np.array(moon_illumination_values<=max_illumination)
		
		filterpattern2 = np.array(moon_zenithaldist_values>=min_zd)
		filterpattern2 = filterpattern2*np.array(moon_zenithaldist_values<=max_zd)
		
		self.apply_filter_pattern(filterpattern+filterpattern2)
	
	'''
	------------------------------------------
	Basic analysis (direct results from PyASB)
	------------------------------------------
	'''
	
	def zeropoint_date_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Zero Point Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = self.summarydata.DateTime
			Y = extract_column(self.summarydata.ZeroPoint,0)
			Yerr = extract_column(self.summarydata.ZeroPoint,1)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)

		print('Performing Zero Point MedianFit and graph for filter '+filtername)
		
		for iteration in xrange(2):
			data_evolution = MedianFit(AnalysisData)
			
			# Needs a sigma filtering of the data.
			AnalysisData.Ypred = \
			 [data_evolution.median_values]*len(AnalysisData.Y)
			AnalysisData.Xpred = AnalysisData.X
			AnalysisData = sigma_clipping(\
			 fdata = AnalysisData,\
			 sigmaX = 0,\
			 sigmaY = data_evolution.median_error,\
			 max_sigmaX = 2,\
			 max_sigmaY = 2)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'Zero Point (mag/arcsec2)'
		AnalysisData.title  = 'Zero Point long-term evolution - '+str(filtername)
		
		data_evolgraph = Graph()
		data_evolgraph.plot_median_fit(AnalysisData,maxerrorbars=30)
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_date_zp_'+filtername+'.png')
		
		# Histogram graph
		AnalysisData.xlabel = 'Zero Point (mag/arcsec2)'
		AnalysisData.ylabel = 'Counts'
		AnalysisData.title  = 'Zero Point histogram - '+str(filtername)
		
		data_histogram = Histogram(AnalysisData,bins=20)
		data_histogram.plot_histogram(AnalysisData)
		if DEBUG==True: data_histogram.show_plot()
		data_histogram.save_fig(\
		 base_path+'/summary_zp_hist_'+filtername+'.png')
		
		# Median Fit
		print(\
		 'ZeroPoint Median Fit: '+\
		 str('%.3f' %data_evolution.median_values)+' +/- '+\
		 str('%.3f' %data_evolution.median_error)
		)
		
		return(0)
		
		
	def extinction_date_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Extinction Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = self.summarydata.DateTime
			Y = extract_column(self.summarydata.Extinction,0)
			Yerr = extract_column(self.summarydata.Extinction,1)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75;
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)

		print('Performing Extinction MedianFit and graph for filter '+filtername)
		
		for iteration in xrange(2):
			data_evolution = MedianFit(AnalysisData)
			
			# Needs a sigma filtering of the data.
			AnalysisData.Ypred = \
			 [data_evolution.median_values]*len(AnalysisData.Y)
			AnalysisData.Xpred = AnalysisData.X
			AnalysisData = sigma_clipping(\
			 fdata = AnalysisData,\
			 sigmaX = 0,\
			 sigmaY = data_evolution.median_error,\
			 max_sigmaX = 2,\
			 max_sigmaY = 2)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'Extinction Point (mag/arcsec2)'
		AnalysisData.title  = 'Extinction long-term evolution - '+str(filtername)
		
		data_evolgraph = Graph()
		data_evolgraph.plot_median_fit(AnalysisData,maxerrorbars=30)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_date_extinction_'+filtername+'.png')
		
		# Histogram graph
		AnalysisData.xlabel = 'Extinction (mag/arcsec2)'
		AnalysisData.ylabel = 'Counts'
		AnalysisData.title  = 'Extinction histogram - '+str(filtername)
		
		data_histogram = Histogram(AnalysisData,bins=20)
		data_histogram.plot_histogram(AnalysisData)
		data_histogram.thegraph.set_xlim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		
		if DEBUG==True: data_histogram.show_plot()
		data_histogram.save_fig(\
		 base_path+'/summary_extinction_hist_'+filtername+'.png')
		
		# Median Fit
		print(\
		 'Extinction Median Fit: '+\
		 str('%.3f' %data_evolution.median_values)+' +/- '+\
		 str('%.3f' %data_evolution.median_error)
		)
		
		return(0)
			
	
	def extinction_vs_zeropoint(self,filtername,ImageInfo):
		''' 
		Perform the Zero Point Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = extract_column(self.summarydata.ZeroPoint,0)
			Xerr = extract_column(self.summarydata.ZeroPoint,1)
			Y = extract_column(self.summarydata.Extinction,0)
			Yerr = extract_column(self.summarydata.Extinction,1)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Extinction vs ZP for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Zero Point (mag/arcsec2)'
		AnalysisData.ylabel = 'Extinction (mag/airmass)'
		AnalysisData.title  = 'Extinction vs Zero Point - '+str(filtername)
		
		xyplot = Graph()
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		xyplot.thegraph.set_ylim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_zp_extinction_'+filtername+'.png')
		
		return(0)
		
	
	def skybrightness_date_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Extinction Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = self.summarydata.DateTime
			Y = extract_column(self.summarydata.ZenithSB,0)
			Yerr = extract_column(self.summarydata.ZenithSB,1)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)

		print('Performing Zenith NSB MedianFit and graph for filter '+filtername)
		
		for iteration in xrange(2):
			data_evolution = MedianFit(AnalysisData)
			
			# Needs a sigma filtering of the data.
			AnalysisData.Ypred = \
			 [data_evolution.median_values]*len(AnalysisData.Y)
			AnalysisData.Xpred = AnalysisData.X
			AnalysisData = sigma_clipping(\
			 fdata = AnalysisData,\
			 sigmaX = 0,\
			 sigmaY = data_evolution.median_error,\
			 max_sigmaX = 2,\
			 max_sigmaY = 2)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB long-term evolution - '+str(filtername)
		
		data_evolgraph = Graph()
		data_evolgraph.plot_median_fit(AnalysisData,maxerrorbars=30)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_date_zenithnsb_'+filtername+'.png')
		
		# Histogram graph
		AnalysisData.xlabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.ylabel = 'Counts'
		AnalysisData.title  = 'Zenith NSB histogram - '+str(filtername)
		
		data_histogram = Histogram(AnalysisData,bins=20)
		data_histogram.plot_histogram(AnalysisData)
		data_histogram.thegraph.set_xlim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		if DEBUG==True: data_histogram.show_plot()
		data_histogram.save_fig(\
		 base_path+'/summary_zenithnsb_hist_'+filtername+'.png')
		
		# Median Fit
		print(\
		 'Zenith NSB Median Fit: '+\
		 str('%.3f' %data_evolution.median_values)+' +/- '+\
		 str('%.3f' %data_evolution.median_error)
		)
		
		return(0)
	
	
	def cloudcoverage_date_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Cloud Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		#self.select_data_clouds(0,1)
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = self.summarydata.DateTime
			Y = extract_column(self.summarydata.CloudCoverage,0)
			Y_max = np.max(Y)
			Y_min = np.min(Y)
			print('Max/Min Cloud coverage: %.2f,%.2f' %(Y_max,Y_min))
			Y = (Y - Y_min)/(Y_max-Y_min)
			#Yerr = extract_column(self.summarydata.CloudCoverage,1)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Cloud Coverage vs Date for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'Cloud Coverage (fraction)'
		AnalysisData.title  = 'Cloud Coverage long-term evolution - '+str(filtername)
		
		data_evolgraph = Graph()
		data_evolgraph.plot_median_fit(AnalysisData,maxerrorbars=30)
		data_evolgraph.thegraph.set_ylim(0,1)
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_date_clouds_'+filtername+'.png')
		
		# Histogram graph
		AnalysisData.xlabel = 'Cloud Coverage (fraction)'
		AnalysisData.ylabel = 'Counts'
		AnalysisData.title  = 'Cloud Coverage histogram - '+str(filtername)
		
		data_histogram = Histogram(AnalysisData,bins=10,normalize=True)
		data_histogram.plot_histogram(AnalysisData)
		data_histogram.thegraph.set_xlim(0,1)
		
		if DEBUG==True: data_histogram.show_plot()
		data_histogram.save_fig(\
		 base_path+'/summary_clouds_hist_'+filtername+'.png')
		 
		'''
		CloudHistogram = Histogram(AnalysisData,bins=20,normalize=True)
		
		freq,clouds = CloudHistogram.data_histogram
		
		self.Fit = DataFit()
		
		freq_clear = freq[clouds<0.6]
		clouds_clear = clouds[clouds<0.6]
		data = [clouds_clear,freq_clear]
		params0 = [1,1,1,1]
		self.Fit.data_fit(data,self.Fit.gaussian_residuals,params0)
		print(self.Fit.params)
		width_clear = self.Fit.params[3]
		center_clear = self.Fit.params[3]
		self.clear_nights_cut = center_clear+width_clear
		
		freq_overcast = freq[clouds>0.6]
		clouds_overcast = clouds[clouds<0.6]
		data = [clouds_overcast,freq_overcast]
		params0 = [1,1,1,1]
		self.Fit.data_fit(data,self.Fit.gaussian_residuals,params0)
		width_overcast = self.Fit.params[3]
		center_overcast = self.Fit.params[3]
		self.overcast_nights_cut = center_overcast-width_overcast
		
		print('Cloud coverage cuts in %.2f and %.2f' \
		 %(self.clear_nights_cut,self.overcast_nights_cut))
		'''
		
		return(0)
	
	def cloudcoverage_monthly_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Cloud Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		#self.select_data_clouds(0,1)
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xraw = np.array(self.summarydata.DateTime)
			Xraw = Xraw - datetime.timedelta(hours=12) # We want to join PM and AM data.
			Xraw = np.array([Item.date() for Item in Xraw])
			Zraw = np.array(extract_column(self.summarydata.CloudCoverage,0))
			Zraw_min = np.min(Zraw)
			Zraw_max = np.max(Zraw)
			print('Max/Min Cloud coverage: %.2f,%.2f' %(Zraw_max,Zraw_min))
			Zraw = (Zraw - Zraw_min)/(Zraw_max-Zraw_min)
			
			# Days with cloud coverage data
			Xdays = np.unique(Xraw)[1:]
			# Months with available data
			Xmonths = np.unique([Item.replace(day=1) for Item in Xdays])
			
			Y = ['Clear','Partly cloudy','Overcast']
			#Colors = ['green','yellow','red']
			Color = ['black']*3
			Colors = ['green','yellow','red']
			Line = ['-','--',':']
			bins = [0,1./3.,2./3.,1]
			
			# Promediate values of cloud coverage in the same night.
			Zmeanday = np.zeros(np.size(Xdays))
			
			# for each day...
			for xi in xrange(len(Xdays)):
				Zmeanday[xi] = np.mean(Zraw[Xraw==Xdays[xi]])
			
			# Get the histogram for each month.
			Zmonths = np.zeros((np.size(Xmonths),3))
			Ztotal = np.zeros(np.size(Xmonths))
			
			for mi in xrange(len(Xmonths)):
				Zmonths[mi] = np.histogram(Zmeanday[\
				 np.array([Item.replace(day=1) for Item in Xdays]) == Xmonths[mi]],\
				 bins=bins)[0]
				Ztotal[mi] = np.sum(Zmonths[mi])
			
			X = Xmonths
			Z = Zmonths
			
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		
		print('Ploting Cloud Coverage vs Date for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'Fraction'
		AnalysisData.title  = 'Monthly Cloud Coverage evolution - '+str(filtername)
		
		
		# Plot
		
		data_evolgraph = Graph()
		
		for cloudindex,cloudfraction in enumerate(AnalysisData.Y):
			x=AnalysisData.X
			y=AnalysisData.Z[:,cloudindex]*1./AnalysisData.Ztotal
			yerr=y*(1-y)/(np.sqrt(AnalysisData.Ztotal))
			data_evolgraph.thegraph.plot_date(\
			 x,y,\
			 color=AnalysisData.Color[cloudindex],marker='',\
			 label=AnalysisData.Y[cloudindex],\
			 linestyle=AnalysisData.Line[cloudindex])
			data_evolgraph.thegraph.errorbar(x,y,yerr=yerr)
		
		data_evolgraph.thegraph.set_ylim(0,1)
		data_evolgraph.thegraph.legend()
		data_evolgraph.title_and_labels(AnalysisData)
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_monthly_clouds_'+filtername+'.png')
		
		
		# Histogram
		
		table_text = "Date; "
		for element in AnalysisData.Y:
			table_text += str(element)+"; "
		table_text += "\r\n"
		
		for line in xrange(len(AnalysisData.X)):
			data_line = str(AnalysisData.X[line])[:-3]+"; "
			for element in AnalysisData.Z[line]:
				data_line += str(int(element))+"; "
			table_text += data_line+"\r\n"
		
		output_table = open(base_path+'/summary_monthly_clouds_'+filtername+'.txt','w+')
		output_table.write(table_text)
		output_table.close()
		
		data_evolgraph2 = Graph()
		
		width=datetime.timedelta(days=7)
		
		for cloudindex,cloudfraction in enumerate(AnalysisData.Y):
			x = AnalysisData.X+width*(cloudindex-1)
			y = AnalysisData.Z[:,cloudindex]*1./AnalysisData.Ztotal
			yerr=y*(1-y)/(np.sqrt(AnalysisData.Ztotal))
			data_evolgraph2.thegraph.bar(\
			 x,y,yerr=yerr,ecolor='k',\
			 label=AnalysisData.Y[cloudindex],\
			 width=7, color=AnalysisData.Colors[cloudindex])
		
		data_evolgraph2.thegraph.set_ylim(0,1)
		data_evolgraph2.thegraph.legend()
		data_evolgraph2.title_and_labels(AnalysisData)
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph2.save_fig(base_path+'/summary_monthly_cloudsbar_'+filtername+'.png')
		
		
	
	
	def extinction_vs_cloudcoverage(self,filtername,ImageInfo):
		''' 
		Perform the Zero Point Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		#self.select_data_clouds(0,1)
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = extract_column(self.summarydata.CloudCoverage,0)
			#Xerr = extract_column(self.summarydata.CloudCoverage,1)
			Y = extract_column(self.summarydata.Extinction,0)
			Yerr = extract_column(self.summarydata.Extinction,1)
			ymin,ymax = sigma_clipping_median_limits(Y,3.5,2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Extinction vs Cloud Coverage for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Cloud coverage (fraction)'
		AnalysisData.ylabel = 'Extinction (mag/airmass)'
		AnalysisData.title  = 'Extinction vs Cloud Coverage - '+str(filtername)
		
		xyplot = Graph()
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		xyplot.thegraph.set_ylim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		xyplot.thegraph.set_xlim(0,1)
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_extinction_clouds_'+filtername+'.png')
		
		return(0)
	
		
	def skybrightness_vs_cloudcoverage(self,filtername,ImageInfo):
		''' 
		Perform the Zero Point Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		#self.select_data_clouds(0,1)
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = extract_column(self.summarydata.CloudCoverage,0)
			#Xerr = extract_column(self.summarydata.CloudCoverage,1)
			Y = extract_column(self.summarydata.ZenithSB,0)
			Yerr = extract_column(self.summarydata.ZenithSB,1)
			ymin,ymax = sigma_clipping_median_limits(Y,3.5,2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting SkyBrightness vs Cloud Coverage for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Cloud coverage (fraction)'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB vs Cloud Coverage - '+str(filtername)
		
		xyplot = Graph()
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		xyplot.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		xyplot.thegraph.set_xlim(0,1)
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_zenithnsb_clouds_'+filtername+'.png')
		
		return(0)
	
	
	'''
	-----------------
	Seasonal analysis
	-----------------
	'''
	
	def cloudcoverage_season_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Extinction Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		#self.select_data_clouds(0,1)
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xraw = np.array(self.summarydata.DateTime)
			Xraw2 = np.array([get_season(xi) for xi in Xraw]) # Get the season 
			Yraw = np.array(extract_column(self.summarydata.CloudCoverage,0))
			sep = 0.1
			#Y = np.histogram(Yraw,bins=np.arange(0,1,0.1))
			#X = np.unique(Xraw2)
			#X = np.append(X,4) # Add the Spring to the end of the array
			#Y = np.arange(0,1,0.1)
			'''
			X = np.linspace(0,4,101)
			Y = np.histogram(Yraw[Xraw2==0],\
			 density=True,bins=np.arange(np.percentile(Yraw,1),\
			 np.percentile(Yraw,99)+sep,sep))[1]
			Y = 0.5*(np.array(Y)[:-1]+np.array(Y)[1:])
			yi,xi = np.meshgrid(Yraw,X)
			Z = np.array([\
			 np.histogram(Yraw[Xraw2==int(season)%4],\
			 density=True,bins=np.arange(np.percentile(Yraw,1),\
			 np.percentile(Yraw,99)+sep,sep))[0] for season in X])
			'''
			
			Xunique = np.unique(Xraw2)
			Ygrouped = np.array([Yraw[Xraw2==xiu] for xiu in Xunique])
			Yerrgrouped = np.array([Yraw[Xraw2==xiu] for xiu in Xunique])
			
			#Yerr = extract_column(self.summarydata.CloudCoverage,1)
			if filtername=='Johnson_B': colormap = cm.Blues
			elif filtername=='Johnson_V': colormap = cm.Greens
			elif filtername=='Johnson_R': colormap = cm.Reds
			elif filtername=='Johnson_U': colormap = cm.Purples
			elif filtername=='Johnson_I': colormap = cm.Oranges
			else: colormap = cm.Greys
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Cloud Coverage vs Season for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Season'
		AnalysisData.ylabel = 'Cloud Coverage (fraction)'
		AnalysisData.zlabel = 'Frequency (counts)'
		AnalysisData.title  = 'Cloud Coverage vs Season - '+str(filtername)
		
		AnalysisData.x2ticks = np.arange(0,4,1)#+0.5
		AnalysisData.x2values = ['Spring','Summer','Autumn','Winter']
		
		'''
		# Old style contourplot
		data_evolgraph = Graph()
		#data_evolgraph.plot_scatter_data(AnalysisData,maxerrorbars=10)
		data_evolgraph.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		data_evolgraph.set_xtickaxis(AnalysisData)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.thegraph.set_ylim(0,1)
		'''
		
		# Boxplot
		data_evolgraph = Graph(12,5)
		data_evolgraph.thegraph.boxplot(AnalysisData.Ygrouped,positions=AnalysisData.Xunique)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.set_xtickaxis(AnalysisData)
		data_evolgraph.thegraph.set_ylim(0,1)
		#data_evolgraph.clip_data(AnalysisData)
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_season_clouds_'+filtername+'.png')
		
		return(0)
	
	
	def skybrightness_season_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the SB Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		vect_get_season = np.vectorize(get_season)
		self.select_moon_filter(ImageInfo,0,0.2,75,180)
		self.select_data_clouds(0,2./3)
		
		#self.select_data_clouds(0,1)
		
		
		class AnalysisData():
			Xraw = np.array(self.summarydata.DateTime)
			Xraw2 = vect_get_season(Xraw) # Get the season 
			Yraw = np.array(extract_column(self.summarydata.ZenithSB,0))
			ymin,ymax = sigma_clipping_median_limits(Yraw,1.2,0.8)
			sep = 0.25
			#Y = np.histogram(Yraw,bins=np.arange(0,1,0.1))
			#X = np.unique(Xraw2)
			#X = np.append(X,4) # Add the Spring to the end of the array
			'''
			X = np.linspace(0,4,101)
			Y = np.histogram(Yraw[Xraw2==0],density=True,\
			 bins=np.arange(ymin,ymax+sep,sep))[1]
			Y = 0.5*(np.array(Y)[:-1]+np.array(Y)[1:])
			yi,xi = np.meshgrid(Yraw,X)
			Z = np.array([np.histogram(Yraw[Xraw2==int(season)%4],density=True,\
			 bins=np.arange(ymin,ymax+sep,sep))[0] for season in X])
			'''
			
			Xunique = np.unique(Xraw2)
			Ygrouped = np.array([Yraw[Xraw2==xiu] for xiu in Xunique])
			Yerrgrouped = np.array([Yraw[Xraw2==xiu] for xiu in Xunique])
			
			#Yerr = extract_column(self.summarydata.CloudCoverage,1)
			if filtername=='Johnson_B': colormap = cm.Blues
			elif filtername=='Johnson_V': colormap = cm.Greens
			elif filtername=='Johnson_R': colormap = cm.Reds
			elif filtername=='Johnson_U': colormap = cm.Purples
			elif filtername=='Johnson_I': colormap = cm.Oranges
			else: colormap = cm.Greys
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith Night Sky Brightness vs Season for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Season'
		AnalysisData.ylabel = 'Sky Brightness (mag/arcsec2)'
		AnalysisData.zlabel = 'Frequency (counts)'
		AnalysisData.title  = 'Sky Brightness vs Season - '+str(filtername)
		
		AnalysisData.x2ticks = np.arange(0,4,1)#+0.5
		AnalysisData.x2values = ['Spring','Summer','Autumn','Winter']
		
		'''
		#Old style contourplot
		data_evolgraph = Graph()
		#data_evolgraph.plot_scatter_data(AnalysisData,maxerrorbars=10)
		data_evolgraph.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		data_evolgraph.set_xtickaxis(AnalysisData)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		'''
		
		# Boxplot
		data_evolgraph = Graph(12,5)
		data_evolgraph.thegraph.boxplot(AnalysisData.Ygrouped,positions=AnalysisData.Xunique)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.set_xtickaxis(AnalysisData)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_sb[0]-1,ImageInfo.limits_sb[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_season_zenithnsb_'+filtername+'.png')
		
		return(0)
	
	
	def extinction_season_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Extinction Data analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		vect_get_season = np.vectorize(get_season)
		#self.select_data_clouds(0,1)
		self.select_moon_filter(ImageInfo,0,0.2,75,180)
		self.select_data_clouds(0,2./3)
		
		class AnalysisData():
			Xraw = np.array(self.summarydata.DateTime)
			Xraw2 = vect_get_season(Xraw) # Get the season 
			Yraw = np.array(extract_column(self.summarydata.Extinction,0))
			ymin,ymax = sigma_clipping_median_limits(Yraw,0.15,0.4)
			sep = 0.05
			#Y = np.histogram(Yraw,bins=np.arange(0,1,0.1))
			#X = np.unique(Xraw2)
			#X = np.append(X,4) # Add the Spring to the end of the array
			'''
			X = np.linspace(0,4,101)
			Y = np.histogram(Yraw[Xraw2==0],density=True,\
			 bins=np.arange(ymin,ymax+sep,sep))[1]
			Y = 0.5*(np.array(Y)[:-1]+np.array(Y)[1:])
			yi,xi = np.meshgrid(Yraw,X)
			Z = np.array([np.histogram(Yraw[Xraw2==int(season)%4],density=True,\
			 bins=np.arange(ymin,ymax+sep,sep))[0] for season in X])
			'''
			Xunique = np.unique(Xraw2)
			Ygrouped = np.array([Yraw[Xraw2==xiu] for xiu in Xunique])
			Yerrgrouped = np.array([Yraw[Xraw2==xiu] for xiu in Xunique])
			
			#Yerr = extract_column(self.summarydata.CloudCoverage,1)
			if filtername=='Johnson_B': colormap = cm.Blues
			elif filtername=='Johnson_V': colormap = cm.Greens
			elif filtername=='Johnson_R': colormap = cm.Reds
			elif filtername=='Johnson_U': colormap = cm.Purples
			elif filtername=='Johnson_I': colormap = cm.Oranges
			else: colormap = cm.Greys
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Extinction vs Season for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Season'
		AnalysisData.ylabel = 'Extinction (mag/airmass)'
		AnalysisData.zlabel = 'Frequency (counts)'
		AnalysisData.title  = 'Extinction vs Season - '+str(filtername)
		
		AnalysisData.x2ticks = np.arange(0,4,1)#+0.5
		AnalysisData.x2values = ['Spring','Summer','Autumn','Winter']
		
		'''
		# Old style contourplot
		data_evolgraph = Graph()
		#data_evolgraph.plot_scatter_data(AnalysisData,maxerrorbars=10)
		data_evolgraph.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		data_evolgraph.set_xtickaxis(AnalysisData)
		data_evolgraph.title_and_labels(AnalysisData)
		'''
		
		# Boxplot
		data_evolgraph = Graph(12,5)
		data_evolgraph.thegraph.boxplot(AnalysisData.Ygrouped,positions=AnalysisData.Xunique)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.set_xtickaxis(AnalysisData)
		#data_evolgraph.clip_data(AnalysisData)
		
		#data_evolgraph.thegraph.set_ylim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		data_evolgraph.thegraph.set_ylim(0,None)
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_season_extinction_'+filtername+'.png')
		
		return(0)
	
	
	'''
	-----------------------------------------------------------------
	Galactic analysis. Here we study the impact of the MW in the data
	-----------------------------------------------------------------
	'''
	
	def skybrightness_galacticlatitude_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness analysis dependence with Galactic Latitude.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_galactic_latlon_zenith = np.vectorize(galactic_latlon_zenith)
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		#self.select_data_clouds(0,0.1)
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			X = np.array(vect_galactic_latlon_zenith(Xdate,ImageInfo)[0]) # Get galactic latitude
			#Xerr = extract_column(self.summarydata.CloudCoverage,1)
			Y = np.array(extract_column(self.summarydata.ZenithSB,0))
			Yerr = np.array(extract_column(self.summarydata.ZenithSB,1))
			ymin,ymax = sigma_clipping_median_limits(Y,2.5,2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
		
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		''' Fit dependence to sinoidal curve '''
		self.Fit = DataFit()
		#Weights = DensityWeighted(AnalysisData.X)
		#Weights = Weights-np.min(Weights)+1
		#Weights = 1./Weights
		myfilter = AnalysisData.X>-180.0
		xmodeling = np.linspace(np.min(AnalysisData.X),np.max(AnalysisData.X),1000)
		
		data = [AnalysisData.X[myfilter],AnalysisData.Y[myfilter]]#,Weights]
		#params0 = [15,2,np.pi/180.,+15*np.pi/180.] # Initial values
		params0 = [15,-1,0,30]
		self.Fit.data_fit(data,self.Fit.gaussian_residuals,params0)
		self.Fit.data_confidence_band(xmodel=xmodeling)
		self.Fit.add_comment(\
		 'Galactic latitude fit for '+\
		 filtername+' done at '+str(datetime.datetime.now())\
		 )
		
		''' Append the fit parameters to AnalysisData '''
		AnalysisData.fitmodel = self.Fit.model
		AnalysisData.fitparam = self.Fit.params
		AnalysisData.fiterror = self.Fit.xerror
		
		''' Print on given output the results '''
		self.Fit.print_results(sys.stdout)
		''' Save the results to file '''
		resultsfile = open(self.results_file,'a+')
		self.Fit.print_results(resultsfile)
		resultsfile.close()
		
		
		print('Ploting Zenith SB vs Galactic Latitude for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Galactic Latitude (degrees)'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB vs Galactic Latitude - '+str(filtername)
		
		xyplot = Graph(12,5)
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		#xyplot.plot_scatter_fit(AnalysisData)
		xyplot.thegraph.plot(self.Fit.xmodel,self.Fit.ymodel,'k-')
		xyplot.thegraph.plot(self.Fit.xmodel,self.Fit.upperband,'k:')
		xyplot.thegraph.plot(self.Fit.xmodel,self.Fit.lowerband,'k:')
		#xyplot.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_zenithnsb_gallat_'+filtername+'.png')
		
		return(0)
	
	
	def skybrightness_galacticlongitude_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness analysis dependence with Galactic Latitude.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_galactic_latlon_zenith = np.vectorize(galactic_latlon_zenith)
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			X = vect_galactic_latlon_zenith(Xdate,ImageInfo)[1] # Get galactic longitude
			#Xerr = extract_column(self.summarydata.CloudCoverage,1)
			Y = extract_column(self.summarydata.ZenithSB,0)
			Yerr = extract_column(self.summarydata.ZenithSB,1)
			ymin,ymax = sigma_clipping_median_limits(Y,2.5,2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith SB vs Galactic Longitude for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Galactic Longitude (degrees)'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB vs Galactic Longitude - '+str(filtername)
		
		xyplot = Graph(12,5)
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		#xyplot.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_zenithnsb_gallon_'+filtername+'.png')
		
		return(0)
	
	
	def skybrightness_galacticlatitudelongitude_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness analysis dependence with Galactic Latitude.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_galactic_latlon_zenith = np.vectorize(galactic_latlon_zenith)
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			Yraw = vect_galactic_latlon_zenith(Xdate,ImageInfo)[1] # Get galactic latitude
			Xraw = vect_galactic_latlon_zenith(Xdate,ImageInfo)[0] # Get galactic longitude
			Zraw = extract_column(self.summarydata.ZenithSB,0)
			Zraw_err = extract_column(self.summarydata.ZenithSB,1)
			
			X = np.arange(0,360,15)
			Y = np.arange(-90,90+10,15)
			Ygrid,Xgrid = np.meshgrid(Y,X)
			#Xgrid,Ygrid = np.mgrid[0:360:25j,-90:90:19j]
			XiYi = np.array([Xraw,Yraw]).transpose()
			
			Z = scipy.interpolate.griddata(XiYi,Zraw,(Xgrid,Ygrid),method='linear')
			
			if filtername=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filtername=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filtername=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filtername=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filtername=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith SB vs Galactic Lat/Lon for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Galactic Longitude (degrees)'
		AnalysisData.ylabel = 'Galactic Latitude (degrees)'
		AnalysisData.zlabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB over the Galactic plane - '+str(filtername)
		
		contourplot = Graph()
		contourplot.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		contourplot.title_and_labels(AnalysisData)
		
		AnalysisData.Y = AnalysisData.Xraw
		AnalysisData.X = AnalysisData.Yraw
		#contourplot.plot_scatter_data(AnalysisData)
		
		if DEBUG==True: contourplot.show_plot()
		contourplot.save_fig(base_path+'/summary_zenithnsb_gallatlon_'+filtername+'.png')
		
		return(0)
	
	
	
	def skybrightness_eclipticlatitude_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness analysis dependence with Ecliptic Latitude.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_ecliptic_latlon_zenith = np.vectorize(ecliptic_latlon_zenith)
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			X = vect_ecliptic_latlon_zenith(Xdate,ImageInfo)[1] # Get ecliptic latitude
			#Xerr = extract_column(self.summarydata.CloudCoverage,1)
			Y = extract_column(self.summarydata.ZenithSB,0)
			Yerr = extract_column(self.summarydata.ZenithSB,1)
			ymin,ymax = sigma_clipping_median_limits(Y,2.5,2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith SB vs Ecliptic Latitude for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Ecliptic Latitude (degrees)'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB vs Ecliptic Latitude - '+str(filtername)
		
		xyplot = Graph(12,5)
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		#xyplot.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_zenithnsb_ecllat_'+filtername+'.png')
		
		return(0)
	
	
	'''
	--------------------------------------------------------------------------------
	Ecliptic analysis. Here we study the impact of the Solar System dust in the data
	--------------------------------------------------------------------------------
	'''
	
	def skybrightness_eclipticlongitude_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness analysis dependence with Ecliptic Longitude.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_ecliptic_latlon_zenith = np.vectorize(ecliptic_latlon_zenith)
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			X = vect_ecliptic_latlon_zenith(Xdate,ImageInfo)[1] # Get ecliptic longitude
			#Xerr = extract_column(self.summarydata.CloudCoverage,1)
			Y = extract_column(self.summarydata.ZenithSB,0)
			Yerr = extract_column(self.summarydata.ZenithSB,1)
			ymin,ymax = sigma_clipping_median_limits(Y,2.5,2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith SB vs Ecliptic Longitude for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Ecliptic Longitude (degrees)'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB vs Ecliptic Longitude - '+str(filtername)
		
		xyplot = Graph()
		xyplot.plot_scatter_data(AnalysisData,maxerrorbars=10)
		xyplot.clip_data(AnalysisData)
		#xyplot.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		if DEBUG==True: xyplot.show_plot()
		xyplot.save_fig(base_path+'/summary_zenithnsb_ecllon_'+filtername+'.png')
		
		return(0)
	
	
	def skybrightness_eclipticlatitudelongitude_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness map with Ecliptic Latitude/Longitude as Y/X axis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_ecliptic_latlon_zenith = np.vectorize(ecliptic_latlon_zenith)
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			Yraw = vect_ecliptic_latlon_zenith(Xdate,ImageInfo)[1] # Get ecliptic latitude
			Xraw = vect_ecliptic_latlon_zenith(Xdate,ImageInfo)[0] # Get ecliptic longitude
			Zraw = extract_column(self.summarydata.ZenithSB,0)
			Zraw_err = extract_column(self.summarydata.ZenithSB,1)
			
			X = np.arange(0,360,15)
			Y = np.arange(-90,90+10,15)
			Ygrid,Xgrid = np.meshgrid(Y,X)
			#Xgrid,Ygrid = np.mgrid[0:360:25j,-90:90:19j]
			XiYi = np.array([Xraw,Yraw]).transpose()
			
			Z = scipy.interpolate.griddata(XiYi,Zraw,(Xgrid,Ygrid),method='linear')
			
			if filtername=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filtername=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filtername=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filtername=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filtername=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith SB vs Ecliptic Lat/Lon for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Ecliptic Longitude (degrees)'
		AnalysisData.ylabel = 'Ecliptic Latitude (degrees)'
		AnalysisData.zlabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB over the Ecliptic plane - '+str(filtername)
		
		contourplot = Graph()
		contourplot.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		contourplot.title_and_labels(AnalysisData)
		
		AnalysisData.Y = AnalysisData.Xraw
		AnalysisData.X = AnalysisData.Yraw
		#contourplot.plot_scatter_data(AnalysisData)
		
		if DEBUG==True: contourplot.show_plot()
		contourplot.save_fig(base_path+'/summary_zenithnsb_ecllatlon_'+filtername+'.png')
		
		return(0)
	
	
	'''
	-----------------------
	Moon impact on the data
	-----------------------
	'''
	
	def skybrightness_moon_phasezdist_analysis(self,filtername,ImageInfo):
		
		''' 
		Perform the Sky Brightness analysis dependence with Moon phase/position.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		vect_moon_phase_calculator = np.vectorize(moon_phase_calculator)
		vect_moon_zdist_calculator = np.vectorize(moon_zdist_calculator)

		self.select_data_filter(filtername)
		#self.select_data_prefilter_bydate(ImageInfo)	# WE NEED JANUARY 2012 data.
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		#self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		zenithsb_values = extract_column(self.summarydata.ZenithSB,0)
		zenithsb_std = np.std(zenithsb_values)
		zenithsb_desv = np.abs(zenithsb_values-np.median(zenithsb_values))
		zenithsb_filter = zenithsb_desv<15*zenithsb_std
		self.apply_filter_pattern(zenithsb_filter)
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			Yraw = vect_moon_zdist_calculator(Xdate,ImageInfo) # Moon altitude calculator
			Xraw = vect_moon_phase_calculator(Xdate,ImageInfo)	# Moon phase calculator
			Zraw = extract_column(self.summarydata.ZenithSB,0)
			Zraw_err = extract_column(self.summarydata.ZenithSB,1)
			# Filterdata!!!!!!!
			
			X = np.arange(0,30+1,1)
			Y = np.arange(50,110+5,5)
			Ygrid,Xgrid = np.meshgrid(Y,X)
			#Xgrid,Ygrid = np.mgrid[0:360:25j,-90:90:19j]
			XiYi = np.array([Xraw,Yraw]).transpose()
			
			Z = scipy.interpolate.griddata(XiYi,Zraw,(Xgrid,Ygrid),method='linear')
			
			if filtername=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filtername=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filtername=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filtername=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filtername=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Zenith SB vs Moon phase/zenithal distance for filter '+filtername)
		
		# Create the graph
		AnalysisData.xlabel = 'Moon phase (days after new moon)'
		AnalysisData.ylabel = 'Moon zenithal distance (degrees)'
		AnalysisData.zlabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB dependence on Moon position/phase - '+str(filtername)
		
		contourplot = Graph(12,5)
		contourplot.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		contourplot.title_and_labels(AnalysisData)
		
		AnalysisData.Y = AnalysisData.Xraw
		AnalysisData.X = AnalysisData.Yraw
		#contourplot.plot_scatter_data(AnalysisData)
		
		if DEBUG==True: contourplot.show_plot()
		contourplot.save_fig(base_path+'/summary_zenithnsb_moonposphase_'+filtername+'.png')
		
		return(0)
	
	
	'''
	--------------------------------------------
	Solar Activity impact on the data (airglow?)
	--------------------------------------------
	'''
	
	def skybrightness_solar_xuv_flux(self,filtername,ImageInfo):
		''' 
		Perform the Sky Brightness analysis dependence with Solar activity (XUV).
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,90,180)
		
		dates_nsbdata = dict(zip(self.summarydata.DateTime, extract_column(self.summarydata.ZenithSB,0)))
		dates_nsbdata_err = dict(zip(self.summarydata.DateTime, extract_column(self.summarydata.ZenithSB,1)))
		
		solarflux_columntitles = ['XRS-B', 'XRS-A', 'SEM', '0.1-7nm', '17.1nm', '25.7nm', '30.4nm', '36.6nm', '121.6']
		solarflux_columnindex  = [1,2,3,4,5,6,7,8,10]
		
		solar_data_columns = dict(zip(solarflux_columnindex, solarflux_columntitles))
		
		for solar_column in solar_data_columns:
			dates_solar_flux = solar_activity_radiation(solar_column)
			
			class AnalysisData():
				Xdate = self.summarydata.DateTime
				
				X = np.array([dates_solar_flux[item.date()][0] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				Xerr = np.array([dates_solar_flux[item.date()][1] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				Y = np.array([dates_nsbdata[item] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				Yerr = np.array([dates_nsbdata_err[item] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				if filtername=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
				elif filtername=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
				elif filtername=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
				elif filtername=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
				elif filtername=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
				else: colormap = cm.Greys; color=0.5; colorpoints=0.75
			
			''' Check uncertainties '''
			self.check_uncertainties(AnalysisData)
			
			print('Ploting Zenith SB vs Solar flux at '+solar_data_columns[solar_column]+' for filter '+filtername)
			
			# Create the graph
			AnalysisData.xlabel = 'Solar XUV flux at '+solar_data_columns[solar_column]+' (erg/s)'
			AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
			AnalysisData.title  = 'Zenith NSB vs Solar flux at '+solar_data_columns[solar_column]+' - '+str(filtername)
			
			xyplot = Graph()
			xyplot.plot_scatter_data(AnalysisData,maxerrorbars=30)
			xyplot.clip_data(AnalysisData)
			xyplot.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
			
			if DEBUG==True: xyplot.show_plot()
			xyplot.save_fig(base_path+'/summary_zenithnsb_solarxuv_'+solar_data_columns[solar_column]+'_'+filtername+'.png')
			
		return(0)
		
	def skycolor_solar_xuv_flux(self,filter1,filter2,ImageInfo):
		''' 
		Perform the Sky Color analysis dependence with Solar Activity (XUV).
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		self.extract_color_filter(filter1,filter2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds=True,filterdata_moon=True)
		
		dates_nsbcolordata = dict(zip(self.summarydata.DateTime, self.summarydata.Colors[0]))
		dates_nsbcolordata_err = dict(zip(self.summarydata.DateTime, self.summarydata.Colors[1]))
		
		solarflux_columntitles = ['XRS-B', 'XRS-A', 'SEM', '0.1-7nm', '17.1nm', '25.7nm', '30.4nm', '36.6nm', '121.6']
		solarflux_columnindex  = [1,2,3,4,5,6,7,8,10]
		
		solar_data_columns = dict(zip(solarflux_columnindex, solarflux_columntitles))
		
		for solar_column in solar_data_columns:
			dates_solar_flux = solar_activity_radiation(solar_column)
			
			class AnalysisData():
				Xdate = self.summarydata.DateTime
				
				X = np.array([dates_solar_flux[item.date()][0] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				Xerr = np.array([dates_solar_flux[item.date()][1] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				Y = np.array([dates_nsbcolordata[item] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				Yerr = np.array([dates_nsbcolordata_err[item] \
				for item in Xdate \
				if item.date() in dates_solar_flux.keys()],dtype=np.float64)
				
				if filter1=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
				elif filter1=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
				elif filter1=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
				elif filter1=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
				elif filter1=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
				else: colormap = cm.Greys; color=0.5; colorpoints=0.75
			
			''' Check uncertainties '''
			self.check_uncertainties(AnalysisData)
			
			print('Ploting Zenith NSB Color vs Solar flux at '+solar_data_columns[solar_column]+' for '+filter1+'-'+filter2)
			# If you get ValueError: Invalid vertices array. , reduce from np.float to np.float64 or float32
			
			# Create the graph
			AnalysisData.xlabel = 'Solar XUV flux at '+solar_data_columns[solar_column]+' (erg/s)'
			AnalysisData.ylabel = str(filter1[-1])+'-'+str(filter2[-1])+' (mag/arcsec2)'
			AnalysisData.title  = 'Zenith Sky Color vs Solar flux at '+\
			 solar_data_columns[solar_column]+' - '+str(filter1[-1])+'-'+str(filter2[-1])
			
			xyplot = Graph()
			xyplot.plot_scatter_data(AnalysisData,maxerrorbars=30)
			xyplot.clip_data(AnalysisData)
			xyplot.thegraph.set_ylim(ImageInfo.limits_colorcolor[0],ImageInfo.limits_colorcolor[1])
			
			if DEBUG==True: xyplot.show_plot()
			xyplot.save_fig(base_path+'/summary_zenithnsb_solarxuv_'+solar_data_columns[solar_column]+'_'+filter1+'-'+filter2+'.png')
			
		return(0)
	
	
	'''
	--------------------------------------------
	Aerosol concentration (Rayleight dispersion?
	--------------------------------------------
	'''
	
	def extinctionslope_analysis(self,filter1,filter2,ImageInfo):
		''' 
		Perform the Extinction Slope (aka Aerosols concentration) analysis over time.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		self.extract_color_filter(filter1,filter2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds=False,filterdata_moon=False)
		
		dates_extinctionslope = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.ExtinctionSlope[0]))
		dates_extinctionslope_err = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.ExtinctionSlope[1]))
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			
			X = np.array([item for item in Xdate \
			 if item in dates_extinctionslope.keys()])
			
			Y = np.array([dates_extinctionslope[item] \
			 for item in Xdate \
			 if item in dates_extinctionslope.keys()])
			
			Yerr = np.array([dates_extinctionslope[item] \
			 for item in Xdate \
			 if item in dates_extinctionslope_err.keys()])
			
			filterpattern = np.array(-5<Y)*np.array(Y<1)
			X = X[filterpattern]
			Y = Y[filterpattern]
			Yerr = Yerr[filterpattern]
			
			
			if filter1=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filter1=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filter1=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filter1=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filter1=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Extinction Slopes vs DateTime for '+filter1+'-'+filter2)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'log(K'+filter1[-1]+'/K'+filter2[-1]+') [mag/airmass]'
		AnalysisData.title  = 'Extinction slope for '+str(filter1[-1])+'-'+str(filter2[-1])
		
		data_evolgraph = Graph()
		data_evolgraph.plot_scatter_data(AnalysisData,maxerrorbars=0)
		data_evolgraph.clip_data(AnalysisData)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_extext[0],ImageInfo.limits_extext[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_extinctionslopes_'+filter1+'-'+filter2+'.png')
		
		return(0)
	
	
	'''
	------------------
	Sky color analysis
	------------------
	'''
	
	def skycolor_date_analysis(self,filter1,filter2,ImageInfo):
		''' 
		Perform the Sky Color analysis over time.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		self.extract_color_filter(filter1,filter2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds=False,filterdata_moon=False)
		
		dates_skycolor = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[0]))
		dates_skycolor_err = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[1]))
		
		class AnalysisData():
			Xdate = self.summarydata.DateTime
			
			X = np.array([item for item in Xdate \
			 if item in dates_skycolor.keys()])
			
			Y = np.array([dates_skycolor[item] \
			 for item in Xdate \
			 if item in dates_skycolor.keys()])
			
			Yerr = np.array([dates_skycolor_err[item] \
			 for item in Xdate \
			 if item in dates_skycolor.keys()])
			
			filterpattern = np.array(ImageInfo.limits_colorcolor[0]<Y)*\
			 np.array(Y<ImageInfo.limits_colorcolor[1])
			X = X[filterpattern]
			Y = Y[filterpattern]
			Yerr = Yerr[filterpattern]
			
			if filter1=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filter1=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filter1=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filter1=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filter1=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Sky Color vs DateTime for '+filter1+'-'+filter2)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = filter1[-1]+' - '+filter2[-1]+' [mag/arcsec2]'
		AnalysisData.title  = 'Night Sky Color vs Date for '+str(filter1[-1])+'-'+str(filter2[-1])
		
		data_evolgraph = Graph()
		data_evolgraph.plot_scatter_data(AnalysisData,maxerrorbars=0)
		data_evolgraph.clip_data(AnalysisData)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_colorcolor[0],ImageInfo.limits_colorcolor[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_date_skycolor_'+filter1+'-'+filter2+'.png')
		
		return(0)
	
	
	def skycolor_moon_phasezdist_analysis(self,filter1,filter2,ImageInfo):
		
		''' 
		Perform the Sky Color analysis dependence with Moon phase/position.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		filters_value={'Johnson_U':0,'Johnson_B':1,'Johnson_V':2,'Johnson_R':3,'Johnson_I':4}
		
		vect_moon_phase_calculator = np.vectorize(moon_phase_calculator)
		vect_moon_zdist_calculator = np.vectorize(moon_zdist_calculator)
		
		self.extract_color_filter(filter1,filter2,ImageInfo,\
		 filterdata_date=False,filterdata_clouds=True,filterdata_moon=False)
		
		dates_skycolor = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[0]))
		dates_skycolor_err = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[1]))
		
		#self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			Yraw = vect_moon_zdist_calculator(dates_skycolor.keys(),ImageInfo) # Moon altitude calculator
			Xraw = vect_moon_phase_calculator(dates_skycolor.keys(),ImageInfo) # Moon phase calculator
			Zraw = np.array([dates_skycolor[item] for item in dates_skycolor.keys()])
			Zraw_err = np.array([dates_skycolor_err[item] for item in dates_skycolor.keys()])
			
			X = np.arange(0,30+1,1)
			Y = np.arange(50,110+5,5)
			Ygrid,Xgrid = np.meshgrid(Y,X)
			#Xgrid,Ygrid = np.mgrid[0:360:25j,-90:90:19j]
			XiYi = np.array([Xraw,Yraw]).transpose()
			
			Z = scipy.interpolate.griddata(XiYi,Zraw,(Xgrid,Ygrid),method='linear')
			
			if filters_value[filter1]>filters_value[filter2]:
				colormap = cm.RdYlBu
				color='r'
				colorpoints=0.75
			else:
				colormap = cm.RdYlBu_r
				color='b'
				colorpoints=0.75
			#else: colormap = cm.Greys; color=0.5)
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Sky Color vs Moon phase/zenithal distance for '+filter1+'-'+filter2)
		
		# Create the graph
		AnalysisData.xlabel = 'Moon phase (days after new moon)'
		AnalysisData.ylabel = 'Moon zenithal distance (degrees)'
		AnalysisData.zlabel = 'Sky color (mag/arcsec2)'
		AnalysisData.title  = 'Sky color dependence on Moon position/phase for '+str(filter1[-1])+'-'+str(filter2[-1])
		
		contourplot = Graph()
		contourplot.plot_contours(AnalysisData,alpha_contours=0.5,alpha_contourf=1,contours=True,contourf=True)
		contourplot.title_and_labels(AnalysisData)
		
		AnalysisData.Y = AnalysisData.Xraw
		AnalysisData.X = AnalysisData.Yraw
		#contourplot.plot_scatter_data(AnalysisData)
		
		if DEBUG==True: contourplot.show_plot()
		contourplot.save_fig(base_path+'/summary_skycolor_moonposphase_'+filter1+'-'+filter2+'.png')
		
		return(0)
	
	
	'''
	--------------------------------------------------------
	Night time evolution of NSB, extinction, color, aerosols
	--------------------------------------------------------
	'''
	
	def skybrightness_time_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Skybrightness vs Time analysis
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a nighttime analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		'''
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = self.summarydata.Time
			X = np.array([X[k].hour+X[k].minute/60.+X[k].second/3600. for k in xrange(len(X))])
			X = X-24*(X>12)
			
			Y = np.array(extract_column(self.summarydata.ZenithSB,0))
			Yerr = np.array(extract_column(self.summarydata.ZenithSB,1))
			
			sep = 1
			Xint = np.array(X+sep*0.5-(X+sep*0.5)%sep)
			Xunique = np.unique(Xint)
			Ygrouped = np.array([Y[Xint==xiu] for xiu in Xunique])
			Yerrgrouped = np.array([Y[Xint==xiu] for xiu in Xunique])
			
			if filtername=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filtername=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filtername=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filtername=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filtername=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)

	
		''' Fit dependence to polynomial curve '''
		'''
		self.Fit = DataFit()
		#Weights = DensityWeighted(AnalysisData.X)
		#Weights = Weights-np.min(Weights)+1
		#Weights = 1./Weights
		#myfilter = AnalysisData.X>-180.0
		#xmodeling = np.linspace(np.min(AnalysisData.X),np.max(AnalysisData.X),1000)
		xmodeling = np.linspace(-5,6,1000)
		
		data = [AnalysisData.X,AnalysisData.Y]#,Weights]
		#data = [AnalysisData.X[myfilter],AnalysisData.Y[myfilter]]#,Weights]
		#params0 = [15,2,np.pi/180.,+15*np.pi/180.] # Initial values
		params0 = [1,1]
		self.Fit.data_fit(data,self.Fit.polynomial,params0)
		self.Fit.data_confidence_band(xmodel=xmodeling)
		self.Fit.add_comment(\
		 'Galactic latitude fit for '+\
		 filtername+' done at '+str(datetime.datetime.now())\
		 )
		
		# Append the fit parameters to AnalysisData 
		
		AnalysisData.fitmodel = self.Fit.model
		AnalysisData.fitparam = self.Fit.params
		AnalysisData.fiterror = self.Fit.xerror
		
		
		# Print on given output the results 
		self.Fit.print_results(sys.stdout)
		# Save the results to file 
		resultsfile = open(self.results_file,'a+')
		self.Fit.print_results(resultsfile)
		resultsfile.close()
		'''

		print('Performing Zenith NSB vs Time graph for filter '+filtername)
		
		for iteration in xrange(2):
			data_evolution = MedianFit(AnalysisData)
			
			# Needs a sigma filtering of the data.
			AnalysisData.Ypred = \
			 [data_evolution.median_values]*len(AnalysisData.Y)
			AnalysisData.Xpred = AnalysisData.X
			AnalysisData = sigma_clipping(\
			 fdata = AnalysisData,\
			 sigmaX = 0,\
			 sigmaY = data_evolution.median_error,\
			 max_sigmaX = 2,\
			 max_sigmaY = 2)
		
		# Create the graph
		AnalysisData.xlabel = 'Time (hours after UTC-midgnight)'
		AnalysisData.ylabel = 'Zenith NSB (mag/arcsec2)'
		AnalysisData.title  = 'Zenith NSB nighttime evolution - '+str(filtername)
		AnalysisData.Xtimeticks = 'hours'
		
		'''
		data_evolgraph = Graph()
		#data_evolgraph.fix_aspect_ratio()
		data_evolgraph.plot_scatter_2dhistogram(\
		 AnalysisData,bins=20,drawpoints=False,\
		 contours=True,contourf=True,alpha_contours=0.3,alpha_contourf=0.5)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.clip_data(AnalysisData)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		'''
		
		data_evolgraph = Graph(12,5)
		data_evolgraph.thegraph.boxplot(AnalysisData.Ygrouped,positions=AnalysisData.Xunique)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.clip_data(AnalysisData)
		#data_evolgraph.thegraph.set_ylim(ImageInfo.limits_sb[0],ImageInfo.limits_sb[1])
		
		'''
		data_evolgraph.thegraph.plot(self.Fit.xmodel,self.Fit.ymodel,'k-')
		data_evolgraph.thegraph.plot(self.Fit.xmodel,self.Fit.upperband,'k:')
		data_evolgraph.thegraph.plot(self.Fit.xmodel,self.Fit.lowerband,'k:')
		'''
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_zenithnsb_time_'+filtername+'.png')
		
		return(0)
	
	
	def extinction_time_analysis(self,filtername,ImageInfo):
		''' 
		Perform the Extinction vs Time analysis
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a nighttime analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		'''
		
		self.select_data_filter(filtername)
		self.select_data_prefilter_bydate(ImageInfo)
		self.select_data_clouds(0,0.75)\
		#ImageInfo.limits_goodnightsclouds[0],ImageInfo.limits_goodnightsclouds[1])
		self.select_moon_filter(ImageInfo,0,0.25,75,180)
		
		class AnalysisData():
			X = self.summarydata.Time
			X = np.array([X[k].hour+X[k].minute/60.+X[k].second/3600. for k in xrange(len(X))])
			X = X-24*(X>12)
			Y = np.array(extract_column(self.summarydata.Extinction,0))
			Yerr = np.array(extract_column(self.summarydata.Extinction,1))
			
			sep = 1
			Xint = np.array(X+sep*0.5-(X+sep*0.5)%sep)
			Xunique = np.unique(Xint)
			Ygrouped = np.array([Y[Xint==xiu] for xiu in Xunique])
			Yerrgrouped = np.array([Y[Xint==xiu] for xiu in Xunique])
			
			if filtername=='Johnson_B': colormap = cm.Blues; color='b'; colorpoints=0.75
			elif filtername=='Johnson_V': colormap = cm.Greens; color='g'; colorpoints=0.75
			elif filtername=='Johnson_R': colormap = cm.Reds; color='r'; colorpoints=0.75
			elif filtername=='Johnson_U': colormap = cm.Purples; color='#800080'; colorpoints=0.75
			elif filtername=='Johnson_I': colormap = cm.Oranges; color='#FFA500'; colorpoints=0.75
			else: colormap = cm.Greys; color=0.5; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)

		print('Performing Extinction vs Time graph for filter '+filtername)
		
		for iteration in xrange(2):
			data_evolution = MedianFit(AnalysisData)
			
			# Needs a sigma filtering of the data.
			AnalysisData.Ypred = \
			 [data_evolution.median_values]*len(AnalysisData.Y)
			AnalysisData.Xpred = AnalysisData.X
			AnalysisData = sigma_clipping(\
			 fdata = AnalysisData,\
			 sigmaX = 0,\
			 sigmaY = data_evolution.median_error,\
			 max_sigmaX = 2,\
			 max_sigmaY = 2)
		
		# Create the graph
		AnalysisData.xlabel = 'Time (hours after UTC-midgnight)'
		AnalysisData.ylabel = 'Extinction (mag/airmass)'
		AnalysisData.title  = 'Extinction nighttime evolution - '+str(filtername)
		AnalysisData.Xtimeticks = 'hours'
		
		'''
		data_evolgraph = Graph()
		#data_evolgraph.fix_aspect_ratio()
		data_evolgraph.plot_scatter_2dhistogram(\
		 AnalysisData,bins=20,drawpoints=False,contours=True,contourf=True,alpha_contours=0.3,alpha_contourf=0.5)
		data_evolgraph.title_and_labels(AnalysisData)
		#data_evolgraph.clip_data(AnalysisData)
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		'''
		
		data_evolgraph = Graph(12,5)
		data_evolgraph.thegraph.boxplot(AnalysisData.Ygrouped,positions=AnalysisData.Xunique)
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.clip_data(AnalysisData)
		#data_evolgraph.thegraph.set_ylim(ImageInfo.limits_extinction[0],ImageInfo.limits_extinction[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_extinction_time_'+filtername+'.png')
		
		return(0)
	
	
	'''
	-----------------------------------------------------------------------------
	Doble/Triple filter analysis (color-color and extinction-extinction diagrams)
	-----------------------------------------------------------------------------
	'''
	
	def extinction_extinction_analysis(self,filterA1,filterA2,filterB1,filterB2,ImageInfo):
		''' 
		Perform the Sky Extinction vs extinction analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		self.extract_color_filter(\
		 filterA1,filterA2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds='NoOvercast',filterdata_moon=False)
		
		dates_extinctionslope1 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.ExtinctionSlope[0]))
		dates_extinctionslope_err1 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.ExtinctionSlope[1]))
		
		self.extract_color_filter(\
		 filterB1,filterB2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds='NoOvercast',filterdata_moon=False)
		
		dates_extinctionslope2 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.ExtinctionSlope[0]))
		dates_extinctionslope_err2 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.ExtinctionSlope[1]))
		
		DateTime1 = dates_extinctionslope1.keys()
		DateTime2 = dates_extinctionslope2.keys()
		
		match_dates,mean_dates = match_dates_array(DateTime1,DateTime2,max_absoffset=1200)
		
		class AnalysisData():
			X = np.array([dates_extinctionslope1.values()[item] \
			 for item in match_dates.keys()])
			Xerr = np.array([dates_extinctionslope_err1.values()[item] \
			 for item in match_dates.keys()])
			Y = np.array([dates_extinctionslope2.values()[item] \
			 for item in match_dates.values()])
			Yerr = np.array([dates_extinctionslope_err2.values()[item] \
			 for item in match_dates.values()])
			'''
			filterpattern = np.array(ImageInfo.limits_extext[0]<Y)*\
			 np.array(Y<ImageInfo.limits_extext[1])
			X = X[filterpattern]
			Y = Y[filterpattern]
			Yerr = Yerr[filterpattern]
			'''
			colormap = cm.Blues; color=0.5; colorpoints=0.75
		
		''' Plot July-2012 in other color '''
		
		class AnalysisData_july():
			julyfilter = self.create_filter_bydate(\
			 mean_dates.values(),\
			 datetime_from_number([20120701,120000]),\
			 datetime_from_number([20120801,120000]))
			X = np.array(AnalysisData.X[julyfilter])
			Xerr = np.array(AnalysisData.Xerr[julyfilter])
			Y = np.array(AnalysisData.Y[julyfilter])
			Yerr = np.array(AnalysisData.Yerr[julyfilter])
			colormap = cm.Reds; color='orange'; colorpoints='orange'
		
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Sky Extinction slope-slope for '+filterB1+'-'+filterB2+\
		 ' vs '+filterA1+'-'+filterA2)
		
		# Create the graph
		AnalysisData.xlabel = 'log(K_'+filterA1[-1]+'/K_'+filterA2[-1]+') [mag/airmass]'
		AnalysisData.ylabel = 'log(K_'+filterB1[-1]+'/K_'+filterB2[-1]+') [mag/airmass]'
		AnalysisData.title  = 'Extinction diagram for '+\
		 str(filterB1[-1])+'-'+str(filterB2[-1])+' vs '+\
		 str(filterA1[-1])+'-'+str(filterA2[-1])
		
		AnalysisData_july.xlabel = 'log(K'+filterA1[-1]+'/K'+filterA2[-1]+') [mag/airmass]'
		AnalysisData_july.ylabel = 'log(K'+filterB1[-1]+'/K'+filterB2[-1]+') [mag/airmass]'
		AnalysisData_july.title  = 'Extinction diagram '+\
		 str(filterB1[-1])+'-'+str(filterB2[-1])+' vs '+\
		 str(filterA1[-1])+'-'+str(filterA2[-1])
		
		data_evolgraph = Graph()
		data_evolgraph.fix_aspect_ratio()
		
		data_evolgraph.plot_scatter_2dhistogram(\
		 AnalysisData,bins=20,contours=True,alpha_contours=0.3)
		 
		try:
			data_evolgraph.plot_scatter_2dhistogram(\
			 AnalysisData_july,bins=20,contours=False,alpha_contours=0.3)
		except:
			print('Error ploting July data')
		 
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.clip_data(AnalysisData)
		data_evolgraph.thegraph.set_xlim(ImageInfo.limits_extext[0],ImageInfo.limits_extext[1])
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_extext[0],ImageInfo.limits_extext[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_extinction_extinction_'+\
		 filterB1+'-'+filterB2+'_'+filterA1+'-'+filterA2+'.png')
		
		return(0)
	
	def skycolor_color_analysis(self,filterA1,filterA2,filterB1,filterB2,ImageInfo):
		''' 
		Perform the Sky Color vs Color analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform the dependence study
		'''
		
		self.extract_color_filter(\
		 filterA1,filterA2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds='NoOvercast',filterdata_moon=False)
		
		dates_skycolor1 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[0]))
		dates_skycolor_err1 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[1]))
		
		self.extract_color_filter(\
		 filterB1,filterB2,ImageInfo,\
		 filterdata_date=True,filterdata_clouds='NoOvercast',filterdata_moon=False)
		
		dates_skycolor2 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[0]))
		dates_skycolor_err2 = \
			dict(zip(\
			 self.summarydata.DateTime, \
			 self.summarydata.Colors[1]))
		
		DateTime1 = dates_skycolor1.keys()
		DateTime2 = dates_skycolor2.keys()
		
		match_dates,mean_dates = match_dates_array(DateTime1,DateTime2,max_absoffset=1200)
		
		class AnalysisData():
			X = np.array([dates_skycolor1.values()[item] \
			 for item in match_dates.keys()])
			Xerr = np.array([dates_skycolor_err1.values()[item] \
			 for item in match_dates.keys()])
			Y = np.array([dates_skycolor2.values()[item] \
			 for item in match_dates.values()])
			Yerr = np.array([dates_skycolor_err2.values()[item] \
			 for item in match_dates.values()])
			
			'''
			filterpattern = np.array(ImageInfo.limits_colorcolor[0]<Y)*\
			 np.array(Y<ImageInfo.limits_colorcolor[1])
			X = X[filterpattern]
			Y = Y[filterpattern]
			Yerr = Yerr[filterpattern]
			'''
					
			colormap = cm.Blues; color=0.5; colorpoints=0.75
		
		class AnalysisData_july():
			julyfilter = self.create_filter_bydate(\
			 mean_dates.values(),\
			 datetime_from_number([20120701,120000]),\
			 datetime_from_number([20120801,120000]))
			X = np.array(AnalysisData.X[julyfilter])
			Xerr = np.array(AnalysisData.Xerr[julyfilter])
			Y = np.array(AnalysisData.Y[julyfilter])
			Yerr = np.array(AnalysisData.Yerr[julyfilter])
			colormap = cm.Reds; color='orange'; colorpoints='orange'
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)
		
		print('Ploting Sky Color-Color for '+filterA1+'-'+filterA2+' vs '+\
		 filterB1+'-'+filterB2)
		
		# Create the graph
		AnalysisData.xlabel = filterA1[-1]+' - '+filterA2[-1]+' [mag/arcsec2]'
		AnalysisData.ylabel = filterB1[-1]+' - '+filterB2[-1]+' [mag/arcsec2]'
		AnalysisData.title  = 'Night Sky Color-Color for '+str(filterB1[-1])+\
		 '-'+str(filterB2[-1])+' vs '+str(filterA1[-1])+'-'+str(filterA2[-1])
		 
		# Create the graph
		AnalysisData_july.xlabel = filterA1[-1]+' - '+filterA2[-1]+' [mag/arcsec2]'
		AnalysisData_july.ylabel = filterB1[-1]+' - '+filterB2[-1]+' [mag/arcsec2]'
		AnalysisData_july.title  = 'Night Sky Color-Color for '+str(filterB1[-1])+\
		 '-'+str(filterB2[-1])+' vs '+str(filterA1[-1])+'-'+str(filterA2[-1])
		
		data_evolgraph = Graph()
		data_evolgraph.fix_aspect_ratio()
		
		data_evolgraph.plot_scatter_2dhistogram(\
		 AnalysisData,bins=20,contours=False,alpha_contours=0.3)
		
		try:
			data_evolgraph.plot_scatter_2dhistogram(\
			 AnalysisData_july,bins=20,contours=False,alpha_contours=0.3)
		except:
			print('Error ploting July data')
		
		data_evolgraph.title_and_labels(AnalysisData)
		data_evolgraph.clip_data(AnalysisData)
		data_evolgraph.thegraph.set_xlim(ImageInfo.limits_colorcolor[0],ImageInfo.limits_colorcolor[1])
		data_evolgraph.thegraph.set_ylim(ImageInfo.limits_colorcolor[0],ImageInfo.limits_colorcolor[1])
		
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_skycolor_color_'+\
		 filterB1+'-'+filterB2+'_'+filterA1+'-'+filterA2+'.png')
		
		return(0)
	
	
	def bias_background_statistics(self,filtername,ImageInfo):
		''' 
		Perform the Background bias analysis.
		
		This pipeline works as follows:
		- Select data that fits a given filter in the complete data list.
		- Filter the data to get only clear nights.
		- Perform a long-term analysis to see how the Data has changed over the time
		- Plot the evolution graph and show on screen / save to disk.
		- Perform an histogram to see the frequencies of the data.
		- Plot the histogram and show on screen / save to disk.
		'''
		
		self.select_bias_filter(filtername)
		self.select_biasdata_prefilter_bydate(ImageInfo)
		
		class AnalysisData():
			X = self.bias_dates
			Y = self.bias_value+ImageInfo.valueerror_cornerbias[0]
			Yerr = np.array(self.bias_error**2+ImageInfo.valueerror_cornerbias[1]**2)
			if filtername=='Johnson_B': color = 'blue'; colorpoints=0.75
			elif filtername=='Johnson_V': color = 'green'; colorpoints=0.75
			elif filtername=='Johnson_R': color = 'red'; colorpoints=0.75
			elif filtername=='Johnson_U': color = 'purple'; colorpoints=0.75
			elif filtername=='Johnson_I': color = 'orange'; colorpoints=0.75
			else: color = 0.25; colorpoints=0.75
			
		
		''' Check uncertainties '''
		self.check_uncertainties(AnalysisData) 
		''' Check for valid data '''
		self.check_validdata(AnalysisData)

		print('Performing background bias histogram and graph for filter '+filtername)
		
		for iteration in xrange(2):
			data_evolution = MedianFit(AnalysisData)
			
			# Needs a sigma filtering of the data.
			AnalysisData.Ypred = \
			 [data_evolution.median_values]*len(AnalysisData.Y)
			AnalysisData.Xpred = AnalysisData.X
			AnalysisData = sigma_clipping(\
			 fdata = AnalysisData,\
			 sigmaX = 0,\
			 sigmaY = data_evolution.median_error,\
			 max_sigmaX = 2,\
			 max_sigmaY = 2)
		
		# Create the graph
		AnalysisData.xlabel = 'Date'
		AnalysisData.ylabel = 'Image Bias (counts)'
		AnalysisData.title  = 'Image Bias evolution - '+str(filtername)
		
		data_evolgraph = Graph()
		data_evolgraph.plot_median_fit(AnalysisData,maxerrorbars=30)
		if DEBUG==True: data_evolgraph.show_plot()
		data_evolgraph.save_fig(base_path+'/summary_date_imagebias_'+filtername+'.png')
		
		# Histogram graph
		AnalysisData.xlabel = 'Image Bias (counts)'
		AnalysisData.ylabel = 'Counts'
		AnalysisData.title  = 'Image Bias histogram - '+str(filtername)
		
		data_histogram = Histogram(AnalysisData,bins=20)
		data_histogram.plot_histogram(AnalysisData)
		if DEBUG==True: data_histogram.show_plot()
		data_histogram.save_fig(\
		 base_path+'/summary_imagebias_hist_'+filtername+'.png')
		
		# Median Fit
		print(\
		 'Image Bias Median Fit: '+\
		 str('%.3f' %data_evolution.median_values)+' +/- '+\
		 str('%.3f' %data_evolution.median_error)
		)
		
		return(0)
	
		

def data_analysis(each_filter,ImageInfo):
	'''
	Analysis that applies to only 1 filter
	'''
	
	# Calibration data
	#verbose(Analysis.zeropoint_date_analysis,each_filter,ImageInfo)
	
	verbose(Analysis.extinction_date_analysis,each_filter,ImageInfo)
	#verbose(Analysis.extinction_vs_zeropoint,each_filter,ImageInfo)
	verbose(Analysis.skybrightness_date_analysis,each_filter,ImageInfo)
	verbose(Analysis.cloudcoverage_date_analysis,each_filter,ImageInfo)
	
	verbose(Analysis.extinction_vs_cloudcoverage,each_filter,ImageInfo)
	verbose(Analysis.skybrightness_vs_cloudcoverage,each_filter,ImageInfo)
	
	# Monthly data
	verbose(Analysis.cloudcoverage_monthly_analysis,each_filter,ImageInfo)
	
	# Seasonal
	verbose(Analysis.cloudcoverage_season_analysis,each_filter,ImageInfo)
	verbose(Analysis.skybrightness_season_analysis,each_filter,ImageInfo)
	verbose(Analysis.extinction_season_analysis,each_filter,ImageInfo)
	
	# Galactic dependence
	verbose(Analysis.skybrightness_galacticlatitude_analysis,each_filter,ImageInfo)
	#verbose(Analysis.skybrightness_galacticlongitude_analysis,each_filter,ImageInfo)
	verbose(Analysis.skybrightness_galacticlatitudelongitude_analysis,each_filter,ImageInfo)
	
	# Ecliptic dependence
	verbose(Analysis.skybrightness_eclipticlatitude_analysis,each_filter,ImageInfo)
	#verbose(Analysis.skybrightness_eclipticlongitude_analysis,each_filter,ImageInfo)
	verbose(Analysis.skybrightness_eclipticlatitudelongitude_analysis,each_filter,ImageInfo)
	
	# Moon phase and position
	verbose(Analysis.skybrightness_moon_phasezdist_analysis,each_filter,ImageInfo)
	
	# Solar activity (From Sunspots and from SDO 0.1-7nm)
	verbose(Analysis.skybrightness_solar_xuv_flux,each_filter,ImageInfo)
	
	# Time of night analysis
	verbose(Analysis.skybrightness_time_analysis,each_filter,ImageInfo)
	verbose(Analysis.extinction_time_analysis,each_filter,ImageInfo)
	
	# Bias image analysis
	if ImageInfo.analyzebias == True:
		verbose(Analysis.bias_background_statistics,each_filter,ImageInfo)
	

def data_analysis_doublefilter(filters,ImageInfo):
	'''
	Analysis that depends on 2 filters (colors, extinction vs extinction and so)
	'''
	for fnum1,filter1 in enumerate(filters):
		for fnum2,filter2 in enumerate(filters):
			if fnum1<fnum2:
				verbose(Analysis.skycolor_solar_xuv_flux,filter1,filter2,ImageInfo)
				verbose(Analysis.extinctionslope_analysis,filter1,filter2,ImageInfo)
				verbose(Analysis.skycolor_date_analysis,filter1,filter2,ImageInfo)
				verbose(Analysis.skycolor_moon_phasezdist_analysis,filter1,filter2,ImageInfo)

def data_analysis_triplefilter(filters,ImageInfo):
	for fnumA1,filterA1 in enumerate(filters):
		for fnumA2,filterA2 in enumerate(filters):
			if fnumA1<fnumA2:
				for fnumB1,filterB1 in enumerate(filters):
					for fnumB2,filterB2 in enumerate(filters):
						if fnumB1<fnumB2 and (fnumA2<fnumB2 or fnumA1<fnumB1):
							verbose(Analysis.skycolor_color_analysis,\
							  filterA1,filterA2,filterB1,filterB2,ImageInfo)
							verbose(Analysis.extinction_extinction_analysis,\
							  filterA1,filterA2,filterB1,filterB2,ImageInfo)

def analize_all_data(ImageInfo):
	#for each_filter in ['Johnson_B','Johnson_V','Johnson_R']:
	
	filters_to_analyze = ['Johnson_B','Johnson_V','Johnson_R']
	
	request = [None for thefilter in filters_to_analyze]
	
	count = multiprocessing.cpu_count()
	pool = multiprocessing.Pool(processes=count)
	
	#for thefilter in filters_to_analyze: data_analysis(thefilter,ImageInfo) # Single thread
	
	# Multithread
	
	for number,thefilter in enumerate(filters_to_analyze):
		request[number] = \
		 pool.apply_async(data_analysis,(thefilter,ImageInfo))
	
	for number in xrange(len(filters_to_analyze)):
		request[number].get(timeout=1800)
	
	
	# Analysis that depends on two or more filters
	data_analysis_doublefilter(filters_to_analyze,ImageInfo)
	data_analysis_triplefilter(filters_to_analyze,ImageInfo)
	return(0)
	

if __name__ == '__main__':
	DEBUG=True
	
	# Read config and Observatory properties
	InputOptions = ReadOptions(sys.argv)
	if InputOptions.show_help==True:
		PlatformHelp_ = PlatformHelp()
		PlatformHelp_.show_help()
		exit(0)
	
	config_file  = get_config_filename(InputOptions)
	ImageInfo	= load_config_file(config_file)
	
	try:
		base_path = InputOptions.base_path
	except:
		print('Using the default base_path')
	
	Analysis = SummariesAnalysis(base_path)
	Analysis.search_biasfile(base_path)
	ImageInfo.analyzebias = bool(len(Analysis.biasfiles))
	
	print('Data loaded ... starting analysis')
	DEBUG=False
	verbose(analize_all_data,ImageInfo)
	
	
	
