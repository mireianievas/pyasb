#!/usr/bin/env python2

'''
Bouguer fitting module

Fit fluxes and star data to an extinction law to obtain
extinction and instrument zeropoint.
____________________________

This module is part of the PyASB project,
created and maintained by Miguel Nievas [UCM].
____________________________
'''

DEBUG=False

__author__ = "Miguel Nievas"
__copyright__ = "Copyright 2012, PyASB project"
__credits__ = ["Miguel Nievas"]
__license__ = "GNU GPL v3"
__shortname__ = "PyASB"
__longname__ = "Python All-Sky Brightness pipeline"
__version__ = "1.99.0"
__maintainer__ = "Miguel Nievas"
__email__ = "miguelnr89[at]gmail[dot]com"
__status__ = "Prototype" # "Prototype", "Development", or "Production"


try:
	import sys,os,inspect
	import matplotlib.pyplot as plt
	import matplotlib.colors as mpc
	import matplotlib.patches as mpp
	import scipy.stats as stats
	import math
	import numpy as np
	import astrometry
except:
	print("Bouguer, "+str(inspect.stack()[0][2:4][::-1])+': One or more modules missing')
	raise SystemExit

class BouguerFit():
	def __init__(self,ImageInfo,PhotometricCatalog):
		print('Calculating Instrument zeropoint and extinction ...')
		self.can_continue = False
		# Get Zero Point from ImageInfo and Stars from the Catalog
		self.bouguer_fixedy(ImageInfo)
		self.bouguer_data(PhotometricCatalog)
		# Set the default values
		self.bouguer_setdefaults(ImageInfo)
		# Try to fit the data
		try:
			self.bouguer_fit(ImageInfo)
			self.bouguer_plot(ImageInfo)
		except:
			print(str(inspect.stack()[0][2:4][::-1])+\
			 ' cannot fit the data. Npoints='+str(len(self.ydata)))
		else:
			self.can_continue = True
		
		assert(self.can_continue == True)

		print("Bouguer extinction fit results: \n"+\
		 " -> C=%.3f+/-%.3f, K=%.3f+/-%.3f, r=%.3f" \
		 %(self.Regression.mean_zeropoint,self.Regression.error_zeropoint,\
		   self.Regression.extinction,self.Regression.error_extinction,\
		   self.Regression.kendall_tau))

	def bouguer_data(self,StarCatalog):
		''' Get Star data from the catalog '''
		'''
		self.xdata = np.array([Star.airmass \
		 for Star in StarCatalog.StarList_Phot])
		self.ydata = np.array([Star.m25logF \
		 for Star in StarCatalog.StarList_Phot])
		self.yerr  = np.array([Star.m25logF_unc \
		 for Star in StarCatalog.StarList_Phot])
		'''
		self.xdata = np.array([Star.airmass for Star in StarCatalog.StarList_Phot])
		self.ydata = np.array([Star.m25logF for Star in StarCatalog.StarList_Phot])
		self.yerr  = np.array([Star.m25logF_unc for Star in StarCatalog.StarList_Phot])

	def bouguer_fixedy(self,ImageInfo):
		''' Try to get the fixed Y (zero point)'''
		try:
			self.fixed_y	 = ImageInfo.used_zero_point[0]
			self.fixed_y_unc = ImageInfo.used_zero_point[1]
			self.yisfixed=True
		except:
			if DEBUG==True: print(str(inspect.stack()[0][2:4][::-1])+' dont fix the Zero Point')
			self.yisfixed=False

	def bouguer_setdefaults(self,ImageInfo):
		''' Set default values (just in case that the bouguer fit fails '''
		if self.yisfixed == True:
			class Regression:
				mean_zeropoint  = self.fixed_y
				error_zeropoint = self.fixed_y_unc
				mean_slope = 10.0
				error_slope = 10.0
				extinction  = 10.0
				error_extinction = 10.0
				kendall_tau = 0.0
				Nstars_initial = 0
				Nstars_final = 0
				Nstars_rel = 0
			self.Regression = Regression()
			self.can_continue = True

	def bouguer_fit(self,ImageInfo):
		'''
		Fit measured fluxes to an extinction model
		Return regression parameters (ZeroPoint, Extinction)
		'''

		if self.yisfixed:
			self.Regression = TheilSenRegression(\
			 X = self.xdata,\
			 Y = self.ydata,\
			 ImageInfo = ImageInfo,\
			 Yerr = self.yerr,\
			 y0 = self.fixed_y,\
			 y0err = self.fixed_y_unc)
		else:
			try:
				self.Regression = TheilSenRegression(\
				 X = self.xdata,\
				 Y = self.ydata,\
				 ImageInfo = ImageInfo,\
				 Yerr = self.yerr,)
			except:
				print(inspect.stack()[0][2:4][::-1]);
				raise

		# Apply bad point filter to data
		self.xdata = self.xdata[self.Regression.badfilter]
		self.ydata = self.ydata[self.Regression.badfilter]
		self.yerr = self.yerr[self.Regression.badfilter]

	def bouguer_plot(self,ImageInfo):
		if ImageInfo.bouguerfit_path==False:
			# Don't draw anything
			print('Skipping BouguerFit Graph')
			return(None)

		''' Plot photometric data from the bouguer fit '''

		xfit = np.linspace(1,astrometry.calculate_airmass(ImageInfo.min_altitude),10)
		yfit = np.polyval([self.Regression.mean_slope,self.Regression.mean_zeropoint],xfit)

		bouguerfigure = plt.figure(figsize=(8,6))
		bouguerplot = bouguerfigure.add_subplot(111)
		bouguerplot.set_title('Bouguer extinction law fit\n',size="xx-large")
		bouguerplot.set_xlabel('Airmass')
		bouguerplot.set_ylabel(r'$m_0+2.5\log_{10}(F)$',size="large")
		bouguerplot.errorbar(\
			self.xdata, self.ydata, yerr=self.yerr, \
			fmt='D', mec='black', ecolor='black', mfc='blue')
		bouguerplot.plot(xfit,yfit,'r-')

		try:
			plot_infotext = \
				str("%s \n%.3f     %.3f\n%s    ChiSq/dof=%.3f/%d\n"\
				  %(ImageInfo.date_string,ImageInfo.latitude,ImageInfo.longitude,\
				  ImageInfo.used_filter,self.Regression.chi_square,\
				  self.Regression.degoffreedom))+\
				str("C=%.3f" %float(self.Regression.mean_zeropoint))+\
				str("+/-%.3f\n" %float(self.Regression.error_zeropoint))+\
				str("K=%.3f" %float(self.Regression.extinction))+\
				str("+/-%.3f\n"%float(self.Regression.error_slope))+\
				str("%.0f%s of %d photometric measures shown"\
				  %(self.Regression.Nstars_rel,'%',self.Regression.Nstars_initial))

			bouguerplot.text(0.05,0.05,plot_infotext,fontsize='x-small',transform = bouguerplot.transAxes)
		except:
			print(inspect.stack()[0][2:4][::-1])
			raise

		def bouguer_filename(ImageInfo):
			filename = ImageInfo.bouguerfit_path +\
				"/BouguerFit_"+ImageInfo.obs_name+"_"+ImageInfo.fits_date+"_"+\
				ImageInfo.used_filter+".png"
			return(filename)

		# Show or save the bouguer plot
		if ImageInfo.bouguerfit_path=="screen":
			plt.show()
		else:
			plt.savefig(bouguer_filename(ImageInfo),bbox_inches='tight')

		#plt.clf()
		#plt.close('all')

class TheilSenRegression():
	# Robust Theil Sen estimator, instead of the classic least-squares.
	def __init__(self,X,Y,ImageInfo,Yerr=None,y0=None,y0err=None,x0=None,x0err=None):
		print(' -> Check if we have enough points')
		assert(len(X)==len(Y) and len(Y)>2)
		self.Xpoints = np.array(X)
		self.Ypoints = np.array(Y)
		if Yerr is not None:
			self.Ypointserr = np.array(Yerr)
		else:
			self.Ypointserr = None

		if y0!=None:
			self.fixed_zp = True
			self.y0 = y0
			if y0err!=None:
				self.y0err=y0err
			else:
				self.y0err=0.0

			if x0!=None:
				self.x0 = x0
				if x0err!=None:
					self.x0err = 0.0
			else:
				self.x0 = 0.0
				self.x0err = 0.0
		else: self.fixed_zp = False
		self.Nstars_initial = len(self.Ypoints)
		self.Nstars_final = self.Nstars_initial
		self.pair_blacklist = []
		# Perform the regression
		print(' -> 1st regression (with all points)')
		self.perform_regression()
		# Delete bad points
		self.delete_bad_points(ImageInfo)
		# Try to improve the regression with filtered data
		print(' -> 2nd regression (w/o outliers)')
		self.perform_regression()

		self.Nstars_final = sum(self.badfilter)
		self.Nstars_rel = 100.*self.Nstars_final/self.Nstars_initial

	def perform_regression(self):
		# Prepare data for regression
		self.build_matrix_values()
		self.build_complementary_matrix()
		self.build_slopes_matrix()
		self.upper_diagonal_slope_matrix_values()
		# Slope
		print('  - Calculating slope')
		self.calculate_mean_slope()
		# Zero point
		print('  - Calculating zeropoint')
		self.build_zeropoint_array()
		self.calculate_mean_zeropoint()
		# Errors and fit quality
		print('  - Calculating residuals')
		self.calculate_residuals()
		print('  - Calculating fit goodness')
		self.calculate_kendall_tau()
		self.calculate_chi2()
		print('  - Calculating errors')
		self.calculate_errors()

		if self.fixed_zp == True:
			self.mean_zeropoint = self.y0
			self.error_zeropoint = self.y0err

		self.Nstars_final = len(self.Ypoints)

	def build_matrix_values(self):
		self.X_matrix_values = \
			np.array([[column for column in self.Xpoints] for line in self.Xpoints])
		self.Y_matrix_values = \
			np.array([[line for line in self.Ypoints] for line in self.Ypoints])

	def build_complementary_matrix(self):
		if self.fixed_zp == False:
			self.X_complementary_values = self.X_matrix_values.transpose()
			self.Y_complementary_values = self.Y_matrix_values.transpose()
		if self.fixed_zp == True:
			self.X_complementary_values = np.array([[self.x0\
				for column in self.Xpoints] for line in self.Xpoints])
			self.Y_complementary_values = np.array([[self.y0\
				for column in self.Ypoints] for line in self.Ypoints])

	def build_slopes_matrix(self):
		self.slopes_matrix = \
			((self.Y_matrix_values-self.Y_complementary_values +1e-20)/ \
			(self.X_matrix_values-self.X_complementary_values +1e-20))
		# +1e-20 lets us hide Numpy warning with 0/0

	def upper_diagonal_slope_matrix_values(self):
		self.upper_diag_slopes = \
			np.array([self.slopes_matrix[l][c] \
			for l in xrange(len(self.slopes_matrix)) \
			for c in xrange(len(self.slopes_matrix[0])) if c>l])

	def calculate_mean_slope(self):
		self.mean_slope = np.median(self.upper_diag_slopes)
		self.extinction = -self.mean_slope

	def build_zeropoint_array(self):
		self.zeropoint_array = self.Ypoints - self.Xpoints*self.mean_slope

	def calculate_mean_zeropoint(self):
		self.mean_zeropoint  = np.median(self.zeropoint_array)

	def calculate_residuals(self):
		self.residuals = self.zeropoint_array-self.mean_zeropoint

	def delete_bad_points(self,ImageInfo):
		# 3*std_residuals threshold
		std_residual = np.std(self.residuals)
		self.badfilter = np.abs(self.residuals)<np.abs(ImageInfo.lim_Kendall_tau*std_residual)
		self.Xpoints = self.Xpoints[self.badfilter]
		self.Ypoints = self.Ypoints[self.badfilter]
		if self.Ypointserr is not None:
			self.Ypointserr = self.Ypointserr[self.badfilter]

	def calculate_errors(self):
		xmedcuad = np.median(self.Xpoints)**2
		xcuaddif = self.Xpoints**2 - xmedcuad
		xdensity = np.sum(xcuaddif)
		sigma2_res = (1./(self.Nstars_final-2))*abs(sum(self.residuals))
		sigma2_slope = sigma2_res/abs(xdensity)
		sigma2_int = sigma2_res*(1./self.Nstars_final + 1.*xmedcuad/abs(xdensity))

		self.error_slope = stats.t.ppf(0.975,self.Nstars_final-2) * math.sqrt(sigma2_slope)
		self.error_zeropoint = stats.t.ppf(0.975,self.Nstars_final-2) * math.sqrt(sigma2_int)
		self.error_extinction = self.error_slope

	def calculate_kendall_tau(self):
		self.kendall_tau = \
			(1.*np.sum(self.upper_diag_slopes>0)-1.*np.sum(self.upper_diag_slopes<0))\
			/(1.*np.size(self.upper_diag_slopes))

	def calculate_chi2(self):
		if self.Ypointserr is None:
			self.chi_square = -1
			self.reduced_chi_square = -1
		else:
			self.degoffreedom = len(self.residuals)-2
			self.chi_square = np.sum(self.residuals**2 *1./self.Ypointserr)
			self.reduced_chi_square = self.chi_square*1./self.degoffreedom


class InstrumentCalibration():
	# Fit a Bouguer extinction law
	def __init__(self,ImageInfo,StarCatalog):
		try:
			self.BouguerFit = BouguerFit(ImageInfo,StarCatalog)
		except Exception as e:
			print(inspect.stack()[0][2:4][::-1])
			print('Cannot perform the Bouguer Fit. Error is: ')
			print type(e)
			print e
			exit(0)
			#raise

