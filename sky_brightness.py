#!/usr/bin/env python2

'''
Sky Brightness photometry module

Measure Sky brightness from the image using previous
instrument calibration.

____________________________

This module is part of the PyASB project,
created and maintained by Miguel Nievas [UCM].
____________________________
'''

__author__ = "Miguel Nievas"
__copyright__ = "Copyright 2012, PyASB project"
__credits__ = ["Miguel Nievas"]
__license__ = "GNU GPL v3"
__shortname__ = "PyASB"
__longname__ = "Python All-Sky Brightness pipeline"
__version__ = "1.99.0"
__maintainer__ = "Miguel Nievas"
__email__ = "miguelnr89[at]gmail[dot]com"
__status__ = "Prototype" # "Prototype", "Development", or "Production"


try:
	import sys,os,inspect	
	import numpy as np
	import scipy.interpolate as sint
	import matplotlib as mpl
	import matplotlib.pyplot as plt
	import matplotlib.colors as mpc
	import matplotlib.patches as mpp
	
	# Custom projection
	from matplotlib.projections.geo import HammerAxes
	import matplotlib.projections as mprojections
	from matplotlib.axes import Axes
	from matplotlib.patches import Wedge
	import matplotlib.spines as mspines

	from astrometry import *
	
	
except:
	print("NSB, "+str(inspect.stack()[0][2:4][::-1])+': One or more modules missing')
	raise SystemExit


class SkyBrightness():
	'''
	Class with Sky Brightness measure methods.
	Init requires FitsImage (with Coordinates), ImageInfo and Bouguerfit objects.
	'''

	def __init__(self,FitsImage,ImageInfo,BouguerFit):
		if ImageInfo.skybrightness_map_path!=False:
			print('Measuring All-Sky Sky Brightness ...')
			#NOTE: This function is very slow, I need to figure how to improve it.
			self.measure_in_grid(FitsImage,ImageInfo,BouguerFit)
			self.sbdata_table(ImageInfo)
		else:
			print('Measuring SB only at zenith ...')
		self.measure_in_positions(FitsImage,ImageInfo,BouguerFit)
		
		print("Night sky background at zenith: %.3f+/-%.3f" \
		 %(self.SBzenith,self.SBzenith_err))

	@staticmethod
	def sky_brightness_region(BouguerFit,ImageInfo,fits_region_values,limits):
		'''
		First we estimate the pixel_scale,
		then calculate the mean sky brightness in the region
		'''

		num_pixeles = np.size(fits_region_values)
		azseparation = (limits['az_max']-limits['az_min'])%(360+1e-8) * np.pi/180.
		zdseparation = np.cos((90-limits['alt_max'])*np.pi/180.)-np.cos((90-limits['alt_min'])*np.pi/180.+1e-8)

		sky_surface = azseparation*zdseparation*(3600*180./np.pi)**2
		pixel_scale = abs(sky_surface*1./num_pixeles)

		#sky_flux = 2.5*np.median(fits_region_values)-1.5*np.mean(fits_region_values)
		sky_flux = np.median(fits_region_values)
		sky_flux_err = np.std(fits_region_values)/np.sqrt(np.size(fits_region_values))

		sky_brightness = BouguerFit.Regression.mean_zeropoint - \
			2.5*np.log10(sky_flux/(ImageInfo.exposure*pixel_scale))
		sky_brightness_err = np.sqrt(BouguerFit.Regression.error_zeropoint**2 +\
			(2.5*sky_flux_err/(np.log(10)*sky_flux))**2)

		return(sky_brightness,sky_brightness_err)

	def measure_in_grid(self,FitsImage,ImageInfo,BouguerFit):
		''' Returns sky brightness measures in a grid with a given separation
			in degrees and interpolates the result with griddata.'''
		azseparation  = 30
		zdseparation = 15

		self.AZdirs  = np.arange(0,360+1,azseparation)
		try: assert(ImageInfo.nsbmap_projection=='hammer')
		except: self.ZDdirs = np.arange(0,75+1,zdseparation)
		else: self.ZDdirs = np.arange(0,90+1,zdseparation)
		
		self.ALTdirs = 90-self.ZDdirs

		self.AZgrid,self.ZDgrid = np.meshgrid(self.AZdirs,self.ZDdirs)
		self.ALTgrid = 90.-self.ZDgrid

		def sky_brightness_point(az,zd):
			alt = 90.-zd

			limits = {\
			 'alt_min':max(0,alt-zdseparation/2.),\
			 'alt_max':min(90,alt+zdseparation/2.),\
			 'az_min':az-azseparation/2.,\
			 'az_max':az+azseparation/2.,\
			}

			fits_region_values = FitsImage.fits_data[\
				(np.array(FitsImage.Coordinates.altitude_map>=limits['alt_min'])*\
				 np.array(FitsImage.Coordinates.altitude_map<limits['alt_max'])\
				)*(\
				(np.array(FitsImage.Coordinates.azimuth_map>=limits['az_min'])*\
				 np.array(FitsImage.Coordinates.azimuth_map<limits['az_max'])\
				)+\
				(np.array(FitsImage.Coordinates.azimuth_map>=limits['az_min']+360)+\
				 np.array(FitsImage.Coordinates.azimuth_map<limits['az_max']-360)\
				))]
				# The last two lines must be set to get a smooth transition between 360 and 0 degrees azimuth

			return(self.sky_brightness_region(BouguerFit,ImageInfo,fits_region_values,limits))

		sky_brightness_list = np.vectorize(sky_brightness_point)
		self.SBgrid,self.SBgrid_errors = np.array(sky_brightness_list(self.AZgrid,self.ZDgrid))

		# Once we measured the sky brightness in the image, convert to radians the azimuths
		self.AZgrid = self.AZgrid*np.pi/180.

	def sbdata_table(self,ImageInfo):
		try:
			assert(ImageInfo.skybrightness_table_path!=False)
		except:
			print(inspect.stack()[0][2:4][::-1])
			print('Skipping write skybrightness table to file')
		else:
			print('Write skybrightness table to file')

			header = '#Altitude\Azimuth'
			for az_ in  self.AZdirs:
				header += ', '+str(az_)
			header+='\n'

			content = [header]

			for k,alt_ in enumerate(self.ALTdirs):
				line = str(alt_)
				for j in xrange(len(self.AZdirs)):
					line += ', '+str("%.3f" % float(self.SBgrid[k][j])) +\
					' +/- ' + str("%.3f" % float(self.SBgrid_errors[k][j]))
				line+='\n'
				content.append(line)

			if ImageInfo.skybrightness_table_path == "screen":
				print(content)
			else:
				def sbtable_filename(ImageInfo):
					filename = ImageInfo.skybrightness_table_path +\
						"/SBTable_"+ImageInfo.obs_name+"_"+ImageInfo.fits_date+"_"+\
						ImageInfo.used_filter+".txt"
					return(filename)

				sbdatafile = open(sbtable_filename(ImageInfo),'w+')
				sbdatafile.writelines(content)
				sbdatafile.close()


	def measure_in_positions(self,FitsImage,ImageInfo,BouguerFit):
		# Measure Sky Brightness at zenith
		try:
			# If previous grid calculus, then extract from that grid
			self.SBzenith = np.median(self.SBgrid[self.ZDgrid==0])
			self.SBzenith_err = np.max(self.SBgrid_errors[self.ZDgrid==0])
		except:
			# If not previous grid, calculate manually.
			zenith_acceptance = 10
			limits = {\
			 'alt_min':90-zenith_acceptance,\
			 'alt_max':90,\
			 'az_min':0,\
			 'az_max':360-1e-6,\
			}
			fits_zenith_region_values = FitsImage.fits_data[\
				FitsImage.Coordinates.altitude_map>=90-zenith_acceptance]
			self.SBzenith,self.SBzenith_err = \
				self.sky_brightness_region(BouguerFit,ImageInfo,fits_zenith_region_values,limits)


class SemiHammerAxes(HammerAxes):
	name = 'hammer'
	
	def autoscale_None(self):
		pass
	
	def cla(self):
		HammerAxes.cla(self)
		Axes.set_xlim(self, -np.pi, np.pi)
		Axes.set_ylim(self, 0 , np.pi/2.0)
				
	def _gen_axes_patch(self):
		"""
		Override this method to define the shape that is used for the
		background of the plot.  It should be a subclass of Patch.
		"""
		return Wedge((0.5,0.5), 0.5, 0,180)
		#return Circle((0.5, 0.5), 0.5)
		
	def _gen_axes_spines(self):
		path = Wedge((0, 0), 1.0, 0, 180).get_path()
		spine = mspines.Spine(self, 'circle', path)
		spine.set_patch_circle((0.5, 0.5), 0.5)
		return {'wedge':spine}

mprojections.register_projection(SemiHammerAxes)

class SkyBrightnessGraph():
	def __init__(self,SkyBrightness,ImageInfo,BouguerFit):
		if ImageInfo.skybrightness_map_path==False:
			# Don't draw anything
			print('Skipping SkyBrightness Graph ...')
			return(None)
		else:
			print('Generating Sky Brightness Map ...')

		try: assert(ImageInfo.nsbmap_projection=='hammer')
		except:
			self.projection='polar'
		else:
			self.projection='hammer'
		
		self.create_plot()
		self.plot_labels(SkyBrightness,ImageInfo,BouguerFit)
		self.define_contours(ImageInfo)
		self.ticks_and_locators()
		self.grid_data(SkyBrightness)
		self.plot_data()
		self.color_bar()
		self.show_map(ImageInfo)

	def create_plot(self):
		''' Create the figure (empty) with matplotlib '''
		self.SBfigure = plt.figure(figsize=(8,8))
		self.SBgraph  = self.SBfigure.add_subplot(111,projection=self.projection)

	def grid_data(self,SkyBrightness):
		# Griddata.
		self.AZgridi,self.ZDgridi = np.mgrid[0:2*np.pi:1000j, 0:90:1000j]

		coord_reshape = [[SkyBrightness.AZgrid[j][k],SkyBrightness.ZDgrid[j][k]] \
			for k in xrange(len(SkyBrightness.AZgrid[0])) \
			for j in xrange(len(SkyBrightness.AZgrid))]

		data_reshape = [ SkyBrightness.SBgrid[j][k] \
			for k in xrange(len(SkyBrightness.AZgrid[0])) \
			for j in xrange(len(SkyBrightness.AZgrid))]

		self.SBgridi = sint.griddata(coord_reshape,data_reshape, \
			(self.AZgridi,self.ZDgridi), method='linear')

	def plot_data(self):
		
		if self.projection=='hammer':
			self.xi = self.AZgridi-np.pi
			self.yi = np.pi/2 - self.ZDgridi*np.pi/180.
		elif self.projection=='polar':
			self.xi = self.AZgridi
			self.yi = self.ZDgridi
		
		''' Returns the graph with data plotted.'''
		self.SBcontoursf = self.SBgraph.contourf(\
			self.xi, self.yi, self.SBgridi, \
			cmap=plt.cm.YlGnBu,levels=self.level_list)
		self.SBcontours  = self.SBgraph.contour(\
			self.xi, self.yi, self.SBgridi,
			colors='k',alpha=0.3,levels=self.coarse_level_list)
		self.SBcontlabel = self.SBgraph.clabel(self.SBcontours,\
			inline=True,fmt='%.1f',fontsize=10)

	def plot_labels(self,SkyBrightness,ImageInfo,BouguerFit):
		''' Set the figure title and add extra information (annotation) '''
		# Image title
		self.SBgraph.text(0,90, unicode(ImageInfo.backgroundmap_title,'utf-8'),\
			horizontalalignment='center',size='xx-large')

		# Image information
		image_information = str(ImageInfo.date_string)+" UTC\n"+str(ImageInfo.latitude)+5*" "+\
			str(ImageInfo.longitude)+"\n"+ImageInfo.used_filter+4*" "+\
			"K="+str("%.3f" % float(BouguerFit.Regression.extinction))+"+-"+\
			str("%.3f" % float(BouguerFit.Regression.error_extinction))+"\n"+\
			"SB="+str("%.2f" % float(SkyBrightness.SBzenith))+"+-"+\
			str("%.2f" % float(SkyBrightness.SBzenith_err))+" mag/arcsec2 (zenith)"

		onlydatetime_information = \
			str(ImageInfo.date_string)+" UTC | +"+str(ImageInfo.latitude)+", "+\
			str(ImageInfo.longitude)+" | "+ImageInfo.used_filter
		
		if self.projection=='polar':
			#self.SBgraph.text(5*np.pi/4,125,unicode(image_information,'utf-8'),fontsize='x-small')
			self.SBgraph.text(5*np.pi/4,125,unicode(onlydatetime_information,'utf-8'),fontsize='large')
		elif self.projection=='hammer':
			self.SBgraph.text(0.5,1.02,unicode(onlydatetime_information,'utf-8'),\
			 fontsize='large',transform = self.SBgraph.transAxes)

	def define_contours(self,ImageInfo):
		''' Calculate optimal contours for pyplot.contour and pyplot.contourf '''
		_min_ = float(ImageInfo.background_levels[ImageInfo.used_filter][0])
		_max_ = float(ImageInfo.background_levels[ImageInfo.used_filter][1])

		sval = 0.1
		def create_ticks(_min_,_max_,sval):
			return  np.arange(_min_,_max_+sval/10.,sval)

		self.level_list = create_ticks(_min_,_max_,0.1)
		self.label_list = create_ticks(_min_,_max_,sval)
		self.coarse_level_list = create_ticks(_min_,_max_,0.2)

		while len(self.label_list)>15:
			sval = sval*2.
			self.label_list = create_ticks(_min_,_max_,sval)

		if len(self.level_list)<3: self.update_ticks=False
		else: self.update_ticks=True

	def ticks_and_locators(self):
		if self.projection=='polar':
			''' Add ticks to the graph '''
			radial_locator = np.arange(10,90+1,10)
			radial_label = ["$80$","$70$","$60$","$50$","$40$","$30$","$20$","$10$","$0$"]
			theta_locator = np.arange(0,360,45)
			theta_label = ["$N$","$NE$","$E$","$SE$","$S$","$SW$","$W$","$NW$"]
			self.SBgraph.set_rgrids(radial_locator,radial_label,size="large",color='k',alpha=0.75)
			self.SBgraph.set_thetagrids(theta_locator,theta_label,size="large")
			# rotate the graph (North up)
			self.SBgraph.set_theta_direction(-1)
			self.SBgraph.set_theta_offset(np.pi/2)
			# Limits
			self.SBgraph.set_ylim(0,75)
		elif self.projection=='hammer':
			# Change plot size
			self.SBfigure.set_size_inches(15,8)
			# Plot the grid
			plt.grid(True)
			#self.SBgraph.set_ylim([0,np.pi/2.0]) # Hammer projection is defined to ignore the limit!.
			self.SBgraph.set_latitude_grid(10)
			self.SBgraph.set_longitude_grid(45)
			# Limits
			#mpl_axes.Axes.set_ylim(self.SBgraph,0,np.pi/2.0)			

	def color_bar(self):
		''' Add the colorbar '''
		
		if self.projection=='polar':
			# Separation between colour bar and graph
			self.SBfigure.subplots_adjust(right=1.05)
			# Color bar
			self.SBcolorbar = plt.colorbar(self.SBcontoursf,orientation='vertical',shrink=0.75)
			self.SBfigure.subplots_adjust(right=0.85) # Restore separation
			self.SBcolorbar.set_label("mag/arcsec2",rotation="vertical",size="large")
		elif self.projection=='hammer':
			#colorbar_axes = self.SBfigure.add_axes([0.1, 0.1, 0.8, 0.05])
			#self.SBcolorbar = self.SBfigure.colorbar(self.SBgraph,colorbar_axes,orientation='horizontal')
			self.SBcolorbar = plt.colorbar(self.SBcontoursf,orientation='horizontal',shrink=0.6,pad=-0.4)
			self.SBcolorbar.set_label("mag/arcsec2",rotation="horizontal",size="medium")
		
		self.SBcolorbar.set_ticks(self.label_list,update_ticks=self.update_ticks)

	def show_map(self,ImageInfo):
		def skybrightness_filename(ImageInfo):
			filename = ImageInfo.skybrightness_map_path +\
				"/SkyBrightnessMap_"+ImageInfo.obs_name+"_"+ImageInfo.fits_date+"_"+\
				ImageInfo.used_filter+".png"
			return(filename)

		#plt.show(self.SBfigure)		
		if ImageInfo.skybrightness_map_path=="screen":
			plt.show()
		else:
			plt.savefig(skybrightness_filename(ImageInfo),bbox_inches='tight')

		#plt.clf()
		#plt.close('all')


class MeasureSkyBrightness():
	def __init__(self,FitsImage,ImageInfo,BouguerFit):
		SkyBrightnessData  = SkyBrightness(FitsImage,ImageInfo,BouguerFit)
		SkyBrightnessImage = SkyBrightnessGraph(SkyBrightnessData,ImageInfo,BouguerFit)
		self.SBzenith = SkyBrightnessData.SBzenith
		self.SBzenith_err = SkyBrightnessData.SBzenith_err
