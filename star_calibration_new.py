#!/usr/bin/env python2

'''
Load Catalog file and make PyASB StarCatalog

This module loads the catalog file and returns 
an array of Star objects (StarCatalog) with
their fluxes.

____________________________

This module is part of the PyASB project, 
created and maintained by Miguel Nievas [UCM].
____________________________
'''

DEBUG = False

__author__ = "Miguel Nievas"
__copyright__ = "Copyright 2012, PyASB project"
__credits__ = ["Miguel Nievas"]
__license__ = "GNU GPL v3"
__shortname__ = "PyASB"
__longname__ = "Python All-Sky Brightness pipeline"
__version__ = "1.99.0"
__maintainer__ = "Miguel Nievas"
__email__ = "miguelnr89[at]gmail[dot]com"
__status__ = "Prototype"     # "Prototype", "Development", or "Production"

try:
    import sys
    import os
    import inspect
    import ephem
    import gmatch
    import scipy.stats
    import scipy.ndimage as sndimage
    import scipy.ndimage.morphology as smorphology
    import scipy.ndimage.measurements as smeasurements
    import scipy.ndimage.filters as sfilters
    from astrometry import *
    from skymap_plot import *
except:
    print(str(inspect.stack()[0][2:4][::-1])+': One or more modules missing')
    raise SystemExit


def verbose(function, *args):
    '''
    Run a function in verbose mode
    '''
    try:
        out = function(*args)
    except:
        # Something happened while runing function
        # raise
        if DEBUG is True:
            print(str(inspect.stack()[0][2:4][::-1])+' Error')
            raise
    else:
        return(out)


class Star(object):
    """ Star proterties and methods. """
    def __init__(self):
        ''' 
        The only thing we want to do now is the catalog_phase
        Dont run the image_phase because we dont know if the star
        is detectable yet.
        '''
        self.catalog_phase()

    def catalog_phase(self):
        '''
        Starting Point: 
         - Catalog star, which contains RA,DEC, photometric properties.
        Goal: 
         - Position in the image X,Y
         '''
        self.destroy=False
        self.saturated=False
        self.cold_pixels=False
        self.masked=False
        self.to_be_masked=False
        
        # Extract stars from Catalog
        self.verbose_detection(self.from_catalog,StarCatalogLine,\
         errormsg=' Error extracting from catalog')
        # Estimate magnitude on the given image
        self.verbose_detection(self.magnitude_on_image,ImageInfo,\
         errormsg=' Error reading used filter or magnitude>max')
        # Astrometry for the current star
        self.verbose_detection(self.star_astrometry,ImageInfo,\
         errormsg=' Error performing star astrometry, Star not visible?')
    
    def set_position(self,Y,X):
        '''
        Set the position
        '''
        self.Xcoord = X
        self.Ycoord = Y

    def image_phase(self):
    	'''
        Starting Point: 
         - Detected star, with is position that match a given catalog star.
        Goal: 
         - Position in the image X,Y
         - Set flags if star is not good for photometric calibration
           (cold pixels, hot pixels, weak detection)
         - Measure fluxes
         - Calculate variables to be used in the calibration
         '''

        # Estimate radius to do the aperture photometry
        self.verbose_detection(self.photometric_radius,ImageInfo,\
         errormsg=' Error generating photometric radius')

        # Create regions of stars and star+background
        self.verbose_detection(self.estimate_fits_region_star,FitsImage,\
         errormsg=' Cannot create the Star region')
        self.verbose_detection(self.estimate_fits_region_complete,FitsImage,\
         errormsg=' Cannot create the Star+Background region')

        '''
        # Measure fluxes (CHECK IF WE REALLY HAVE TO DO IT)
        self.verbose_detection(self.measure_star_fluxes,FitsImage.fits_data,\
         errormsg=' Error measuring fluxes')

        # Check if star is detectable (CHECK IF WE REALLY HAVE TO DO IT)
        self.verbose_detection(self.star_is_detectable,ImageInfo,\
         errormsg=' Star is not detectable')
		'''

        # Check star saturation
        self.verbose_detection(self.star_is_saturated,ImageInfo,\
         errormsg=' Star is satured or has hot pixels')
        # Check cold pixels
        self.verbose_detection(self.star_has_cold_pixels,ImageInfo,\
         errormsg=' Star has cold pixels')
        # Check if star region is masked
        self.verbose_detection(self.star_region_is_masked,FitsImage,\
         errormsg=' Star is masked')

        # Optimal aperture photometry
        self.verbose_detection(\
         self.optimal_aperture_photometry,ImageInfo,FitsImage.fits_data,\
         errormsg=' Error doing optimal photometry')

        # Check if star is detectable (with optimal astrometry)
        self.verbose_detection(self.star_is_detectable,ImageInfo,\
         errormsg=' Star is not detectable')

        # Calculate Bouguer variables
        self.verbose_detection(self.photometry_bouguervar,ImageInfo,\
         errormsg=' Error calculating bouguer variables')

        # Append star to star mask
        if self.to_be_masked==True:
            try: self.append_to_star_mask(FitsImage)
            except: print(str(inspect.stack()[0][2:4][::-1])+' Cannot add star to mask')

        if DEBUG==True:
			print('Clearing Object')

			if self.destroy==False:
				print(self.name + ' DONE. Star detected.')

		self.__clear__()



    def from_catalog(self,CatalogLine):
        ''' 
        Populate class with properties extracted from catalog:
        recno, HDcode, RA1950, DEC1950, Vmag, U_V, B_V, R_V, I_V 
        '''
        
        def coord_pyephem_format(coord_str):
            # Return coordinate in pyepheem str
            while coord_str[0]==' ':
                coord_str = coord_str[1:]
            
            coord_separated = coord_str.split(' ')
            coord_pyephem = str(int(coord_separated[0]))+\
                ':'+str(int(coord_separated[1]))+\
                ":"+str(float(coord_separated[2]))
            return coord_pyephem
        
        def get_float(value):
            '''
            Try to get the magnitude of the star,
            If it is missing, then flat it as Incomplete Photometry
            '''
            try:
                return(float(value))
            except:
                self.IncompletePhot = True
                return(0)
        
        def star_is_photometric(self):
            '''
            Flag the star for its photometry usefulness.
            It must have a complete photometric magnitudes
            and not to be double, variable or with 
            [manual flag] bad photometric properties
            '''
            
            self.PhotometricStandard=True
            if self.isDouble or self.isVariab or self.isBadPhot or self.IncompletePhot: 
                self.PhotometricStandard = False
            
            # Also, if colors are very blue or very red, discard them
            if self.U_V<0 or self.B_V>1.5:
                self.PhotometricStandard = False
        
        self.IncompletePhot = False
        try:
            self.recno     = int(CatalogLine[0])
            self.HDcode    = str(CatalogLine[1]).replace(' ','')
            self.RA2000    = coord_pyephem_format(CatalogLine[2])
            self.DEC2000   = coord_pyephem_format(CatalogLine[3])
            self.RA1950    = coord_pyephem_format(CatalogLine[4])
            self.DEC1950   = coord_pyephem_format(CatalogLine[5])
            self.Vmag      = get_float(CatalogLine[6])
            self.U_V       = get_float(CatalogLine[7])
            self.B_V       = get_float(CatalogLine[8])
            self.R_V       = get_float(CatalogLine[9])
            self.I_V       = get_float(CatalogLine[10])
            self.isDouble  = bool(len(str(CatalogLine[11]).replace(' ','')))
            self.isVariab  = bool(len(str(CatalogLine[12]).replace(' ','')))
            self.r_SpType  = str(CatalogLine[13]).replace(' ','')
            self.r_SpType  = str(CatalogLine[14])#.replace(' ','')
            self.isBadPhot = bool(len(str(CatalogLine[15]).replace(' ','')))
            
            '''
            try:
                # Try to find the common name
                self.name = str(CatalogLine[9])
            except:
                # Use the HDcode as name
                self.name = self.HDcode
            '''
            
            self.name = self.HDcode
            
            star_is_photometric(self)
            
            self.Umag = self.Vmag + self.U_V
            self.Bmag = self.Vmag + self.B_V 
            self.Rmag = self.Vmag + self.R_V
            self.Imag = self.Vmag + self.I_V
        except:
            self.destroy=True
    
    def magnitude_on_image(self,ImageInfo):
        ''' Set the magnitude and color (#-V) that match image filter.'''
        if ImageInfo.used_filter=="Johnson_U":
            self.FilterMag = self.Umag
            self.Color     = self.U_V
        elif ImageInfo.used_filter=="Johnson_B":
            self.FilterMag = self.Bmag
            self.Color     = self.B_V
        elif ImageInfo.used_filter=="Johnson_V":
            self.FilterMag = self.Vmag
            self.Color     = 0.0
        elif ImageInfo.used_filter=="Johnson_R":
            self.FilterMag = self.Rmag
            self.Color     = self.R_V
        elif ImageInfo.used_filter=="Johnson_I":
            self.FilterMag = self.Imag
            self.Color     = self.I_V
        else: 
            self.destroy=True
        
        try:
            assert(self.FilterMag<ImageInfo.max_magnitude)
            assert(self.Bmag<ImageInfo.max_magnitude+1)
            assert(self.Vmag<ImageInfo.max_magnitude+1)
            assert(self.Rmag<ImageInfo.max_magnitude+1)
        except:
            self.destroy=True
    
    def star_astrometry(self,ImageInfo):
        ''' Perform astrometry. 
        Returns (if star is visible and well defined) 
        its position on the sky and image'''
        
        ObsPyephem = pyephem_setup_real(ImageInfo)
        
        def pyephem_declaration(self,ObsPyephem):
            ''' Define the star in Pyephem to make astrometric calculations '''
            pyephem_star = ephem.FixedBody()
            pyephem_star = ephem.readdb('"'+str(self.name)+'"'+\
             ",f|S|A0,"+str(self.RA1950)+'|0'+\
             ","+str(self.DEC1950)+'|0'+","+str(self.Vmag)+',1950,0"')
            pyephem_star.compute(ObsPyephem)
            return pyephem_star
        
        try:
            pyephem_star = pyephem_declaration(self,ObsPyephem)
        except:
            self.destroy=True
            
        if self.destroy==False:
            # The catalog is defined for B1950, get the current coordinates
            ra  = float(pyephem_star.a_ra)*12./np.pi
            dec = float(pyephem_star.a_dec)*180./np.pi
            
            # Get the horizontal coordinates
            self.azimuth,self.altit_real = eq2horiz(ra,dec,ImageInfo)
            
            try:
                assert(self.altit_real)>float(ImageInfo.min_altitude)
            except:
                self.destroy=True
            else:
                self.zdist_real = 90.0-self.altit_real
        
        if self.destroy==False:
            # Apparent coordinates in sky. Atmospheric refraction effect.
            self.altit_appa = atmospheric_refraction(self.altit_real,'dir')
            try:
                assert(self.altit_appa)>float(ImageInfo.min_altitude)
            except:
                self.destroy=True
            else:
                self.zdist_appa = 90.0-self.altit_appa
                self.airmass    = calculate_airmass(self.altit_appa)
        
        if self.destroy==False:            
            # Get the X,Y image coordinates
            XYCoordinates = horiz2xy(self.azimuth,self.altit_appa,ImageInfo)
            self.Xcoord = XYCoordinates[0]
            self.Ycoord = XYCoordinates[1]
            try:
                assert(self.Xcoord>0. and self.Xcoord<ImageInfo.resolution[0])
                assert(self.Ycoord>0. and self.Ycoord<ImageInfo.resolution[1])
            except:
                self.destroy=True

    def photometric_radius(self,ImageInfo):
        ''' Needs astrometry properties, photometric filter properties and ImageInfo
            Returns R1,R2 and R3 '''
        try:
            '''Returns R1,R2,R3. Needs photometric properties and astrometry.'''
            MF_magn = 10**(-0.4*self.FilterMag)
            MF_reso = 0.5*(min(ImageInfo.resolution)/2500)
            MF_texp = 0.1*ImageInfo.exposure
            MF_airm = 0.7*self.airmass
            MF_totl = 1+MF_magn+MF_reso+MF_texp+MF_airm
            
            self.R1 = int(ImageInfo.base_radius*MF_totl)
            self.R2 = self.R1*2
            self.R3 = self.R1*3
        except:
            self.destroy=True

    def estimate_fits_region_star(self,FitsImage):
        ''' Return the region that contains the star 
        (both for calibrated and uncalibrated data)'''
        self.fits_region_star = [[FitsImage.fits_data[y,x] \
            for x in xrange(\
            	int(self.Xcoord - self.R1 + 0.5),\
            	int(self.Xcoord + self.R1 + 0.5))] \
            for y in xrange(\
            	int(self.Ycoord - self.R1 + 0.5),\
            	int(self.Ycoord + self.R1 + 0.5))]

        # We will need this to look for saturated and cold pixels.
        self.fits_region_star_uncalibrated = [[FitsImage.fits_data_notcalibrated[y,x] \
            for x in xrange(\
            	int(self.Xcoord - self.R1 + 0.5),\
            	int(self.Xcoord + self.R1 + 0.5))] \
            for y in xrange(\
            	int(self.Ycoord - self.R1 + 0.5),\
            	int(self.Ycoord + self.R1 + 0.5))]
        
        # We have computed the star region. Flag it to be masked
        self.to_be_masked=True
    
    def estimate_fits_region_complete(self,FitsImage):
        ''' Return the region that contains the star+background '''
        self.fits_region_complete = [[FitsImage.fits_data[y,x] \
            for x in xrange(\
            	int(self.Xcoord - self.R3 + 0.5),\
            	int(self.Xcoord + self.R3 + 0.5))] \
            for y in xrange(\
            	int(self.Ycoord - self.R3 + 0.5),\
            	int(self.Ycoord + self.R3 + 0.5))]
    
    def measure_star_fluxes(self,fits_data):
        '''Needs self.Xcoord, self.Ycoord and self.R[1-3] defined
           Returns star fluxes'''
        
        # Pixels in each ring
        def less_distance(Xi,Yi,reference):
            # returns True if distance from pixel to the star 
            # center is less than a value. False otherwise
            return (Xi)**2 + (Yi)**2 <= reference**2
        
        try
        	# Reference (center of the region)
        	center_x = len(self.fits_region_complete)/2.
        	center_y = len(self.fits_region_complete[0])/2.

        	'''
        	Create circle (R1) and rings (R2 and R3)
        	Pixel1 (circle) -> Star flux + background.
        	Pixel2 (ring)   -> security, not used. 
        	Pixel3 (ring)   -> background estimation
        	'''

            self.pixels1 = [self.fits_region_complete[y][x] \
                for y in xrange(len(self.fits_region_complete))\
                for x in xrange(len(self.fits_region_complete[0])) \
                if less_distance(x-center_x,y-center_y,self.R1)]
            
            self.pixels2 = [self.fits_region_complete[y][x] \
                for y in xrange(len(self.fits_region_complete))\
                for x in xrange(len(self.fits_region_complete[0])) \
                if less_distance(x-center_x,y-center_y,self.R2) and\
                not less_distance(x-center_x,y-center_y,,self.R1)]
            
            self.pixels3 = [self.fits_region_complete[y][x] \
                for y in xrange(len(self.fits_region_complete))\
                for x in xrange(len(self.fits_region_complete[0])) \
                if less_distance(x-center_x,y-center_y,self.R3) and\
                not less_distance(x-center_x,y-center_y,,self.R2)]
            
            # Sky background flux. t_student 95%.
            t_skyflux = scipy.stats.t.isf(0.025,np.size(self.pixels3))
            self.skyflux = np.median(self.pixels3)
            self.skyflux_err = t_skyflux*np.std(self.pixels3)/np.sqrt(np.size(self.pixels3))
            # Sky background + Star flux
            totalflux = np.sum(self.pixels1)
            # Only star flux. 
            self.starflux = totalflux - np.size(self.pixels1)*self.skyflux
            self.starflux_err = np.sqrt(2)*np.size(self.pixels1)*self.skyflux_err
        except:
            self.destroy=True
    
    def star_region_is_masked(self,FitsImage):
        ''' Check if the star is in the star mask'''
        self.masked = False

        range_x = xrange(\
        	int(self.Xcoord - self.R1 + 0.5),\
        	int(self.Xcoord + self.R1 + 0.5))

        range_y = xrange(\
        	int(self.Ycoord - self.R1 + 0.5),\
        	int(self.Ycoord + self.R1 + 0.5))

        for x in range_x:
            for y in range_y:
                if FitsImage.star_mask[y][x] == True:
                    self.masked=True
                    self.destroy = True
                    return(0)

    def star_is_saturated(self,ImageInfo):
        ''' Return true if star has one or more saturated pixels 
            requires a defined self.fits_region_star'''
        try:
        	# We use the number of bits of CCD as reference.
        	limit = (2./3)*2**ImageInfo.ccd_bits
            assert(np.max(self.fits_region_star_uncalibrated)<limit)
        except:
            #self.destroy=True
            self.PhotometricStandard=False
            self.saturated=True
        else:
            self.saturated=False
    
    def star_has_cold_pixels(self,ImageInfo):
        ''' Return true if star has one or more cold (0 value) pixels 
            requires a defined self.fits_region_star'''
        try:
        	cold_value = 0
            assert(np.min(self.fits_region_star_uncalibrated)>cold_value)
        except:
            #self.destroy=True
            self.PhotometricStandard=False
            self.cold_pixels=True
        else:
            self.cold_pixels=False
    
    def star_is_detectable(self,ImageInfo):
        ''' Set a detection limit to remove weak stars'''
        ''' Check if star is detectable '''
        try:
        	threshold = ImageInfo.baseflux_detectable*self.starflux_err+1e-8
        	assert(self.starflux>threshold)
        except:
            self.destroy=True
    
    def optimal_aperture_photometry(self,ImageInfo,fits_data,increment0=0.002):
        '''
        Optimize the aperture to minimize uncertainties and
        check that nearly all flux is contained in R1
        increment0 is the increment needed to trigger a new 
          iteration (R1=R1+1) at first iteration. The real
          increment is calculed as increment0*num_iteration**2
          to limit the number of iterations.
        '''
        
        try:
            radius = (ImageInfo.base_radius+self.R1)/2.
            iterate = True
            num_iterations = 0
            
            self.starflux = 0
            while iterate:
                num_iterations+=1
                old_starflux = self.starflux
                self.R1 = radius
                self.measure_star_fluxes(fits_data)
                if self.starflux < (1+increment0*num_iterations**2)*old_starflux:
                    iterate=False
                else:
                    radius+=1
                
                assert(radius<self.R2)
        except:
            self.destroy=True
    
    def photometry_bouguervar(self,ImageInfo):
        # Calculate parameters used in bouguer law fit
        try:
            _25logF      = 2.5*np.log10(self.starflux/ImageInfo.exposure)
            _25logF_unc  = (2.5/np.log(10))*self.starflux_err/self.starflux
            color_term = ImageInfo.color_terms[ImageInfo.used_filter][0]
            color_term_err = ImageInfo.color_terms[ImageInfo.used_filter][1]
            self.m25logF     = self.FilterMag+_25logF+color_term*self.Color
            self.m25logF_unc = np.sqrt(_25logF_unc**2 + (color_term_err*self.Color)**2)
        except:
            self.PhotometricStandard=False
            #self.destroy=True

    def append_to_star_mask(self,FitsImage):
        for x in xrange(int(self.Xcoord - self.R1 + 0.5),int(self.Xcoord + self.R1 + 0.5)):
            for y in xrange(int(self.Ycoord - self.R1 + 0.5),int(self.Ycoord + self.R1 + 0.5)):
                FitsImage.star_mask[y][x] = True
    
    def __clear__(self):
        backup_attributes = [\
         "destroy","PhotometricStandard","HDcode","name","FilterMag",\
         "Color","saturated","cold_pixels","masked","RA1950","DEC1950",\
         "azimuth","altit_real","airmass","Xcoord","Ycoord",\
         "R1","R2","R3","starflux","starflux_err","m25logF","m25logF_unc"\
         ]
        for atribute in list(self.__dict__):
            #if atribute[0]!="_" and atribute not in backup_attributes:
            if atribute not in backup_attributes:
                del vars(self)[atribute]



class Catalog(object):
    """ Catalog with astrometry and photometry properties for each star """

    def __init__(self):
        pass

    def load_catalog_file(self,catalog_filename):
        ''' Returns Catalog lines from the catalog_filename '''
        
        def line_is_star(textline,sep=';'):
            theline = textline.split(sep)
            try:
                int(theline[0].replace(' ','')[0])
            except:
                return(False)
            else:
                return(True)
        
        def catalog_separation(textline,sep=';'):
            theline = textline
            theline = theline.replace('\r\n','')
            theline = theline.replace('\n','')
            theline = theline.split(sep)
            return(theline)
        
        try:
            catalogfile = open(catalog_filename, 'r')
            CatalogContent = catalogfile.readlines()
            MinLine = 1; separator = ";"
            self.CatalogLines = [catalog_separation(CatalogContent[line])
             for line in xrange(len(CatalogContent)) \
             if line>=MinLine-1 and line_is_star(CatalogContent[line])]
        except IOError:
            print('IOError. Error opening file '+catalog_filename+'.')
            #return 1
        except:
            print('Unknown error:')
            raise
            #return 2
        else:
            print('File '+str(catalog_filename)+' opened correctly.')
        finally:
            catalogfile.close()


    def process_catalog(self,FitsImage,ImageInfo):
        '''Create the masked star matrix'''
        FitsImage.star_mask = np.zeros(np.array(FitsImage.fits_data).shape,dtype=bool)
        
        '''Returns the processed catalog. '''
        self.StarList_Det = []
        self.StarList_Phot = []
        self.StarList_Tot = []
        
        try: ImageInfo.max_star_number
        except: ImageInfo.max_star_number = len(self.CatalogLines)
        
        self.StarsInCatalog = []
        for each_star in self.CatalogLines[0:ImageInfo.max_star_number]:
            NewStar = Star(each_star,FitsImage,ImageInfo)
            if NewStar.destroy is False:
                self.StarsInCatalog.append(NewStar)






class CalibratedImage(FitsImage):
    """ Image with stars and their fluxes """
    def __init__(self):
        '''
        Load fits_data from FitsImage
        '''
        #self.fits_data = FitsImage.fits_data
        #self.fits_uncalibrated_data = FitsImage.fits_data_notcalibrated

    def blind_star_finder(self):

        '''
        Takes an image and detect the peaks using the local maximum filter.
        Returns the location of the peaks, two arrays for Y and X positions.
        '''

        # Search window (in px)
        neighborhood_size = 10
        filter_size = 2

        # Detection threshold
        nsigma = 6

        # Filtered image (noise reduction)
        filtered_data = filters.median_filter(\
            input = self.fits_data_notcalibrated,\
            size = filter_size)

        #apply the local maximum/minimum filter; all pixel of maximal value 
        #in their neighborhood are set to 1
        local_max = sfilters.maximum_filter(\
            input = filtered_data, \
            size = neighborhood_size)==filtered_data

        # Detected peaks (stars)
        detected_peaks = local_max

        # Get the labels for each star
        labels,nlabels = smeasurements.label(local_max)

        # Get the neighbors for each labeled star
        i, j = smorphology.distance_transform_edt(\
            input = (labels == 0),\
            return_distances = False,\
            return_indices = True)

        neighborhoods = labels[i, j]

        # Remove pixels heavy contaminated by stars light 
        # in the background estimation. Iterative.

        for it in xrange(2):
            #print("Star removal iteration %d" %(it))
            
            background = smeasurements.mean(\
                input = filtered_data,\
                labels = neighborhoods,\
                index = neighborhoods)

            # Remove from the mask.
            neighborhoods[filtered_data>background] = 0


        local_min = smeasurements.minimum(\
            input = filtered_data, \
            labels = neighborhoods, \
            index = neighborhoods)
        
        local_std = smeasurements.standard_deviation(\
            input = filtered_data,\
            labels = neighborhoods,\
            index = neighborhoods)

        # Only preserve the values in peaks position.
        local_max = filtered_data*local_max*detected_peaks
        local_min = local_min*detected_peaks
        local_mean = local_mean*detected_peaks
        local_std = local_std*detected_peaks

        '''
        print("Local Max (max value): %.2f" %(np.max(local_max)))
        print("Local Max (min value): %.2f" %(np.min(local_max)))
        print("Local Mean (max value): %.2f" %(np.max(local_mean)))
        print("Local Mean (min value): %.2f" %(np.min(local_mean)))
        print("Local Std (max value): %.2f" %(np.max(local_std)))
        print("Local Std (min value): %.2f" %(np.min(local_std)))
        '''

        # Select the candidates
        candidates = (local_max - local_min) > nsigma*local_std
        detected_peaks[candidates == False] = 0
          
        # Number of stars after selection
        #print("After selection, %d stars detected", %(np.sum(detected_peaks)))
        
        return(np.where(detected_peaks))

    def match_image_catalog(self,StarsInCatalog):

    	# Stars with calculated position (from catalog)
  		self.StarList_Tot = StarsInCatalog

        # Extract calculated position for each star
        StarsPositionCatalog = np.array([[Star.Ycoord,Star.Xcoord] for Star in StarsInCatalog])
        StarsCatalogDict = dict(zip(StarsPositionCatalog, StarsInCatalog))
        
        # List of stars found in the image
        StarsPositionImage = self.blind_star_finder()

        # Match both lists
        matches = gmatch(StarsPositionCatalog, StarsPositionImage, eps=1e-3)

        StarsFound = [StarsCatalogDict[matches[k]] for k in xrange(len(matches))]

        self.StarList_Det = []
		self.StarList_Phot = []

        # Fix the position using the detected position
        for k,match in enumerate(matches):
            TheStar = StarsFound[k]
            Ycoord = match[0]
            Xcoord = match[1]

            # Fix the star position.
            TheStar.set_position(Y,X)
            # 2nd phase, photometry.
            TheStar.image_phase():

            # List of stars that should be detected

            if TheStar.destroy==False:
            	# Add to the list of detected stars
            	self.StarList_Det.append(PhotometricStar)
            	if TheStar.PhotometricStandard==True:
            		# Add to the list of stars with good photometric properties
            		self.StarList_Phot.append(PhotometricStar)

        print(" - Total stars:      %d" %(len(self.StarList_Tot)))
		print(" - Detected stars:   %d" %(len(self.StarList_Det)))
		print(" - With photometry:  %d" %(len(self.StarList_Phot)))


	def save_to_file(self,ImageInfo):

		try:
			assert(ImageInfo.photometry_table_path!=False)
		except:
			print('Skipping write photometric table to file')
		else:
			print('Write photometric table to file')
			
			content = ['#HDcode, CommonName, RA1950, DEC1950, Azimuth, '+\
			 'Altitude, Airmass, Magnitude, Color(#-V), StarFlux, StarFluxErr, '+\
			 'mag+2.5logF, [mag+2.5logF]_Err\n']
			for Star in self.StarList_Phot:
				text_content = str(\
					"%s, %s, %s, %s, %.5f, %.5f, %.3f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f \n" 
					%(Star.HDcode, Star.name, Star.RA1950, Star.DEC1950, Star.azimuth, \
					Star.altit_real, Star.airmass, Star.FilterMag, Star.Color, \
					Star.starflux, Star.starflux_err, Star.m25logF, Star.m25logF_unc))

				content.append(text_content)
			
			if ImageInfo.photometry_table_path == "screen":
				print(content)
			else:
				def phottable_filename(ImageInfo):
					filename = \
						ImageInfo.photometry_table_path +\
						"/PhotTable_"+ImageInfo.obs_name+"_"+\
						ImageInfo.fits_date+"_"+ImageInfo.used_filter+".txt"
					return(filename)
				
				photfile = open(phottable_filename(ImageInfo),'w+')
				photfile.writelines(content)
				photfile.close()



















