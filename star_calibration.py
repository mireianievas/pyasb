#!/usr/bin/env python2

'''
Load Catalog file and make PyASB StarCatalog

This module loads the catalog file and returns
an array of Star objects (StarCatalog) with
their fluxes.

____________________________

This module is part of the PyASB project,
created and maintained by Miguel Nievas [UCM].
____________________________
'''

DEBUG = True

__author__ = "Miguel Nievas"
__copyright__ = "Copyright 2012, PyASB project"
__credits__ = ["Miguel Nievas"]
__license__ = "GNU GPL v3"
__shortname__ = "PyASB"
__longname__ = "Python All-Sky Brightness pipeline"
__version__ = "1.99.0"
__maintainer__ = "Miguel Nievas"
__email__ = "miguelnr89[at]gmail[dot]com"
__status__ = "Prototype"	 # "Prototype", "Development", or "Production"

try:
	import sys
	import os
	import inspect
	import ephem
	#import gmatch
	import match_coords
	import scipy.stats
	import scipy.ndimage as sndimage
	import scipy.ndimage.morphology as smorphology
	import scipy.ndimage.measurements as smeasurements
	import scipy.ndimage.filters as sfilters
	from load_fitsimage import *
	from astrometry import *
	from skymap_plot import *
except:
	raise
	print("StarCal, " +str(inspect.stack()[0][2:4][::-1])+': One or more modules missing')
	raise SystemExit


def verbose(function, *args):
	'''
	Run a function in verbose mode
	'''
	try:
		out = function(*args)
	except:
		# Something happened while runing function
		# raise
		if DEBUG is True:
			print(str(inspect.stack()[0][2:4][::-1])+' Error')
			raise
	else:
		return(out)


def get_float(value):
	'''
	Try to get the magnitude of the star,
	If it is missing, then flat it as Incomplete Photometry
	'''
	try:
		return(float(value))
	except:
		return(None)


class CatalogStar():
	""" Star proterties and methods. """
	def __init__(self,ImageInfo,StarCatalogLine):
		'''
		The only thing we want to do now is the catalog_phase
		Dont run the image_phase because we dont know if the star
		is detectable yet.
		'''
		self.catalog_phase(ImageInfo,StarCatalogLine)

	def catalog_phase(self,ImageInfo,StarCatalogLine):
		'''
		Starting Point:
		 - Catalog star, which contains RA,DEC, photometric properties.
		Goal:
		 - Position in the image X,Y
		 '''
		self.destroy=False
		self.saturated=False
		self.cold_pixels=False
		self.masked=False

		# Extract stars from Catalog
		self.verbose_detection(self.from_catalog,StarCatalogLine,\
		 errormsg=' Error extracting from catalog')
		# Estimate magnitude on the given image
		self.verbose_detection(self.magnitude_on_image,ImageInfo,\
		 errormsg=' Error reading used filter or magnitude>max')
		# Astrometry for the current star
		self.verbose_detection(self.star_astrometry,ImageInfo,\
		 errormsg=' Error performing star astrometry, Star not visible?')

	def set_position(self,Y,X):
		'''
		Set the position
		'''
		self.Xcoord = X
		self.Ycoord = Y

	def image_phase(self,FitsImage,ImageInfo):
		'''
		Starting Point:
		 - Detected star, with is position that match a given catalog star.
		Goal:
		 - Position in the image X,Y
		 - Set flags if star is not good for photometric calibration
		   (cold pixels, hot pixels, weak detection)
		 - Measure fluxes
		 - Calculate variables to be used in the calibration
		 '''

		#self.to_be_masked=False

		# Estimate radius to do the aperture photometry
		self.verbose_detection(self.photometric_radius,ImageInfo,\
		 errormsg=' Error generating photometric radius')

		# Create regions of stars and star+background
		self.verbose_detection(self.estimate_fits_region_star,FitsImage,\
		 errormsg=' Cannot create the Star region')
		self.verbose_detection(self.estimate_fits_region_complete,FitsImage,\
		 errormsg=' Cannot create the Star+Background region')

		# Check star saturation
		self.verbose_detection(self.star_is_saturated,ImageInfo,\
		 errormsg=' Star is satured or has hot pixels')
		# Check cold pixels
		self.verbose_detection(self.star_has_cold_pixels,ImageInfo,\
		 errormsg=' Star has cold pixels')
		# Check if star region is masked
		self.verbose_detection(self.star_region_is_masked,FitsImage,\
		 errormsg=' Star is masked')


		# Optimal aperture photometry
		self.verbose_detection(\
		 self.optimal_aperture_photometry,ImageInfo,FitsImage.fits_data,\
		 errormsg=' Error doing optimal photometry')

		# Check if star is detectable (with optimal astrometry)
		self.verbose_detection(self.star_is_detectable,ImageInfo,\
		 errormsg=' Star is not detectable')

		# Calculate Bouguer variables
		self.verbose_detection(self.photometry_bouguervar,ImageInfo,\
		 errormsg=' Error calculating bouguer variables')

		# Append star to star mask
		#if self.to_be_masked==True:
		if self.destroy==False:
			try: self.append_to_star_mask(FitsImage)
			except: print(str(inspect.stack()[0][2:4][::-1])+' Cannot add star to mask')

		if DEBUG==True:
			print('Clearing Object')

			if self.destroy==False:
				print(self.name + ' DONE. Star detected.')

		self.__clear__()

	def verbose_detection(self,function, *args, **kwargs):
		'''
		Exec a detection step in verbose mode
		'''
		if self.destroy==False:
			function(*args)
			if self.destroy==True:
				if DEBUG==True:
					print(str(inspect.stack()[0][2:4][::-1])+str(function)+kwargs['errormsg'])


	def star_is_photometric(self,Filter=None):
		'''
		Flag the star for its photometry usefulness.
		It must have a complete photometric magnitudes
		and not to be double, variable or with
		[manual flag] bad photometric properties
		'''

		self.PhotometricStandard=True
		if self.isDouble or self.isVariab or self.isBadPhot or self.IncompletePhot:
			self.PhotometricStandard = False

		# Also, if colors are very blue or very red, discard them
		if Filter=="Johnson_U":
			if self.U_V<0:
				self.PhotometricStandard = False
		elif Filter=="Johnson_B":
			if self.B_V>1.5:
				self.PhotometricStandard = False

	def from_catalog(self,CatalogLine):
		'''
		Populate class with properties extracted from catalog:
		recno, HDcode, RA1950, DEC1950, Vmag, U_V, B_V, R_V, I_V
		'''

		def coord_pyephem_format(coord_str):
			# Return coordinate in pyepheem str
			while coord_str[0]==' ':
				coord_str = coord_str[1:]

			coord_separated = coord_str.split(' ')
			coord_pyephem = str(int(coord_separated[0]))+\
				':'+str(int(coord_separated[1]))+\
				":"+str(float(coord_separated[2]))
			return coord_pyephem

		self.IncompletePhot = False

		# Extract data from catalog

		try:
			self.recno		= int(CatalogLine[0])
			self.HDcode		= str(CatalogLine[1]).replace(' ','')
			self.RA2000		= coord_pyephem_format(CatalogLine[2])
			self.DEC2000	= coord_pyephem_format(CatalogLine[3])
			self.RA1950		= coord_pyephem_format(CatalogLine[4])
			self.DEC1950	= coord_pyephem_format(CatalogLine[5])
			self.Vmag		= get_float(CatalogLine[6])
			self.U_V		= get_float(CatalogLine[7])
			self.B_V		= get_float(CatalogLine[8])
			self.R_V		= get_float(CatalogLine[9])
			self.I_V		= get_float(CatalogLine[10])
			self.Colors		= CatalogLine[7:10+1] # U-V, B-V, R-V, I-V
			self.isDouble	= bool(len(str(CatalogLine[11]).replace(' ','')))
			self.isVariab	= bool(len(str(CatalogLine[12]).replace(' ','')))
			self.r_SpType	= str(CatalogLine[13]).replace(' ','')
			self.r_SpType	= str(CatalogLine[14])#.replace(' ','')
			self.isBadPhot	= bool(len(str(CatalogLine[15]).replace(' ','')))

			# The common name (assume HD number)
			self.name = self.HDcode

		except:
			raise
			self.destroy=True


	def magnitude_on_image(self,ImageInfo):
		''' Set the magnitude and color (#-V) that match image filter.'''

		try:
			''' Get the correct color (according to filter) '''

			# Flag temporary the star as bad.
			self.IncompletePhot = True

			if ImageInfo.used_filter=="Johnson_U":
				self.Color	 = get_float(self.Colors[0])
			elif ImageInfo.used_filter=="Johnson_B":
				self.Color	 = get_float(self.Colors[1])
			elif ImageInfo.used_filter=="Johnson_V":
				self.Color	 = 0.0
			elif ImageInfo.used_filter=="Johnson_R":
				self.Color	 = get_float(self.Colors[2])
			elif ImageInfo.used_filter=="Johnson_I":
				self.Color	 = get_float(self.Colors[3])
			else:
				self.destroy=True

			# If everything went fine, remove the flag
			self.IncompletePhot = False

			''' Get the correct magnitude (according to filter) '''
			self.FilterMag = self.Vmag + self.Color

			assert(self.FilterMag<ImageInfo.max_magnitude)

			# Check if we can use the star for photometry
			self.star_is_photometric(ImageInfo.used_filter)

		except:
			self.destroy=True

	def star_astrometry(self,ImageInfo):
		''' Perform astrometry.
		Returns (if star is visible and well defined)
		its position on the sky and image'''

		ObsPyephem = pyephem_setup_real(ImageInfo)

		def pyephem_declaration(self,ObsPyephem):
			''' Define the star in Pyephem to make astrometric calculations '''
			pyephem_star = ephem.FixedBody()
			pyephem_star = ephem.readdb('"'+str(self.name)+'"'+\
			 ",f|S|A0,"+str(self.RA1950)+'|0'+\
			 ","+str(self.DEC1950)+'|0'+","+str(self.Vmag)+',1950,0"')
			pyephem_star.compute(ObsPyephem)
			return pyephem_star

		try:
			pyephem_star = pyephem_declaration(self,ObsPyephem)
		except:
			self.destroy=True

		if self.destroy==False:
			# The catalog is defined for B1950, get the current coordinates
			ra  = float(pyephem_star.a_ra)*12./np.pi
			dec = float(pyephem_star.a_dec)*180./np.pi

			# Get the horizontal coordinates
			self.azimuth,self.altit_real = eq2horiz(ra,dec,ImageInfo)

			try:
				assert(self.altit_real)>float(ImageInfo.min_altitude)
			except:
				self.destroy=True
			else:
				self.zdist_real = 90.0-self.altit_real

		if self.destroy==False:
			# Apparent coordinates in sky. Atmospheric refraction effect.
			self.altit_appa = atmospheric_refraction(self.altit_real,'dir')
			try:
				assert(self.altit_appa)>float(ImageInfo.min_altitude)
			except:
				self.destroy=True
			else:
				self.zdist_appa = 90.0-self.altit_appa
				self.airmass	= calculate_airmass(self.altit_appa)

		if self.destroy==False:
			# Get the X,Y image coordinates
			XYCoordinates = horiz2xy(self.azimuth,self.altit_appa,ImageInfo)
			self.Xcoord = XYCoordinates[0]
			self.Ycoord = XYCoordinates[1]
			try:
				assert(self.Xcoord>0. and self.Xcoord<ImageInfo.resolution[0])
				assert(self.Ycoord>0. and self.Ycoord<ImageInfo.resolution[1])
			except:
				self.destroy=True

	def photometric_radius(self,ImageInfo):
		''' Needs astrometry properties, photometric filter properties and ImageInfo
			Returns R1,R2 and R3 '''
		try:
			'''Returns R1,R2,R3. Needs photometric properties and astrometry.'''
			MF_magn = 10**(-0.4*self.FilterMag)
			MF_reso = 0.5*(min(ImageInfo.resolution)/2500.)
			MF_texp = 0.1*ImageInfo.exposure
			MF_airm = 0.7*self.airmass
			MF_totl = 1+MF_magn+MF_reso+MF_texp+MF_airm

			self.R1 = int(ImageInfo.base_radius*MF_totl)
			self.R2 = self.R1*2
			self.R3 = self.R1*3
		except:
			self.destroy=True

	def estimate_fits_region_star(self,FitsImage):
		''' Return the region that contains the star
		(both for calibrated and uncalibrated data)'''
		self.fits_region_star = [[FitsImage.fits_data[y,x] \
			for x in xrange(\
				int(self.Xcoord - self.R1 + 0.5),\
				int(self.Xcoord + self.R1 + 0.5))] \
			for y in xrange(\
				int(self.Ycoord - self.R1 + 0.5),\
				int(self.Ycoord + self.R1 + 0.5))]

		# We will need this to look for saturated and cold pixels.
		self.fits_region_star_uncalibrated = [[FitsImage.fits_data_notcalibrated[y,x] \
			for x in xrange(\
				int(self.Xcoord - self.R1 + 0.5),\
				int(self.Xcoord + self.R1 + 0.5))] \
			for y in xrange(\
				int(self.Ycoord - self.R1 + 0.5),\
				int(self.Ycoord + self.R1 + 0.5))]

		# We have computed the star region. Flag it to be masked
		# self.to_be_masked=True

	def estimate_fits_region_complete(self,FitsImage):
		''' Return the region that contains the star+background '''
		self.fits_region_complete = [[FitsImage.fits_data[y,x] \
			for x in xrange(\
				int(self.Xcoord - self.R3 + 0.5),\
				int(self.Xcoord + self.R3 + 0.5))] \
			for y in xrange(\
				int(self.Ycoord - self.R3 + 0.5),\
				int(self.Ycoord + self.R3 + 0.5))]

	def measure_star_fluxes(self,fits_data):
		'''Needs self.Xcoord, self.Ycoord and self.R[1-3] defined
		   Returns star fluxes'''

		# Pixels in each ring
		def less_distance(Xi,Yi,reference):
			# returns True if distance from pixel to the star
			# center is less than a value. False otherwise
			return (Xi)**2 + (Yi)**2 <= reference**2

		try:
			# Reference (center of the region)
			center_y = len(self.fits_region_complete)/2.
			center_x = len(self.fits_region_complete[0])/2.

			'''
			Create circle (R1) and rings (R2 and R3)
			Pixel1 (circle) -> Star flux + background.
			Pixel2 (ring)   -> security, not used.
			Pixel3 (ring)   -> background estimation
			'''

			self.pixels1 = [self.fits_region_complete[y][x] \
				for y in xrange(len(self.fits_region_complete))\
				for x in xrange(len(self.fits_region_complete[0])) \
				if less_distance(x-center_x,y-center_y,self.R1)]

			self.pixels2 = [self.fits_region_complete[y][x] \
				for y in xrange(len(self.fits_region_complete))\
				for x in xrange(len(self.fits_region_complete[0])) \
				if less_distance(x-center_x,y-center_y,self.R2) and\
				not less_distance(x-center_x,y-center_y,self.R1)]

			self.pixels3 = [self.fits_region_complete[y][x] \
				for y in xrange(len(self.fits_region_complete))\
				for x in xrange(len(self.fits_region_complete[0])) \
				if less_distance(x-center_x,y-center_y,self.R3) and\
				not less_distance(x-center_x,y-center_y,self.R2)]

			# Sky background flux. t_student 95%.
			t_skyflux = scipy.stats.t.isf(0.025,np.size(self.pixels3))
			self.skyflux = np.median(self.pixels3)
			self.skyflux_err = t_skyflux*np.std(self.pixels3)/np.sqrt(np.size(self.pixels3))
			# Sky background + Star flux
			totalflux = np.sum(self.pixels1)
			# Only star flux.
			self.starflux = totalflux - np.size(self.pixels1)*self.skyflux
			self.starflux_err = np.sqrt(2)*np.size(self.pixels1)*self.skyflux_err
		except:
			self.destroy=True

	def star_region_is_masked(self,FitsImage):
		''' Check if the star is in the star mask'''
		self.masked = False

		range_x = xrange(\
			int(self.Xcoord - self.R1 + 0.5),\
			int(self.Xcoord + self.R1 + 0.5))

		range_y = xrange(\
			int(self.Ycoord - self.R1 + 0.5),\
			int(self.Ycoord + self.R1 + 0.5))

		for x in range_x:
			for y in range_y:
				if FitsImage.star_mask[y][x] == True:
					self.masked=True
					self.destroy = True
					return(0)

	def star_is_saturated(self,ImageInfo):
		''' Return true if star has one or more saturated pixels
			requires a defined self.fits_region_star'''
		try:
			# We use the number of bits of CCD as reference.
			limit = (2./3)*2**ImageInfo.ccd_bits
			assert(np.max(self.fits_region_star_uncalibrated)<limit)
		except:
			#self.destroy=True
			self.PhotometricStandard=False
			self.saturated=True
		else:
			self.saturated=False

	def star_has_cold_pixels(self,ImageInfo):
		''' Return true if star has one or more cold (0 value) pixels
			requires a defined self.fits_region_star'''
		try:
			cold_value = 0
			assert(np.min(self.fits_region_star_uncalibrated)>cold_value)
		except:
			#self.destroy=True
			self.PhotometricStandard=False
			self.cold_pixels=True
		else:
			self.cold_pixels=False

	def star_is_detectable(self,ImageInfo):
		''' Set a detection limit to remove weak stars'''
		''' Check if star is detectable '''
		try:
			threshold = 1*ImageInfo.baseflux_detectable*self.starflux_err+1e-8
			assert(self.starflux>threshold)
		except:
			self.destroy=True

	def optimal_aperture_photometry(self,ImageInfo,fits_data,increment0=0.002):
		'''
		Optimize the aperture to minimize uncertainties and
		check that nearly all flux is contained in R1
		increment0 is the increment needed to trigger a new
		  iteration (R1=R1+1) at first iteration. The real
		  increment is calculed as increment0*num_iteration**2
		  to limit the number of iterations.
		'''

		try:
			radius = (ImageInfo.base_radius+self.R1)/2.
			iterate = True
			num_iterations = 0

			self.starflux = 0
			while iterate:
				num_iterations+=1
				old_starflux = self.starflux
				self.R1 = radius
				self.measure_star_fluxes(fits_data)
				if self.starflux < (1+increment0*num_iterations**2)*old_starflux:
					iterate=False
				else:
					radius+=1

				assert(radius<self.R2)
		except:
			self.destroy=True

	def photometry_bouguervar(self,ImageInfo):
		# Calculate parameters used in bouguer law fit
		try:
			_25logF	  = 2.5*np.log10(self.starflux/ImageInfo.exposure)
			_25logF_unc  = (2.5/np.log(10))*self.starflux_err/self.starflux
			color_term = ImageInfo.color_terms[ImageInfo.used_filter][0]
			color_term_err = ImageInfo.color_terms[ImageInfo.used_filter][1]
			self.m25logF	 = self.FilterMag+_25logF+color_term*self.Color
			self.m25logF_unc = np.sqrt(_25logF_unc**2 + (color_term_err*self.Color)**2)
		except:
			self.PhotometricStandard=False
			#self.destroy=True

	def append_to_star_mask(self,FitsImage):
		range_x = xrange(int(self.Xcoord - self.R1 + 0.5),int(self.Xcoord + self.R1 + 0.5))
		range_y = xrange(int(self.Xcoord - self.R1 + 0.5),int(self.Xcoord + self.R1 + 0.5))

		for x in range_x:
			for y in range_y:
				FitsImage.star_mask[y][x] = True

	def __clear__(self):
		backup_attributes = [\
		 "destroy","PhotometricStandard","HDcode","name","FilterMag",\
		 "Color","saturated","cold_pixels","masked","RA1950","DEC1950",\
		 "azimuth","altit_real","airmass","Xcoord","Ycoord","xmatched",\
		 "R1","R2","R3","starflux","starflux_err","m25logF","m25logF_unc"\
		 ]
		for atribute in list(self.__dict__):
			#if atribute[0]!="_" and atribute not in backup_attributes:
			if atribute not in backup_attributes:
				del vars(self)[atribute]



class Catalog():
	""" Catalog with astrometry and photometry properties for each star """

	def __init__(self,ImageInfo):
		self.load_catalog_file(ImageInfo.catalog_filename)

	def load_catalog_file(self,catalog_filename):
		''' Returns Catalog lines from the catalog_filename '''

		def line_is_star(textline,sep=';'):
			theline = textline.split(sep)
			try:
				int(theline[0].replace(' ','')[0])
			except:
				return(False)
			else:
				return(True)

		def catalog_separation(textline,sep=';'):
			theline = textline
			theline = theline.replace('\r\n','')
			theline = theline.replace('\n','')
			theline = theline.split(sep)
			return(theline)

		try:
			catalogfile = open(catalog_filename, 'r')
			CatalogContent = catalogfile.readlines()
			MinLine = 1; separator = ";"
			self.CatalogLines = [catalog_separation(CatalogContent[line])
			 for line in xrange(len(CatalogContent)) \
			 if line>=MinLine-1 and line_is_star(CatalogContent[line])]
		except IOError:
			print('IOError. Error opening file '+catalog_filename+'.')
			#return 1
		except:
			print('Unknown error:')
			raise
			#return 2
		else:
			print('File '+str(catalog_filename)+' opened correctly.')
		finally:
			catalogfile.close()


class SolvedImage():
	""" Image with stars and their fluxes """

	def __init__(self,FitsImage,ImageInfo,StarCatalog):
		self.fits_data_notcalibrated = FitsImage.fits_data_notcalibrated
		self.blind_star_finder(FitsImage,ImageInfo)
		self.match_image_catalog(ImageInfo,StarCatalog)
		self.photometric_star_processing(FitsImage,ImageInfo)
		self.save_to_file(ImageInfo)

	def blind_star_finder(self,FitsImage,ImageInfo):

		'''
		Takes an image and detect the peaks using the local maximum filter.
		Returns the location of the peaks, two arrays for Y and X positions.
		'''

		invalid_data_mask = FitsImage.Coordinates.altitude_map<ImageInfo.min_altitude

		print('Looking for stars in the field (blind mode) ...')

		# Search window (in px)
		filter_size = 2

		# Detection threshold
		nsigma = ImageInfo.baseflux_detectable

		print(' -> Median filter on image')
		# Filtered image (noise reduction)
		filtered_data = sfilters.median_filter(\
			input = self.fits_data_notcalibrated,\
			size = filter_size)

		print(' -> Local normalization')

		background_smooth = sfilters.gaussian_filter(\
			input = filtered_data,
			sigma  = 2*ImageInfo.xmatch_dist)

		filtered_data = filtered_data*1./background_smooth

		filtered_data[invalid_data_mask]=None

		print(' -> Maximum filter on image (peaks detection)')
		#apply the local maximum/minimum filter; all pixel of maximal value
		#in their neighborhood are set to 1
		local_max = sfilters.maximum_filter(\
			input = filtered_data, \
			size = ImageInfo.xmatch_dist)==filtered_data

		# Detected peaks (stars)
		detected_peaks = local_max
		detected_peaks[invalid_data_mask] = False

		print(' -> Labeling peaks')
		# Get the labels for each star
		labels,nlabels = smeasurements.label(local_max)

		print(' -> Neighbors of each peak')
		# Get the neighbors for each labeled star
		i, j = smorphology.distance_transform_edt(\
			input = (labels == 0),\
			return_distances = False,\
			return_indices = True)

		neighborhoods = labels[i, j]

		# Remove pixels heavy contaminated by stars light
		# in the background estimation. Iterative.

		print(' -> Remove pixels with stars from background')

		for it in xrange(2):
			without_stars = filtered_data[neighborhoods!=0]
			mdat = without_stars[~np.isnan(without_stars)]
			global_mean = np.median(mdat)
			global_std = np.std(mdat)
			neighborhoods[filtered_data>global_mean-0.5*global_std] = 0

		'''
		print(' -> Local Background estimation (iterative)')

		for it in xrange(2):
			if DEBUG == True:
				print("Iteration %d" %(it))

			local_mean = smeasurements.mean(\
				input = filtered_data,\
				labels = neighborhoods,\
				index = neighborhoods)

			# Remove from the mask.
			neighborhoods[filtered_data>local_mean] = 0
		'''

		print(' -> Min level estimation for each peak region')
		local_min = smeasurements.minimum(\
			input = filtered_data, \
			labels = neighborhoods, \
			index = neighborhoods)

		print(' -> Std estimation for each peak region')
		local_std = smeasurements.standard_deviation(\
			input = filtered_data,\
			labels = neighborhoods,\
			index = neighborhoods)

		print(' -> Reduce images to peak position')
		# Only preserve the values in peaks position.
		local_max = filtered_data*local_max*detected_peaks
		local_min = local_min*detected_peaks
		#local_mean = local_mean*detected_peaks
		local_std = local_std*detected_peaks

		'''
		print("Local Max (max value): %.2f" %(np.max(local_max)))
		print("Local Max (min value): %.2f" %(np.min(local_max)))
		print("Local Mean (max value): %.2f" %(np.max(local_mean)))
		print("Local Mean (min value): %.2f" %(np.min(local_mean)))
		print("Local Std (max value): %.2f" %(np.max(local_std)))
		print("Local Std (min value): %.2f" %(np.min(local_std)))
		'''

		print(' -> Get star candidates')
		# Select the candidates
		candidates = (local_max - local_min) > nsigma*local_std
		detected_peaks[candidates == False] = 0

		# Number of stars after selection
		print(' -> %d stars found in the image.' %(np.sum(detected_peaks)))
		#print("After selection, %d stars detected", %(np.sum(detected_peaks)))

		self.StarsPositionImag = np.where(detected_peaks)
		#return(np.where(detected_peaks))

	def make_pairs(self, coords1, coords2, max_dist):
		'''
		Dummy
		'''

		results = []
		maxdist2 = max_dist * max_dist
		coords1 = sorted(coords1)
		coords2 = sorted(coords2)

		pairs1 = []
		pairs2 = []
		distances = []

		for ia, (xa, ya) in enumerate(coords1):
			for ib, (xb, yb) in enumerate(coords2):
				dx = xb - xa
				dy = yb - ya
				if dx > max_dist or dy > max_dist:
					break
				dist = dx * dx + dy * dy
				if dist < maxdist2:
					results.append([(xa, ya), (xb, yb), ia, ib, dist])

			if len(results)>=1:
				kmin = np.amin(results, axis=4)
				pairs1.append(results[kmin][0])
				pairs2.append(results[kmin][1])
				distances.append(results[kmin][1])

		# Convert to numpys
		pairs1 = np.frombuffer(pairs1)
		pairs2 = np.frombuffer(pairs2)

		# Remove repeated elements
		duplicated1 = np.setxor1d(pairs1, np.unique(pairs1), assume_unique=True)
		duplicated2 = np.setxor1d(pairs2, np.unique(pairs2), assume_unique=True)

		repeated1 = [d == pairs1 for d in duplicated1]
		repeated2 = [d == pairs2 for d in duplicated2]

		thefilter = ~np.sum(repeated1+repeated2, axis=0, dtype=bool)

		pairs1 = pairs1[thefilter]
		pairs2 = pairs2[thefilter]

		return(pairs1, pairs2, distances)

	def match_image_catalog(self,ImageInfo,StarCatalog):
		print('Matching the stars between image and catalog ...')
		# Process the catalog
		StarsInCatalog_orig = []
		for line in StarCatalog.CatalogLines:
			NewStar = CatalogStar(ImageInfo,line)
			StarsInCatalog_orig.append(NewStar)

		# Extract calculated position for each star
		StarsPositionCatalog = []
		self.StarList_Tot = []

		for TheStar in StarsInCatalog_orig:
			try:
				TheStar.Ycoord
				TheStar.Xcoord
			except:
				pass
			else:
				TheStar.xmatched=False
				self.StarList_Tot.append(TheStar)
				StarsPositionCatalog.append([TheStar.Ycoord,TheStar.Xcoord])

		StarsPositionCatalog=np.array(StarsPositionCatalog).transpose()
		#StarsCatalogDict = dict(zip(StarsPositionCatalog, self.StarList_Tot))

		# XMatch both lists
		matches = match_coords.match_coords(\
			StarsPositionCatalog[1], StarsPositionCatalog[0], \
			self.StarsPositionImag[1], self.StarsPositionImag[0], \
			eps=ImageInfo.xmatch_dist, mode='index')

		#self.StarsFound = [StarsCatalogDict[matches[k]] for k in xrange(len(matches))]
		self.StarsFound = []
		for pair in np.transpose(matches):
			TheStar = self.StarList_Tot[pair[0]]
			# Fix the star position
			TheStar.set_position(\
				self.StarsPositionImag[0][pair[1]],\
				self.StarsPositionImag[1][pair[1]])
			TheStar.xmatched=True
			self.StarsFound.append(TheStar)

	def photometric_star_processing(self,FitsImage,ImageInfo):
		'''Create the masked star matrix'''
		FitsImage.star_mask = np.zeros(np.shape(FitsImage.fits_data),dtype=bool)

		self.StarList_Det = []
		self.StarList_Phot = []

		# Fix the position using the detected position
		for TheStar in self.StarsFound:
			# 2nd phase, photometry.
			TheStar.image_phase(FitsImage,ImageInfo)
			# List of stars that should have be detected
			if TheStar.destroy==False:
				# Add to the list of detected stars
				self.StarList_Det.append(TheStar)
				if TheStar.PhotometricStandard==True:
					# Add to the list of stars with good photometric properties
					self.StarList_Phot.append(TheStar)

		print(" -> Total stars (from catalog) :  %d" %(len(self.StarList_Tot)))
		print(" -> XMatched stars (cat+image) :  %d" %(len(self.StarsFound)))
		print(" -> Detected stars with astrom :  %d" %(len(self.StarList_Det)))
		print(" -> With valid photometry      :  %d" %(len(self.StarList_Phot)))


	def save_to_file(self,ImageInfo):

		try:
			assert(ImageInfo.photometry_table_path!=False)
		except:
			print('Skipping write photometric table to file')
		else:
			print('Write photometric table to file')

			content = ['#HDcode, CommonName, RA1950, DEC1950, Azimuth, '+\
			 'Altitude, Airmass, Magnitude, Color(#-V), StarFlux, StarFluxErr, '+\
			 'mag+2.5logF, [mag+2.5logF]_Err\n']
			for Star in self.StarList_Phot:
				text_content = str(\
					"%s, %s, %s, %s, %.5f, %.5f, %.3f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f \n"
					%(Star.HDcode, Star.name, Star.RA1950, Star.DEC1950, Star.azimuth, \
					Star.altit_real, Star.airmass, Star.FilterMag, Star.Color, \
					Star.starflux, Star.starflux_err, Star.m25logF, Star.m25logF_unc))

				content.append(text_content)

			if ImageInfo.photometry_table_path == "screen":
				print(content)
			else:
				def phottable_filename(ImageInfo):
					filename = \
						ImageInfo.photometry_table_path +\
						"/PhotTable_"+ImageInfo.obs_name+"_"+\
						ImageInfo.fits_date+"_"+ImageInfo.used_filter+".txt"
					return(filename)

				photfile = open(phottable_filename(ImageInfo),'w+')
				photfile.writelines(content)
				photfile.close()


