import urllib
import os
import sys
import datetime
import numpy as np

summary_file = 'sdo_daily_data.txt'

def solar_activity_radiation(thedatetime):
	'''
	Estimate mean Solar Activity in the XUV range (1-70A)
	'''
	
	temporary_path = "/tmp/"
	local_filename = temporary_path+"/sdo_data.txt"
	remote_filename = \
	 'http://lasp.colorado.edu/eve/data_access/evewebdata/quicklook/L0CS/SpWx/'+\
	 str(thedatetime.year)+'/'+\
	 str(thedatetime.date()).replace('-','')+\
	 '_EVE_L0CS_DIODES_1m.txt'
	
	def download_file():
		print('Downloading '+str(remote_filename))
		
		# Check if temporary_path exists. If not, create it.
		if not os.path.exists(temporary_path): 
			os.makedirs(temporary_path)
		
		# Check if file exists, then remove it.
		if os.path.isfile(local_filename):
			os.remove(local_filename)
		
		# Download the file with urllib
		urllib.urlretrieve(str(remote_filename),str(local_filename))
	
	def adapt_string(string):
		string = string.replace('\r\n','')
		string = string.replace('\n','')
		string = string.replace('\t',' ')
		while 2*' ' in string:
			string = string.replace('  ',' ')
		return(string)
	
	def process_data():
		sdo_data_file = np.array(open(local_filename).readlines())
		sdo_data_file = np.array([line for line in sdo_data_file \
		  if line.replace(' ','')[0]!=';' and line.replace(' ','')[0]!=''])
		sdo_data = sdo_data_file[2:-2]
		
		sdo_data = np.array([adapt_string(line).split(' ') for line in sdo_data])
		return(sdo_data)
	
	def analize_data(sdo_data,column):
		sdo_column = np.array([line[column] for line in sdo_data])
		#if column==17:
		#	print(sdo_column)
		for value in sdo_column:
			try:
				float(value)
			except:
				print(value)
				#raise
		sdo_column = np.array([float(value) for value in sdo_column if float(value)>=0.0])
		sdo_mean = np.mean(sdo_column)
		sdo_err  = np.std(sdo_column)/np.sqrt(len(sdo_column))
		return(sdo_mean,sdo_err)
	
	sdo_textdata = [str(thedatetime.date())] + ["None +/- None" for k in xrange(17)]
	
	try:
		download_file()
		sdo_data = process_data()
		for column in xrange(1,18):
			try:
				sdo_mean,sdo_err = analize_data(sdo_data,column)
				sdo_textdata[column] = str(sdo_mean)+"+/-"+str(sdo_err)
			except:
				#print(sdo_data)
				print('Cannot analize column '+str(column))
				#raise
	except:
		print('Error processing data for '+str(thedatetime))
		#raise
	
	file_append = open(summary_file,'a+')
	file_append.write(\
	 str(sdo_textdata)\
	  .replace("]","")\
	  .replace("[","")\
	  .replace("'","")+"\n")
	
	file_append.close()

if __name__ == '__main__':
	'''
	Fetch data from SDO server, process data and write to a summary file
	'''
	
	file_append = open(summary_file,'w')
	file_append.write(
	 "# YYYY-MM-DD, XRS-B, XRS-A, SEM, 0.1-7, 17.1, 25.7, 30.4, 36.6, dark, 121.6, dark, q0, q1, q2, q3, CM, CM\n"+\
	 "# proxy, proxy, proxy, ESPquad, ESP, ESP, ESP, ESP, ESP, MEGS-P, MEGS-P, ESP, ESP, ESP, ESP, Lat, Lon\n")
	file_append.close()
	
	start_date = datetime.datetime(2012,1,1,0,0,0)
	end_date   = datetime.datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)
	
	thedate = start_date
	
	while(thedate<end_date):
		solar_activity_radiation(thedate)
		thedate = thedate+datetime.timedelta(days=1)
	
	
	
	
	
