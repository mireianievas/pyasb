'''
Extracted from astropysics
https://astropysics.googlecode.com/hg/astropysics/coords/funcs.py
http://pythonhosted.org/Astropysics/
'''



import numpy as np

def match_coords(a1,b1,a2,b2,eps=1,mode='mask'):
    """
    Match one pair of coordinate :class:`arrays <numpy.ndarray>` to another
    within a specified tolerance (`eps`).

    Distance is determined by the cartesian distance between the two arrays,
    implying the small-angle approximation if the input coordinates are
    spherical. Units are arbitrary, but should match between all coordinates
    (and `eps` should be in the same units)

    :param a1: the first coordinate for the first set of coordinates
    :type a1: array-like
    :param b1: the second coordinate for the first set of coordinates
    :type b1: array-like
    :param a2: the first coordinate for the second set of coordinates
    :type a2: array-like
    :param b2: the second coordinate for the second set of coordinates
    :type b2: array-like
    :param eps:
        The maximum separation allowed for coordinate pairs to be considered
        matched.
    :type eps: float
    :param mode:
        Determines behavior if more than one coordinate pair matches.  Can be:

        * 'mask'
            Returns a 2-tuple of boolean arrays (mask1,mask2) where `mask1`
            matches the shape of the first coordinate set (`a1` and `b1`), and
            `mask2` matches second set (`a2` and `b2`). The mask value is True
            if a match is found for that coordinate pair, otherwise, False.
        * 'maskexcept'
            Retuns the same values as 'mask', and will raise an exception if
            more than one match is found.
        * 'maskwarn'
            Retuns the same values as 'mask', and a warning will be issued if
            more than one match is found.
        * 'count'
            Returns a 2-tuple (nmatch1,nmatch2) with the number of objects that
            matched for each of the two sets of coordinate systems.
        * 'index'
            Returns a 2-tuple of integer arrays (ind1,ind2). `ind1` is a set of
            indecies into the first coordinate set, and `ind2` indexes the
            second.  The two arrays match in shape and each element is the
            index for a matched pair of coordinates - e.g. a1[ind1[i]] and
            a2[ind2[i]] will give the "a" coordinate for a matched pair
            of coordinates.
        * 'match2D'
            Returns a 2-dimensional bool array. The array element M[i,j] is True
            if the ith coordinate of the first coordinate set
            matches the jth coordinate of the second set.
        * 'nearest'
            Returns (nearestind,distance,match). `nearestind` is an int array
            such that nearestind holds indecies into the *second* set of
            coordinates for the nearest object to the ith object in the first
            coordinate set (hence, it's shape matches the *first* coordinate
            set). `distance` is a float array of the same shape giving the
            corresponding distance, and `match` is a boolean array that is True
            if the distance is within `eps`, and is the same shape as the other
            outputs. Note that if a1 and b1 are the same object (and a2 and b2),
            this finds the second-closest match (because the first will always
            be the object itself if the coordinate pairs are the same) This mode
            is a wrapper around :func:`match_nearest_coords`.

    :returns: See `mode` for a description of return types.

    **Examples**

    >>> from numpy import array
    >>> ra1 = array([1,2,3,4])
    >>> dec1 = array([0,0,0,0])
    >>> ra2 = array([4,3,2,1])
    >>> dec2 = array([3.5,2.5,1.5,.5])
    >>> match_coords(ra1,dec1,ra2,dec2,1)
    (array([ True, False, False, False], dtype=bool), array([False, False, False,  True], dtype=bool))
    """

    identical = a1 is a2 and b1 is b2

    a1 = np.array(a1,copy=False).ravel()
    b1 = np.array(b1,copy=False).ravel()
    a2 = np.array(a2,copy=False).ravel()
    b2 = np.array(b2,copy=False).ravel()

    #bypass the rest for 'nearest', as it calls match_nearest_coords
    if mode == 'nearest':
        #special casing so that match_nearest_coords dpes second nearest
        if identical:
            t = (a1,b1)
            seps,i2 = match_nearest_coords(t,t)
        else:
            seps,i2 = match_nearest_coords((a1,b1),(a2,b2))
        return i2,seps,(seps<=eps)

    def find_sep(A,B):
        At = np.tile(A,(len(B),1))
        Bt = np.tile(B,(len(A),1))
        return At.T-Bt
    sep1 = find_sep(a1,a2)
    sep2 = find_sep(b1,b2)
    sep = np.hypot(sep1,sep2)

    matches =  sep <= eps

    if mode == 'mask':
        return np.any(matches,axis=1),np.any(matches,axis=0)
    elif mode == 'maskexcept':
        s1,s2 = np.sum(matches,axis=1),np.sum(matches,axis=0)
        if np.all(s1<2) and np.all(s2<2):
            return s1>0,s2>0
        else:
            raise ValueError('match_coords found multiple matches')
    elif mode == 'maskwarn':
        s1,s2 = np.sum(matches,axis=1),np.sum(matches,axis=0)
        from warnings import warn

        for i in np.where(s1>1)[0]:
            warn('1st index %i has %i matches!'%(i,s1[i]))
        for j in np.where(s2>1)[0]:
            warn('2nd index %i has %i matches!'%(j,s2[j]))
        return s1>0,s2>0
    elif mode == 'count':
        return np.sum(np.any(matches,axis=1)),np.sum(np.any(matches,axis=0))
    elif mode == 'index':
        return np.where(matches)
    elif mode == 'match2D':
        return matches.T
    elif mode == 'nearest':
        assert False,"'nearest' should always return above this - code should be unreachable!"
    else:
        raise ValueError('unrecognized mode')
